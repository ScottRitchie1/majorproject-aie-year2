﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    Scrollbar scrollBar;
    PlayerController player;
    public Color hitGlowColour = Color.red;
    public Gradient healthBarGradient = new Gradient();
    public List<Image> gradientImages = new List<Image>();
    public Image glowImage;
    public float glowHitFadeTime = 1;
    float glowHitTimeStamp = -1;

    // Start is called before the first frame update
    void Start()
    {
        scrollBar = GetComponent<Scrollbar>();
        player = FindObjectOfType<PlayerController>();
        player.OnDamagedEvent.AddListener(OnPlayerHit);
    }

    // Update is called once per frame
    void Update()
    {
        if (player)
        {
            scrollBar.size = Mathf.InverseLerp(0, player.maxHealth, player.currentHealth);
            foreach(Image img in gradientImages)
            {
                img.color = healthBarGradient.Evaluate(scrollBar.size);
            }

            if (glowImage) glowImage.color = Color.Lerp(hitGlowColour, Color.black, Mathf.InverseLerp(glowHitTimeStamp, glowHitTimeStamp + glowHitFadeTime, Time.time));
        }
    }

    void OnPlayerHit(float dmg)
    {
        glowHitTimeStamp = Time.time;
    }
}

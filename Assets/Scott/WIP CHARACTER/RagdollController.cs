﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour {

    public float deathvelocity = 14;

    public List<SkinnedMeshRenderer> skins;

    public List<Transform> ragDollBones;

    public List<Transform> destroyOnRagDoll;

    private void Start()
    {
        if(skins.Count == 0)
        {
            skins = new List<SkinnedMeshRenderer>(GetComponentsInChildren<SkinnedMeshRenderer>());
        }

        CharacterInstanceManager.instance.controller.OnGroundEvent += CheckFallDamage;
    }
    private void OnDestroy()
    {
        CharacterInstanceManager.instance.controller.OnGroundEvent -= CheckFallDamage;

    }
    public void InitilizeRagdoll()
    {
        foreach (Transform t in ragDollBones)
        {
            t.GetComponent<Rigidbody>().isKinematic = true;
            t.GetComponent<Collider>().enabled = false;
            t.gameObject.layer = LayerMask.NameToLayer("RagDollBones");
        }
    }

    public void EnableRagDoll(Vector3 force)
    {
        foreach (Transform t in ragDollBones)
        {
            Rigidbody trb = t.GetComponent<Rigidbody>();
            trb.isKinematic = false;
            trb.velocity = force;
            t.GetComponent<Collider>().enabled = true;
        }

        foreach(Transform t in destroyOnRagDoll)
        {
            Destroy(t.gameObject);
        }
    }

    public void CheckFallDamage(bool isGrounded)
    {
        if (isGrounded && CharacterInstanceManager.instance.rb.velocity.magnitude > deathvelocity)// OLD CODE REMOVE AND MAKE BETTER DIE FUNCTION
        {
            CharacterInstanceManager.instance.animatorController.ToRagDoll();
        }
    }
}

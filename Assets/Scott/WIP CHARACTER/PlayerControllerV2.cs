﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerControllerV2 : MonoBehaviour
{
    public Vector2 overrideInput = new Vector2(1, 0);

    CharacterController m_characterController;
    bool m_grounded = false;
    float m_distanceToGround = 0;
    public LayerMask groundLayerMask;
    RaycastHit m_groundHit;
    public Vector3 slopeMoveVector;
    public float slopeMoveAngle;


    Vector3 targetMoveDirection;
    [Header("Grounded")]
    public float runSpeed = 7;
    public float walkSpeed = 5;
    public float crouchedSpeed = 3;
    [Space]
    public float accelerationRunning = 6;
    public float accelerationWalking = 3;
    public float accelerationCrouched = 3;
    public float deaccelerationGrounded = 5;

    [Space]
    public float groundedGravity = 20;
    public float airTimeGravity = 10;

    [Space]
    public float m_turnRate = 4;

    public float m_moveSpeed = 6.0f;
    public float m_acceleration = 6.0f;
    public float m_deacceleration = 6.0f;
    public float m_moveSmoothing = 4.0f;
    public float m_jumpForce = 8.0f;
    public float m_gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;

    public float groundedDistance = 1;
    float maxMoveSpeedOnCurrentIncline = 99;
    void Awake()
    {
        m_characterController = GetComponent<CharacterController>();
        //m_characterController.detectCollisions = false;
    }

    void UpdateGrounded()
    {
        if (Physics.SphereCast(transform.position + Vector3.up * m_characterController.radius, m_characterController.radius, Vector3.down, out m_groundHit, Mathf.Infinity, groundLayerMask, QueryTriggerInteraction.Ignore))
        {
            RaycastHit slopCheckHit;
            //m_groundHit.collider.Raycast(new Ray(m_groundHit.point + Vector3.up, Vector3.down), out slopCheckHit, 2);
            if(Physics.Raycast(new Ray(m_groundHit.point + Vector3.up, Vector3.down), out slopCheckHit, 2, groundLayerMask, QueryTriggerInteraction.Ignore))
                m_groundHit.normal = slopCheckHit.normal;

            m_distanceToGround = CalculateDistanceToGround();
            slopeMoveVector = Vector3.Cross(Vector3.Cross(Vector3.up, targetMoveDirection), m_groundHit.normal);
            slopeMoveAngle = Vector3.SignedAngle((m_grounded ? slopeMoveVector : targetMoveDirection), targetMoveDirection, Vector3.Cross(Vector3.up, targetMoveDirection));

            // if we arnt jumping this frame, and we are within grounded distance 
            if(Input.GetKeyDown(KeyCode.Space) == false && (m_distanceToGround <= groundedDistance || m_characterController.isGrounded) && (m_grounded || m_characterController.velocity.y <= 0))
            {
                m_grounded = true;
            }
            else
            {
                m_grounded = false;
            
            }
            //if (m_grounded && m_distanceToGround > m_characterController.stepOffset)
            //{
            //    OnNotGrounded();
            //}
            //else if(!m_grounded && m_distanceToGround <= m_characterController.stepOffset)
            //{
            //    OnGrounded();
            //}

            //if (m_distanceToGround - 0.1f <= -m_characterController.velocity.y * Time.deltaTime)
            //{
            //    //recalculate the incline of the hit surface
            //    //downInclineAngle = CalculateDownInclineAngle();
            //    //if (downInclineAngle < m_maxGroundedIncline)
            //    //{
            //    if(!m_grounded)
            //        
            //    //return;
            //    //}
            //}
            //else
            //{
            //    if (m_grounded)
            //        
            //}

        }
        else
        {
            m_distanceToGround = Mathf.Infinity;
            m_grounded = false;
        }

        
    }

    void Update()
    {

        Vector3 lookDirection = CamController.instance.GetCameraDirectionAligned();
        Vector3 inputDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * Mathf.Max(Mathf.Abs(Input.GetAxisRaw("Horizontal")), Mathf.Abs(Input.GetAxisRaw("Vertical")));// gets the move input
        if(overrideInput.magnitude > 0)
        {
            inputDirection = new Vector3(overrideInput.x, 0, overrideInput.y).normalized * Mathf.Max(Mathf.Abs(overrideInput.x), Mathf.Abs(overrideInput.y));// gets the move input
        }
        
        //if(inputDirection.magnitude > 0)
        targetMoveDirection = Quaternion.LookRotation(lookDirection) * inputDirection;

        UpdateGrounded();


        if (inputDirection.magnitude > 0)
        {
            m_deacceleration = deaccelerationGrounded;

            if (Input.GetKey(KeyCode.LeftShift))
            {
                m_moveSpeed = runSpeed;
                m_acceleration = accelerationRunning;
            }
            else
            {
                m_moveSpeed = walkSpeed;
                m_acceleration = accelerationWalking;

            }
        }
        else
        {
            m_moveSpeed = 0;
        }

        if (m_grounded)
        {
            //m_gravity = groundedGravity;
            // We are grounded, so recalculate
            // move direction directly from axes

            //moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
            float speedChangeRate = ((m_characterController.velocity.magnitude < m_moveSpeed ? m_acceleration : m_deacceleration));
            Debug.Log("speedChangeRate: " + speedChangeRate);

            moveDirection = Vector3.Slerp((m_characterController.velocity.magnitude <= 0.1f? transform.forward : m_characterController.velocity.normalized), slopeMoveVector, m_moveSmoothing * Time.deltaTime).normalized;
            //Debug.Log(Vector3.Dot(moveDirection, transform.forward));
            //moveDirection *= (Vector3.Dot(moveDirection, targetMoveDirection) > 0? m_moveSpeed : 1);
            Debug.Log(Mathf.Clamp01(Vector3.Dot(moveDirection, targetMoveDirection)));
            moveDirection *= m_moveSpeed;

            //moveDirection *= Mathf.MoveTowards(m_characterController.velocity.magnitude, m_moveSpeed, speedChangeRate * Time.deltaTime);
            //moveDirection = slopeMoveVector * m_moveSpeed;
            moveDirection.y -= groundedGravity * Time.deltaTime;

            //moveDirection.y = m_characterController.velocity.y + moveDirection.y;
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = m_jumpForce * Time.deltaTime;
            }
        }
        else
        {
            moveDirection.y -= airTimeGravity * Time.deltaTime;

            //m_gravity = airTimeGravity;
        }



        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)


        // Move the controller
        bool wasGroundeded = m_characterController.isGrounded;
        m_characterController.Move(moveDirection * Time.deltaTime);

        float angle = Vector3.SignedAngle(transform.forward, Vector3.Slerp(transform.forward, (Input.GetMouseButton(0) ? Camera.main.transform.forward : targetMoveDirection), m_turnRate * Time.deltaTime), Vector3.up);
        transform.RotateAround(transform.position, Vector3.up, angle);
        Debug.Log(m_characterController.isGrounded);
    }

    [ContextMenu("TEST")]
    public void TEST()
    {
        m_characterController.Move(new Vector3(1,-1,0));
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(50, 50, 250, 50), "Grounded Status CONTROLLER: " + (m_characterController.isGrounded ? "GROUNDED" : "AIRTIME"));
        GUI.Label(new Rect(50, 70, 250, 50), "Grounded Status CUSTOM: " + (m_grounded ? "GROUNDED" : "AIRTIME"));
        GUI.Label(new Rect(50, 90, 250, 50), "Dist to ground: " + (Mathf.Round(m_distanceToGround * 100) / 100).ToString());
        GUI.Label(new Rect(50, 110, 250, 50), "Ground Angle: " + (Mathf.Round(slopeMoveAngle * 100) / 100).ToString());
        GUI.Label(new Rect(50, 130, 250, 50), "Velocity: " + m_characterController.velocity.ToString() + "   Mag:" +  m_characterController.velocity.magnitude.ToString());


    }

    private void OnDrawGizmosSelected()
    {
        if (!Application.isPlaying) {
            return;
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, targetMoveDirection * 2);
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position + Vector3.up * m_characterController.radius, m_characterController.radius * 0.9f);
        Gizmos.color = Color.green;
        Gizmos.DrawRay(m_groundHit.point, m_groundHit.normal);
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(m_groundHit.point, slopeMoveVector);

    }

    private float CalculateDistanceToGround()
    {
        return Mathf.Abs(transform.position.y - m_groundHit.point.y - m_characterController.skinWidth);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimator : MonoBehaviour {

    CharacterInstanceManager charInstance;
    [HideInInspector]
    public Animator anim;

    public Transform centerOfMassBone;

    

    [Header("JetPack")]
    public float velocityRequiredForFullJetpackAnimation = 5;
    public float angleRequiredForFullJetpackTurnAnimation = 90;

    [Header("TILT")]
    public bool allowTilting = true;
    [Space]
    public float forwardTiltAmount = 25f;
    public float forwardTiltThreshhold = 1;
    public float backwardsTiltAmount = 25f;
    public float backwardsTiltThreshhold = 1;
    public float forwardTiltSmooth = 5;
    float currentForwardTilt;

    [Space]
    public float sideTiltAmount = 15f;
    public float sideTiltThreshhold = 1;
    public float sideTiltSmoothing = 5;
    float currentSideTilt;


    float lastRotaton;
    Vector3 localVel;

    Vector3 previousPosition;
    float forwardVelocity;
    float previousForwardVeocity;
    float forwardAcceleration;
    float rotationDelta;

    [Header("RAGDOLL")]
    public RagdollController ragdoll;
    public RagdollController activeRagdoll;
    private void Start()
    {
        charInstance = CharacterInstanceManager.instance;
        anim = GetComponent<Animator>();
        charInstance.controller.OnForceAppliedEvent += OnAnimateJump;
        if(ragdoll)
            ragdoll.InitilizeRagdoll();
        //WTF DO I NEED TO CALL THIS
        GetComponent<PlayerIkController>().enabled = false;
        GetComponent<PlayerIkController>().enabled = true;
        if (!centerOfMassBone)
        {
            centerOfMassBone = transform;
        }
    }

    private void Update()
    {
        Vector3 velXY = new Vector3(charInstance.rb.velocity.x, 0, charInstance.rb.velocity.z);
        localVel = charInstance.transform.InverseTransformDirection(charInstance.rb.velocity);
        //Vector3.SignedAngle(charInstance.rb.velocity.normalized, transform.forward, Vector3.up);
        anim.SetFloat("forwardVel", localVel.z);
        anim.SetFloat("sideVel", localVel.x);
        anim.SetFloat("verticelVel", localVel.y);
        anim.SetFloat("distToGround", charInstance.controller.distanceToGround);
        anim.SetBool("isGrounded", charInstance.controller.isGrounded);
        //anim.SetBool("JetPackActive", charInstance.jetPack.isThrusting);
        //anim.SetFloat("turnDelta", Mathf.MoveTowards(anim.GetFloat("turnDelta"), (-Vector3.SignedAngle(velXY.normalized, charInstance.transform.forward, Vector3.up) / angleRequiredForFullJetpackTurnAnimation) * Mathf.InverseLerp(0, velocityRequiredForFullJetpackAnimation, velXY.magnitude), Time.deltaTime * 100));
        anim.SetBool("Magic01", Input.GetKey(KeyCode.R));
    }

    public void FixedUpdate()
    {
        rotationDelta =  lastRotaton - charInstance.transform.eulerAngles.y;

        lastRotaton = charInstance.transform.eulerAngles.y;

        forwardVelocity = charInstance.transform.InverseTransformDirection(charInstance.transform.position - previousPosition).z;
        previousPosition = charInstance.transform.position;
        forwardAcceleration = forwardVelocity - previousForwardVeocity;
        previousForwardVeocity = forwardVelocity;

        if (allowTilting && charInstance.controller.isGrounded)
        {
            currentForwardTilt = Mathf.LerpAngle(currentForwardTilt, 
                (forwardAcceleration > 0? 
                ((Mathf.InverseLerp(-forwardTiltThreshhold, forwardTiltThreshhold, forwardAcceleration * 100) - 0.5f) * 2) * forwardTiltAmount:
                ((Mathf.InverseLerp(-backwardsTiltThreshhold, backwardsTiltThreshhold, forwardAcceleration * 100) - 0.5f) * 2) * backwardsTiltAmount)
                , Time.fixedDeltaTime * forwardTiltSmooth);

            currentSideTilt = Mathf.LerpAngle(currentSideTilt, ((Mathf.InverseLerp(-sideTiltThreshhold, sideTiltThreshhold, rotationDelta) - 0.5f) * 2) * sideTiltAmount, Time.fixedDeltaTime * sideTiltSmoothing);

            transform.localEulerAngles = new Vector3(currentForwardTilt, 0, currentSideTilt * Mathf.InverseLerp(1, 2, charInstance.rb.velocity.magnitude));
        }
        else if(transform.localEulerAngles != Vector3.zero)
        {
            transform.localEulerAngles = new Vector3(Mathf.MoveTowardsAngle(transform.localEulerAngles.x, 0, Time.fixedDeltaTime * 10), 0, Mathf.MoveTowardsAngle(transform.localEulerAngles.z, 0, Time.fixedDeltaTime * 10));
        }
    }

    public void OnAnimateJump(Vector3 force)
    {
        anim.SetTrigger("Jumped");
    }

    public void OnOutOfFuel()
    {
        //if(charInstance.controller.isGrounded)
        anim.SetTrigger("OutOfFuel");
    }

    public void ToRagDoll()
    {
        if (ragdoll)
        {
            activeRagdoll = Instantiate(ragdoll, ragdoll.transform.position, ragdoll.transform.rotation, null);
            activeRagdoll.EnableRagDoll(CharacterInstanceManager.instance.rb.velocity);
            foreach (SkinnedMeshRenderer skin in ragdoll.skins)
            {
                skin.enabled = false;
            }
         
        }
    }

    public void ResetRagDoll()
    {
        if (ragdoll)
        {
            activeRagdoll = null;
            foreach (SkinnedMeshRenderer skin in ragdoll.skins)
            {
                skin.enabled = true;
            }
        }
    }

}

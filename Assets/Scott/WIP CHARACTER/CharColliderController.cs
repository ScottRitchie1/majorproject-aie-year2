﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class CharColliderController : MonoBehaviour {

    //public CapsuleCollider upperCollider;
    public CapsuleCollider lowerCollider;
    public float groundedFriction;// the amount of friction our character has when grounded

    float lowerColHeight;
    float lowerColOffset;
    //float upperColOffset;

    // Use this for initialization
    void Start () {
        if(!lowerCollider)
            lowerCollider = GetComponent<CapsuleCollider>();
        lowerColHeight = lowerCollider.height;
        lowerColOffset = lowerCollider.center.y;
        //if (upperCollider)
            //upperColOffset = upperCollider.center.y;

        
        //if (upperCollider)
            //upperCollider.material = pm;
    }
	
	// Update is called once per frame
	void Update () {
        float animRootOffset = CharacterInstanceManager.instance.animatorController.transform.localPosition.y;
        //lowerCollider.height = lowerColHeight + animRootOffset;
        //lowerCollider.center = Vector3.up * (lowerColOffset + (animRootOffset / 2));
        //if(upperCollider)
            //upperCollider.center = Vector3.up * (upperColOffset + (animRootOffset));
    }
}

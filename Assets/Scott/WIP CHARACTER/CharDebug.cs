﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharDebug : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P) && CharacterInstanceManager.instance.gameObject.activeInHierarchy)
        {
            CharacterInstanceManager.instance.animatorController.ResetRagDoll();
            CharacterInstanceManager.instance.animatorController.ToRagDoll();
        }
        else if (Input.GetKeyDown(KeyCode.O) && CharacterInstanceManager.instance.gameObject.activeInHierarchy)
        {
            CharacterInstanceManager.instance.animatorController.ResetRagDoll();
        }
    }
}

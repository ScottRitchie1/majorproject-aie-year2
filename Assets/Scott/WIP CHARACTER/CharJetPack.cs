﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class controllerState
{
    public float moveSpeed;// the max rate at which the character moves
    public float turnSpeed;// the max rate at which the character turns
    [Space]
    public float acceleration;// the rate at which the character can reach their max move speed
    public float deacceleration;// the rate at which the character returns to no movement speed
    [Range(0.0f, 5.0f)]
    public float moveDirectionalSmoothing = 0.2f;// the amount our character's horizontal movement is smoothed - this doesnt effect forwards and backwards movement - change acceleration for this
}
[RequireComponent(typeof(PlayerEngine))]
public class CharJetPack : MonoBehaviour {
    PlayerEngine charController;
    Rigidbody rb;
    [Header("Thrust")]
    public bool isThrusting = false;
    public float force;
    public float maxVelLimit;
    public float inputDelay;
    [Range(0,1)]
    public float turnDampingOnThrust;
    float curInputDelay;

    [Header("Fuel")]
    public float fuelTime;
    [SerializeField]
    float curFuelTime;
    public float recoveryTime;
    
    

    public float recoveryStepAngle;

    bool waitToRepressJump = true;
    bool wasCharacterGrounded = true;

    [Space]
    public controllerState jetPackState;

	// Use this for initialization
	void Start () {
        charController = GetComponent<PlayerEngine>();
        rb = GetComponent<Rigidbody>();
        curFuelTime = fuelTime;
        charController.OnForceAppliedEvent += OnJump;
    }

    private void OnDestroy()
    {
        charController.OnForceAppliedEvent -= OnJump;
    }

    // Update is called once per frame
    void Update () {
		if(!wasCharacterGrounded)
        {

            if (Input.GetButtonUp("Jump") || curFuelTime < 0 && isThrusting)
            {
                isThrusting = false;
                //charController.currentControllerState = charController.airTimeControllerState;
                waitToRepressJump = false;
            }
            else if(Input.GetButtonDown("Jump") && !wasCharacterGrounded)
            {
                if (!waitToRepressJump)
                {
                    isThrusting = true;
                    //charController.currentControllerState = jetPackState;
                }
            }

            if (isThrusting)
            {
                curInputDelay += Time.deltaTime;
                if (curInputDelay >= inputDelay)
                {
                    curFuelTime -= Time.deltaTime;
                    if (rb.velocity.y < maxVelLimit)
                    {
                        rb.AddForce(transform.up * force * Time.deltaTime, ForceMode.Force);
                    }
                }
            }
            else
            {
                curInputDelay = 0;
            }
        }else
        {
            isThrusting = false;
            //if (charController.GetCurrentStepAngle() < recoveryStepAngle)
            //{
                if (curFuelTime < fuelTime)
                    curFuelTime += Time.deltaTime * fuelTime / recoveryTime;
                else
                {
                    curFuelTime = fuelTime;
                }
            //}

            curInputDelay = 0;
        }

        wasCharacterGrounded = charController.isGrounded;



    }

    void OnJump(Vector3 force)
    {
        waitToRepressJump = true;
    }

    public float GetCurrentFuelPercentage()
    {
        return curFuelTime / fuelTime;
    }
}

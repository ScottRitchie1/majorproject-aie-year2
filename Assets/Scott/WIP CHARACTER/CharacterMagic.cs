﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterMagic : MonoBehaviour
{
    public enum _magicType { fire, water, earth };
    public enum lockSPineType {None, CharacterForward, CameraForward};
    [System.Serializable]
    public class SpellDefinition
    {
        public enum _MouseEnum {Left, Right, Middle };
        public _MouseEnum spellInput;
        public _magicType spellType;
        public float spellCooldown = 0.2f;
        public GameObject enableWhileActive;
        public GameObject spawnOnActivate;
        public Transform spawnLocation;
        public bool isHoldSpell;
        public bool characterFaceMoveDirection = true;
        public bool forceWalk;
        public lockSPineType lockSpine;
        public string animationTrigger = "";
        public bool onlyCastWhenGrounded = false;
        public AudioClip sound;
    }
    public SpellDefinition shieldSpell;

    public SpellDefinition fireTrapSpell;
    public SpellDefinition earthTrapSpell;
    public SpellDefinition iceTrapSpell;


    SpellDefinition currentSpell;

    
    public _magicType magicType;

    PlayerIkController ikController;
    PlayerController playerController;

    public float trapManaCost = 75;
    public AudioSource audioSource;
    [SerializeField]
    bool castingSpell = false;

    public float waitTillCastSpellTime;
    public float slerpRate = 0.5f;
    public float currentSlerp = 0;
    PlayerSpellCaster caster;
    // Start is called before the first frame update
    void Start()
    {
        ikController = GetComponentInChildren<PlayerIkController>();
        playerController = GetComponent<PlayerController>();
        magicType = _magicType.earth;
        caster = GetComponent<PlayerSpellCaster>();
    }

    // Update is called once per frame
    void Update()
    {
        if (caster && (int)magicType != (int)caster.selectedElementType)
        {
            if(caster.selectedElementType == EElementType.FIRE)
            {
                magicType = _magicType.fire;
                //magicColour.sharedMaterial.color = fireCharacterColor;
                //capeColour.sharedMaterial.color = fireCharacterColor;
            }
            else if(caster.selectedElementType == EElementType.WATER)
            {
                magicType = _magicType.water;
               // magicColour.sharedMaterial.color = iceCharacterColor;
                //capeColour.sharedMaterial.color = iceCharacterColor;
            }
            else
            {
                magicType = _magicType.earth;
                //magicColour.sharedMaterial.color = earthCharacterColor;
                //capeColour.sharedMaterial.color = earthCharacterColor;
            }
        }
        bool canCastSpell = waitTillCastSpellTime < 0;

        if (!canCastSpell)
        {
            caster.currentManaCooldown = 0;
        }

        caster.canCastSpells = (currentSpell == null);


        if (currentSpell != null)
        {
            UpdateCurrentSpell();
        }
        else
        {
            waitTillCastSpellTime -= Time.deltaTime;

            if(waitTillCastSpellTime < 0 && (caster == null || caster.isCasting == false) && caster.currentMana - trapManaCost >= 0)
            {
                if (Input.GetMouseButtonDown((int)shieldSpell.spellInput))
                {
                    CastSpell(shieldSpell);
                }
                else if (Input.GetMouseButtonDown(1))
                {
                    if(magicType == _magicType.fire)
                    {
                        CastSpell(fireTrapSpell);
                    }
                    else if(magicType == _magicType.water)
                    {
                        CastSpell(iceTrapSpell);

                    }
                    else
                    {
                        CastSpell(earthTrapSpell);
                    }
                }
            }
        }
       
    }

    public void CastSpell(SpellDefinition spell)
    {
        if (currentSpell != null)
            return;

        if(spell.onlyCastWhenGrounded == true && !playerController.isGrounded)
        {
            return;
        }

        if(audioSource && spell.sound)
        {
            audioSource.PlayOneShot(spell.sound);
        }

        currentSpell = spell;
        StartCoroutine(depleteManaOverTime(currentSpell.spellCooldown, trapManaCost));
        if (currentSpell.spawnOnActivate)
        {
            Vector3 spawnPos = (currentSpell.spawnLocation ? currentSpell.spawnLocation.position : transform.position);
            Quaternion spawnRot = (currentSpell.spawnLocation ? currentSpell.spawnLocation.rotation : transform.rotation);

            GameObject newObj = GameObject.Instantiate(currentSpell.spawnOnActivate, spawnPos, spawnRot, null);
        }

        if(currentSpell.animationTrigger.Length > 0 && playerController.anim)
        {
            playerController.anim.SetBool(currentSpell.animationTrigger, true);
        }

        if (currentSpell.isHoldSpell)
        {
            if (currentSpell.enableWhileActive)
            {
                currentSpell.enableWhileActive.SetActive(true);
            }

            playerController.alignToMoveDirection = currentSpell.characterFaceMoveDirection;
            playerController.forceWalk = currentSpell.forceWalk;

        }
    }
    public void UpdateCurrentSpell()
    {
        if (currentSpell == null)
            return;

        if (!currentSpell.isHoldSpell || !Input.GetMouseButton((int)currentSpell.spellInput))
        {
            StopCastingActiveSpell();
            return;
        }

    }
    public void StopCastingActiveSpell()
    {
        if(currentSpell == null)
        {
            return;
        }

        if (currentSpell.enableWhileActive)
        {
            currentSpell.enableWhileActive.SetActive(false);
        }

        playerController.alignToMoveDirection = true;
        playerController.forceWalk = false;
        if (ikController)
            ikController.lookatBodyRatio = 0;
        if (currentSpell.animationTrigger.Length > 0 && playerController.anim)
        {
            playerController.anim.SetBool(currentSpell.animationTrigger, false);
        }

        waitTillCastSpellTime = currentSpell.spellCooldown;
        currentSpell = null;
    }

    IEnumerator depleteManaOverTime(float time, float amount)
    {
        float start = Time.time;
        while (Time.time < start + time)
        {
            if(!caster.HasInfiniteMana)
                caster.currentMana -= (amount * Time.deltaTime) / time;
            yield return null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


public class CamController : MonoBehaviour {

    public bool trackTarget = true;

    public static CamController instance;
    public CharacterInstanceManager target;
    public Transform transformTarget;
    [Space]
    public bool lockCursor;
    public float mouseSensitivity = 10;
    [Range(0,1)]
    public float mouseSensitivityWeight = 1;
    public float rotationSmoothTime = .12f;
    [Space]
    public float maxCameraDistance = 2;
    public float minCameraDistance = 0.4f;
    [Space]
    public float targetOffset;
    public Vector2 pitchMinMax = new Vector2(-40, 85);
    RaycastHit collisionHit;
    [Space]
    public Vector2 camPositionCastsOffset;
    
    Vector3[] cameraRayCollisionCheckDirections = new Vector3[5];
    public float collisionSmoothing;

    Vector3 rotationSmoothVelocity;
    Vector3 currentRotation;

    float curCollisionDist;

    float yaw;
    float pitch;
    int layerMask;

    private void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("CamController instance already set."); }

        //QualitySettings.vSyncCount = 0;
        //Application.targetFrameRate = 30;
    }

    void Start()
    {
        lockCursor = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        layerMask = ~LayerMask.GetMask("DontBlockCamera", "Ignore Raycast", "InteractableEntity", "Character", "BlockBuilding");

        if (!target)
        {
            target = CharacterInstanceManager.instance;
        }
        curCollisionDist = maxCameraDistance;
    }

    void Update()
    {

        if (lockCursor)
        {
            yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
            pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        }

        if (trackTarget)
        {

            Vector3 targetPos = Vector3.zero;// = (target.animatorController.activeRagdoll ? target.animatorController.activeRagdoll.ragDollBones[0].transform.position : target.animatorController.centerOfMassBone.position) + (Vector3.up * targetOffset);
            if (transformTarget)
            {
                targetPos = transformTarget.position + (Vector3.up * targetOffset);
            }

            pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);
            currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);
            transform.eulerAngles = currentRotation;

            cameraRayCollisionCheckDirections[0] = -transform.forward;
            cameraRayCollisionCheckDirections[1] = -transform.forward + (transform.rotation * (camPositionCastsOffset / maxCameraDistance));
            cameraRayCollisionCheckDirections[2] = -transform.forward + (transform.rotation * (new Vector2(-camPositionCastsOffset.x, -camPositionCastsOffset.y) / maxCameraDistance));
            cameraRayCollisionCheckDirections[3] = -transform.forward + (transform.rotation * (new Vector2(camPositionCastsOffset.x, -camPositionCastsOffset.y) / maxCameraDistance));
            cameraRayCollisionCheckDirections[4] = -transform.forward + (transform.rotation * (new Vector2(-camPositionCastsOffset.x, camPositionCastsOffset.y) / maxCameraDistance));

            float closestRayCollisionDist = maxCameraDistance;

            for (int i = 0; i < cameraRayCollisionCheckDirections.Length; i++)
            {
                Debug.DrawRay(targetPos, cameraRayCollisionCheckDirections[i] * maxCameraDistance);
                if (Physics.Raycast(targetPos, cameraRayCollisionCheckDirections[i], out collisionHit, maxCameraDistance, layerMask, QueryTriggerInteraction.Ignore))
                {
                    float dist = Vector3.Distance(collisionHit.point, targetPos);
                    if (dist < closestRayCollisionDist)
                    {
                        closestRayCollisionDist = dist;
                    }
                }
            }

            if (closestRayCollisionDist < minCameraDistance) closestRayCollisionDist = minCameraDistance;

            curCollisionDist = (curCollisionDist < closestRayCollisionDist ? Mathf.Lerp(curCollisionDist, closestRayCollisionDist, Time.deltaTime * collisionSmoothing) : closestRayCollisionDist);

            transform.position = targetPos - transform.forward * curCollisionDist;
        }
        UpdateCursorLockState();
    }

    public Vector3 GetCameraDirection()
    {
        return transform.forward;
    }

    public Vector3 GetCameraDirectionAligned()
    {
        return Vector3.Cross(transform.right, Vector3.up);
    }

    public void UpdateCursorLockState()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                LockCursor(false);
            }
            else
            {
                LockCursor(true);
            }
        }
    }

    public void LockCursor(bool state)
    {
        if (state == false)
        {
            lockCursor = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            lockCursor = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}

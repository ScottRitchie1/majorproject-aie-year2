﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInstanceManager : MonoBehaviour {

    public static CharacterInstanceManager instance;

    public PlayerEngine controller;
    public CharColliderController colliderController;
    public CharJetPack jetPack;
    public Rigidbody rb;

    public CharAnimator animatorController;

    private void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("CharacterInstanceManager instance already set."); }

        controller = GetComponent<PlayerEngine>();
        colliderController = GetComponent<CharColliderController>();
        jetPack = GetComponent<CharJetPack>();
        rb = GetComponent<Rigidbody>();
        animatorController = GetComponentInChildren<CharAnimator>();
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal : MonoBehaviour
{
    public List<GameObject> CrystalChunks;
    public List<Vector3> positions;
    public List<Quaternion> rotations;
    public List<GameObject> recoverChunks;
    public float recoverRate = 10;
    public Transform player;
    // Start is called before the first frame update

    private void Awake()
    {
        foreach(GameObject chunk in CrystalChunks)
        {
            positions.Add(chunk.transform.position);
            rotations.Add(chunk.transform.rotation);
        }

        player = FindObjectOfType<PlayerController>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        foreach(GameObject chunk in CrystalChunks)
        {
            if(!recoverChunks.Contains(chunk) && chunk.transform.parent == null && Vector3.Distance(player.position, chunk.transform.position) < 2)
            {
                recoverChunks.Add(chunk);
                chunk.GetComponent<Collider>().enabled = false;
                chunk.GetComponent<Rigidbody>().isKinematic = true;
            }
        }

        for (int i = 0; i < recoverChunks.Count; i++)
        {
            Vector3 chunkOrigin = positions[CrystalChunks.IndexOf(recoverChunks[i])];
            if (recoverChunks[i].transform.parent == null)
            {
                recoverChunks[i].transform.position = Vector3.MoveTowards(recoverChunks[i].transform.position, chunkOrigin, Time.deltaTime * recoverRate);

                if(Vector3.Distance(recoverChunks[i].transform.position, chunkOrigin) < 3)
                {
                    recoverChunks[i].transform.position = chunkOrigin;
                    recoverChunks[i].transform.rotation = rotations[CrystalChunks.IndexOf(recoverChunks[i])];
                    recoverChunks[i].transform.SetParent(transform);
                    recoverChunks[i].GetComponent<Rigidbody>().isKinematic = true;
                    recoverChunks[i].GetComponent<Collider>().enabled = true;
                    recoverChunks.RemoveAt(i);
                    i--;
                }
            }
        }
    }

    public GameObject GetNextCrystalChunk()
    {
        for(int i = 0; i < CrystalChunks.Count; i++)
        {
            if(CrystalChunks[i].transform.parent == transform)
            {
                return CrystalChunks[i];
            }
        }

        return null;
    }

    public GameObject GetClosestCrystalChunk(Vector3 pos)
    {
        GameObject closestChunk = null;
        foreach(GameObject chunk in CrystalChunks)
        {
            if (chunk.transform.parent == null && !recoverChunks.Contains(chunk))
            {
                if (closestChunk == null)
                {
                    closestChunk = chunk;
                }
                else if (Vector3.Distance(pos, closestChunk.transform.position) > Vector3.Distance(chunk.transform.position, pos))
                {
                    closestChunk = chunk;
                }
            }
        }

        if (closestChunk && Vector3.Distance(transform.position, pos) > Vector3.Distance(closestChunk.transform.position, pos))
        {
            return closestChunk;
        }
        else
        {
            return GetNextCrystalChunk();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealerSpawner : MonoBehaviour
{
    public float delay = 10;
    public float curTime;
    public int amount = 1;
    public GameObject toSpawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        curTime -= Time.deltaTime;

        if(curTime < 0)
        {
            for(int i = 0; i < amount; i++)
            {
                GameObject.Instantiate(toSpawn, transform.position, transform.rotation);
            }
            curTime = delay;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillStealers : MonoBehaviour
{
    public float range = 2;
    public bool alwaysActive = true;
    public LayerMask mask;
    // Start is called before the first frame update
    void Start()
    {
        mask = LayerMask.GetMask("Character");
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] hitObjs = Physics.OverlapSphere(transform.position, range, mask);

        foreach(Collider obj in hitObjs)
        {
            Debug.Log(obj.gameObject.name);
            Stealer stealer = obj.transform.GetComponent<Stealer>();
            if (stealer)
            {
                stealer.Die();
            }
        }

        if (!alwaysActive)
        {
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

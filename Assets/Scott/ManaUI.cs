﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaUI : MonoBehaviour
{
    RectTransform rectT;
    Scrollbar scrollBar;
    PlayerSpellCaster player;
    public float manaBarSizeIncreasePerMana = 5;
    public Text infiniteManaUI;
    public Image glowImage;
    float manaBarOriginalSize;
    float originalManaPoolSize;
    public Color manaBarGlowColour = Color.cyan;
    public float manaGlowFadeTime = 0.5f;
    float manaGlowTimeStamp = -1;
    // Start is called before the first frame update
    void Start()
    {
        scrollBar = GetComponent<Scrollbar>();
        rectT = GetComponent<RectTransform>();
        player = FindObjectOfType<PlayerSpellCaster>();

        manaBarOriginalSize = rectT.sizeDelta.x;
        originalManaPoolSize = player.maxMana;
    }

    // Update is called once per frame
    void Update()
    {
        if (player)
        {
            scrollBar.size = Mathf.InverseLerp(0, player.maxMana, player.currentMana);
            rectT.sizeDelta = new Vector2( manaBarOriginalSize + (manaBarSizeIncreasePerMana * (player.maxMana - originalManaPoolSize)), rectT.sizeDelta.y);

            if (infiniteManaUI)
            {
                if(player.HasInfiniteMana)
                {
                    if (infiniteManaUI.enabled == false)
                    {
                        infiniteManaUI.enabled = true;
                        manaGlowTimeStamp = Time.time;
                    }
                    infiniteManaUI.text = Mathf.Round(player.infiniteManaTimeRemaining).ToString();
                    if (glowImage) glowImage.color = Color.Lerp(Color.black, manaBarGlowColour, Mathf.InverseLerp(manaGlowTimeStamp, manaGlowTimeStamp + manaGlowFadeTime, Time.time));


                }
                else 
                {
                    if (infiniteManaUI.enabled == true)
                    {
                        infiniteManaUI.enabled = false;
                        manaGlowTimeStamp = Time.time;
                    }

                    if (glowImage) glowImage.color = Color.Lerp(manaBarGlowColour, Color.black, Mathf.InverseLerp(manaGlowTimeStamp, manaGlowTimeStamp + manaGlowFadeTime, Time.time));
                }
            }
        }
    }
}

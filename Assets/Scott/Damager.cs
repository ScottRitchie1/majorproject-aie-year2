﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    public float damage = 10;
    public EElementType damageType;
    public bool damagePlayer = false;


    private void OnTriggerStay(Collider other)
    {
        IEntity entity = other.GetComponentInParent<IEntity>();

        if (entity != null && (damagePlayer == true || !other.GetComponentInChildren<PlayerController>()))
        {
            
            entity.InflictDamage(damage * Time.deltaTime, (int)damageType);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntity
{
    void Die();

    void InflictDamage(float damage, int damageType);

    float GetHealthPercentage();
}

public interface Elemental
{
    EElementType GetElement();
}

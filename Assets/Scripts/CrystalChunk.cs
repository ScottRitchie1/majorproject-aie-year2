﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class CrystalChunk : MonoBehaviour
{
    public static List<CrystalChunk> crystals = new List<CrystalChunk>();

    Rigidbody rb;
    Collider col;
    public Shrine myShrine = null;
    public Stealer myStealer = null;
    public PlayerController myRevive = null;
    public bool inTransit = false;
    public static float travelSpeed = 5;
    public static CrystalChunk revivingChunk;

    // Start is called before the first frame update
    Vector3 velref;
    IEnumerator statusCoroutine = null;
    public static UnityEvent OnPlayerUnrevivable = new UnityEvent();

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        crystals.Add(this);

    }


    private void OnDestroy()
    {
        StopAllCoroutines();

        if (myShrine)
        {
            myShrine.thisShrineCrystals.Remove(this);
        }
        if (myStealer)
        {
            myStealer.carriedCrystal = null;
        }

        crystals.Remove(this);
        
    }

    void Start()
    {
        myShrine = Shrine.playerShrine;
        TeatherToShrine(Shrine.playerShrine);
    }

    // Update is called once per frame
    void Update()
    {
        if (myRevive == null && myShrine == null && myStealer == null && Vector3.Distance(transform.position, Shrine.player.transform.position) < 2.5f)
        {
            TeatherToShrine(Shrine.playerShrine);
        }
    }

    public void DropChunk()
    {
        col.enabled = true;
        if (statusCoroutine != null)
        {
            StopCoroutine(statusCoroutine);
        }

        if (myShrine)
        {
            myShrine.thisShrineCrystals.Remove(this);
            myShrine = null;
        }

        if (myStealer)
        {
            transform.SetParent(null);
            myStealer.carriedCrystal = null;
            myStealer = null;
        }

        rb.isKinematic = false;
        inTransit = false;
    }

    public void PickedUpByStealer(Stealer stealer)
    {
        DropChunk();
        if (statusCoroutine != null)
        {
            StopCoroutine(statusCoroutine);
        }

        statusCoroutine = updateStealerCoroutine(stealer);
        StartCoroutine(statusCoroutine);
        col.enabled = false;
    }


    public void TeatherToShrine(Shrine shrine)
    {
        DropChunk();
        col.enabled = false;
        if (statusCoroutine != null)
        {
            StopCoroutine(statusCoroutine);
        }


        statusCoroutine = updateShrineCoroutine(shrine);
        StartCoroutine(statusCoroutine);
    }

    public static void RevivePlayer(PlayerController player)
    {
        CrystalChunk crystal = (Shrine.playerShrine.thisShrineCrystals.Count > 0 ? Shrine.playerShrine.thisShrineCrystals[0] : null);
        if (crystal && player)
        {
            crystal.DropChunk();
            crystal.col.enabled = false;
            if (crystal.statusCoroutine != null)
            {
                crystal.StopCoroutine(crystal.statusCoroutine);
            }


            crystal.statusCoroutine = crystal.updateReviveCoroutine(player);
            crystal.StartCoroutine(crystal.statusCoroutine);

        }
        else
        {
            if(OnPlayerUnrevivable != null)
            {
                OnPlayerUnrevivable.Invoke();
            }
            //game over
        }
    }

    public IEnumerator updateShrineCoroutine(Shrine shrine)
    {

        myShrine = shrine;
        rb.isKinematic = true;

        myShrine.thisShrineCrystals.Add(this);
        float distance = Random.Range(shrine.radiusRange.x, shrine.radiusRange.y);
        float speed = Random.Range(shrine.rotationSpeedRange.x, shrine.rotationSpeedRange.y);
        float height = Random.Range(shrine.heightRange.x, shrine.heightRange.y);
        Vector3 rotationAxis = new Vector3(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));

        List<Vector3> points = new List<Vector3>();
        NavMeshPath path = new NavMeshPath();
        NavMeshQueryFilter filter = new NavMeshQueryFilter();
        filter.agentTypeID = CrystalChunkNavMesh.navmesh.agentTypeID; //1479372276;
        filter.areaMask =  NavMesh.AllAreas;

        NavMeshHit p1;
        NavMeshHit p2;
        

            

        if (NavMesh.SamplePosition(transform.position, out p1, float.MaxValue, filter) &&
            NavMesh.SamplePosition(myShrine.transform.position, out p2, float.MaxValue, filter) &&
            NavMesh.CalculatePath(p1.position, p2.position, filter, path))
        {
            points = new List<Vector3>(path.corners);
            
            Vector3 direction = (new Vector3(points[points.Count-1].x, 0, points[points.Count - 1].z) - new Vector3(shrine.transform.position.x, 0, shrine.transform.position.z)).normalized;
            points.Add(shrine.transform.position + (Vector3.up * height) + (direction * distance));
        }



        while (true)
        {
            if(myShrine == null)
            {
                DropChunk();
                break;
            }

            Vector3 direction = (new Vector3(transform.position.x, 0, transform.position.z) - new Vector3(shrine.transform.position.x, 0, shrine.transform.position.z)).normalized;
            if (points.Count == 0 || Vector3.Distance(transform.position, myShrine.transform.position) < distance * 3)
            {
                inTransit = false;
                transform.position = Vector3.MoveTowards(transform.position, shrine.transform.position + (Vector3.up * height) + (direction * distance), Time.deltaTime * travelSpeed);
                transform.RotateAround(shrine.transform.position, Vector3.up, speed * Time.deltaTime);
            }
            else
            {
                    
                 //= (true : hit.point ? points[0]);
                transform.position = Vector3.MoveTowards(transform.position, points[0] + Vector3.up * 3, Time.deltaTime * travelSpeed);
                if(Vector3.Distance(transform.position, points[0] + Vector3.up * 3) < 1)
                {
                    points.RemoveAt(0);
                }


                inTransit = true;
            }
            transform.Rotate(rotationAxis * Time.deltaTime);

            yield return null;
        }
    }

    public IEnumerator updateStealerCoroutine(Stealer stealer)
    {
        rb.isKinematic = true;
        myStealer = stealer;
        while (true)
        {
            if(stealer == null || stealer.GetHealthPercentage() <= 0)
            {
                DropChunk();
                break;
            }

            transform.position = Vector3.MoveTowards(transform.position, stealer.crystalHoldPosition.position, Time.deltaTime * travelSpeed);

            inTransit = true;
            if (Vector3.Distance(transform.position, stealer.crystalHoldPosition.transform.position) < 0.1f)
            {
                inTransit = false;
                stealer.carriedCrystal = this;
                transform.SetParent(stealer.crystalHoldPosition);
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
                break;
            }

            yield return null;
        }
    }

    public IEnumerator updateReviveCoroutine(PlayerController player)
    {
        revivingChunk = this;
        Vector3 rotationAxis = new Vector3(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));

        rb.isKinematic = true;
        myRevive = player;

        List<Vector3> points = new List<Vector3>();
        NavMeshPath path = new NavMeshPath();
        NavMeshQueryFilter filter = new NavMeshQueryFilter();
        filter.agentTypeID = CrystalChunkNavMesh.navmesh.agentTypeID; //1479372276;
        filter.areaMask = NavMesh.AllAreas;

        NavMeshHit p1;
        NavMeshHit p2;

        if (NavMesh.SamplePosition(transform.position, out p1, float.MaxValue, filter) &&
            NavMesh.SamplePosition(myRevive.transform.position, out p2, float.MaxValue, filter) &&
            NavMesh.CalculatePath(p1.position, p2.position, filter, path))
        {
            for(int i = 0; i < path.corners.Length; i++)
            {
                points.Add(path.corners[i] + Vector3.up * 3);
            }
            points.Add(myRevive.transform.position);
        }

        while (true)
        {
            if (myRevive == null)
            {
                DropChunk();    
                break;
            }

            inTransit = true;

            transform.position = Vector3.MoveTowards(transform.position, points[0], Time.deltaTime * travelSpeed);
            if (Vector3.Distance(transform.position, points[0]) < 1)
            {
                
                points.RemoveAt(0);
                if(points.Count == 0)
                {
                    inTransit = false;
                    myRevive.Revive();
                    Destroy(gameObject);
                    break;
                }
            }

            transform.Rotate(rotationAxis * Time.deltaTime);

            yield return null;
        }

        revivingChunk = null;
    }

    

    public static CrystalChunk GetClosestCrystal(Vector3 position)
    {
        CrystalChunk closestChunk = null;
        foreach (CrystalChunk chunk in crystals)
        {
            if (chunk.inTransit == false && (chunk.myShrine == Shrine.playerShrine || chunk.myShrine == null) && chunk.myStealer == null && chunk.myRevive == null)
            {
                if (closestChunk == null)
                {
                    closestChunk = chunk;
                }
                else if (Vector3.Distance(position, closestChunk.transform.position) > Vector3.Distance(chunk.transform.position, position))
                {
                    closestChunk = chunk;
                }
            }
        }

        return closestChunk;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
[RequireComponent(typeof(Targetable))]
[RequireComponent(typeof(Collider))]
public class AttackerEgg : MonoBehaviour, IEntity
{
    Targetable targetable;
    AudioSource audioSource;
    public static AttackerEgg fireEgg;
    public static AttackerEgg iceEgg;
    public static AttackerEgg EarthEgg;
    Shrine myElementShrine = null;

    [Header("General")]
    public EElementType elementType;
    [Min(0)]
    public float health = 10;
    float currentHealth = 0;
    [Min(0)]
    public int requiredStealersToSpawnAttacker = 10;
    int currentAmount = 0;

    [Header("Other")]
    public Transform damageNumberPoint = null;

    float startScale = 0;// the sized scaled depending on how many stealers a consumbed
    float targetScale = 0;

    NavMeshQueryFilter filter;// the filter used to sample the navmesh to find a point to create the egg

    public UnityEvent OnEggReset = new UnityEvent();// called when this egg is reset
    public static StaticTakeDamageEvent OnTakeDamageStaticEvent = new StaticTakeDamageEvent();// take damage static event called when the egg takes damage

    [Space]
    public AudioClip onConsumeSound;
    public ParticleSystem onConsumeParticle;
    [Space]
    public AudioClip onEggDieSound;
    public ParticleSystem OnEggDieParticle;
    [Space]
    public AudioClip onEggHatchSound;
    public ParticleSystem onEggHatchParticle;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        startScale = transform.localScale.y;// gets our starting scale
        transform.localScale = Vector3.zero;
        currentHealth = health;
        targetable = GetComponent<Targetable>();
        targetable.targetElement = elementType;
        targetable.enabled = false;
        if(elementType == EElementType.FIRE && fireEgg == null)
        {
            fireEgg = this;
        }else if (elementType == EElementType.WATER && iceEgg == null)
        {
            iceEgg = this;
        }
        else if (elementType == EElementType.EARTH && EarthEgg == null)
        {
            EarthEgg = this;
        }

        myElementShrine = Shrine.GetShrineByElement(elementType);// gets our shrine

        filter = new NavMeshQueryFilter();//setup the navmesh filters
        filter.areaMask = NavMesh.AllAreas;
        if (myElementShrine)
        {
            filter.agentTypeID = myElementShrine.attacker.GetComponent<NavMeshAgent>().agentTypeID;
        }

        Invoke("ResetEgg", 1);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = Vector3.one * Mathf.MoveTowards(transform.localScale.y, targetScale, Time.deltaTime);// scale our size depending on how many stealers are in the gg
    }
    [ContextMenu("IncreaseTally")]
    public void ConsumeStealer()//a stealer is consumed
    {
        currentAmount++;
        targetable.enabled = true;// make us targetable
        targetScale = Mathf.Lerp(0, startScale, Mathf.InverseLerp(0, requiredStealersToSpawnAttacker, currentAmount+1));

        if(currentAmount >= requiredStealersToSpawnAttacker)// if we reached the threshhold spawn an attacker and reset our egg
        {
            Hatch();
        }

        if (audioSource && onConsumeSound) audioSource.PlayOneShot(onConsumeSound);
        if (onConsumeParticle) onConsumeParticle.Play();
    }
    public void Hatch()// spawn an attacker
    {
        if (myElementShrine)// if we found our shrine spawn attacker
        {
            Attacker attacker = Instantiate(myElementShrine.attacker, transform.position, transform.rotation);
        }
        DetachShell(true);

        if (audioSource && onEggHatchSound) Instantiate(audioSource.gameObject, audioSource.transform.position, audioSource.transform.rotation, null).GetComponent<AudioSource>().PlayOneShot(onEggHatchSound);
        if (onEggHatchParticle) Instantiate(onEggHatchParticle, onEggHatchParticle.transform.position, onEggHatchParticle.transform.rotation, null).Play();
        ResetEgg();// reset our egg
    }

    [ContextMenu("Die")]
    public void Die()// if the egg dies
    {
        currentHealth = 0;
        DetachShell(false);
        if (OnEggDieParticle) Instantiate(OnEggDieParticle, OnEggDieParticle.transform.position, OnEggDieParticle.transform.rotation, null).Play();
        if (audioSource && onEggDieSound) Instantiate(audioSource.gameObject, audioSource.transform.position, audioSource.transform.rotation, null).GetComponent<AudioSource>().PlayOneShot(onEggDieSound);

        ResetEgg();
    }

    public void InflictDamage(float damage, int damageType)
    {
        bool wasEffective = false;
        if ((damageType == 0 && (int)elementType == 2) || damageType - 1 == (int)elementType)
        {
            wasEffective = true;
            //double damage
            damage *= 3.0f;
        }


        currentHealth -= damage;

        if (OnTakeDamageStaticEvent != null)
        {
            OnTakeDamageStaticEvent.Invoke((damageNumberPoint ? damageNumberPoint : transform), damage, (EElementType)damageType, wasEffective);
        }

        if (currentHealth <= 0)// if we died becasue of the damage
        {
            Die();
        }
    }

    void DetachShell(bool shouldExplode = false)
    {
        if (targetScale > 0.1f)// if our size is big enough to spawn chunks
        {
            foreach (Rigidbody r in GetComponentsInChildren<Rigidbody>(true))
            {
                GameObject shell = Instantiate(r.gameObject, r.transform.position, r.transform.rotation, transform);//copy our shell
                shell.gameObject.transform.SetParent(null);//unset parent, we cant do this during instantiation becasue we want to keep the scale
                shell.gameObject.SetActive(true);// eanble
                shell.GetComponent<ScaleCurvesController>().GraphTimeMultiplier *= Random.Range(0.9f, 1.1f);//candomize despawn values
                if(shouldExplode)
                    shell.GetComponent<Rigidbody>().AddExplosionForce(20, transform.position + Vector3.up, 10);//add explosive foprce
            }
        }
    }

    public float GetHealthPercentage()
    {
        return Mathf.InverseLerp(0, health, currentHealth);
    }

    public static AttackerEgg GetEggByElement(EElementType elementType)// gets the statioc egg using an element
    {
        return (elementType == EElementType.FIRE ? AttackerEgg.fireEgg : (elementType == EElementType.WATER ? AttackerEgg.iceEgg : AttackerEgg.EarthEgg));
    }

    public void ResetEgg()//resets the egg after an attacker has spawned or its destoyed
    {
        targetable.enabled = false;// we cant be targetable becasue its so small - this is reenabled when we consume a stealer
        currentHealth = health;
        currentAmount = 0;
        targetScale = 0;//reset our scale
        transform.localScale = Vector3.zero;

        if (myElementShrine) {
            Vector2 randomPoint = Random.insideUnitCircle * myElementShrine.eggSpawnRadius;
            NavMeshHit navHit;
            if(NavMesh.SamplePosition(myElementShrine.eggSpawn.position + new Vector3(randomPoint.x, 0, randomPoint.y), out navHit, float.MaxValue, filter))
            {
                //calculate and sample a new point that stealers can reach on the navmesh and respawn the egg there.
                transform.position = navHit.position;
            }
        }

        if(OnEggReset != null)// call egg reset - this is so stealersc an crecalculate their paths
            OnEggReset.Invoke();
    }
}

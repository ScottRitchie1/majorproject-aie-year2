﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class StaticTakeDamageEvent : UnityEvent<Transform, float, EElementType, bool>
{
}
public class StaticOnDieEvent : UnityEvent<GameObject>
{
}
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Collider))]
public class Stealer : MonoBehaviour, IEntity, Elemental
{
    static PlayerController player;// static reference to the player
    Animator anim;
    NavMeshAgent agent;
    Shrine myShrine = null;// the stealers shrine
    AudioSource audioSource;
    [HideInInspector]
    public CrystalChunk carriedCrystal = null; // the crystal were carrying;
    IEnumerator currentBehaviour = null;

    [Header("General")]
    public EElementType elementType;// the element type of this stealer
    public float maxHealth = 3;// the max health opf the stealer
    float currentHealth = 0;
    public bool IsDead{get{return currentHealth <= 0;}}

    [Header("Movement")]
    public float runSpeed = 5;// move speed without crystal
    public float walkWithCrystalSpeed = 2;// move speed with crystal


    [HideInInspector]
    public int groupSeed = 0;// used the seed random to generate varying paths for the enemies to go

    [Header("Attacking")]
    public float agroRange = 5;// the range we attack the player instead of go for a crystal
    public float damageRange = 1;// the range we can actually damage the player
    public float damageDelay = 0.5f;// the delay we have whiloe we play our attack animation
    
    public float attackDamage = 5;// the damage of the attack
    public float attackCooldown = 1.5f;// the time we must wait to attack again

    public AudioSource attackAudioSource;
    public List<AudioClip> attackImpactAudioClip = new List<AudioClip>();


    [Header("Other")]
    public float takeCrystalRange = 2;// range we can pickup cruystal from
    public float depositCrystalRange = 2;// range we can pickup cruystal from

    public Transform crystalHoldPosition;// the holding position of the stealer for the crystal chunk
    [Space]
    public bool alwaysSeekEgg = false;// should we always seed the egg
    bool recalculateEggPath = true;// if the eggs position has changed, we must recalculate the path
    AttackerEgg egg;// our egg
    public Transform damageNumberPoint = null;// the position damage numbers are displayed from
    int[] navmeshRouteCostIndexes = new int[2];// the index of the layers for thye current routes the enemy will take

    public static StaticTakeDamageEvent OnTakeDamageStaticEvent = new StaticTakeDamageEvent();// the static event thaqt is called when a stealer takes damage
    public static StaticOnDieEvent OnDieEvent = new StaticOnDieEvent();// the static event thaqt is called when a stealer takes damage

    [Space]
    public AudioSource otherAudio;
    public List<AudioClip> hitReactionAudio = new List<AudioClip>();

    [Space]
    public List<AudioClip> onDieSound = new List<AudioClip>();
    public ParticleSystem onDieParticle;
    private void Start()
    {
        if (!player)
        {
            player = FindObjectOfType<PlayerController>();
        }
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        currentHealth = maxHealth;

        //configure and get the attacker egg
        if (egg == null) {
            egg = AttackerEgg.GetEggByElement(elementType);
            if (egg)
            {
                egg.OnEggReset.AddListener(OnEggRecalculateEggPath);
                Physics.IgnoreCollision(GetComponent<Collider>(), egg.GetComponent<Collider>(), true);
            }
        }

        // set our behaviour
        if (currentBehaviour == null)
        {
            currentBehaviour = seekCrystalBehaviour();
            StartCoroutine(currentBehaviour);
        }

        //get our shrine
        if (myShrine == null)
        {
            myShrine = Shrine.GetShrineByElement(elementType);
            if (myShrine)
                myShrine.OnShrineDie.AddListener(Die);
        }

        if (carriedCrystal)
            agent.avoidancePriority = 1;
        else
            agent.avoidancePriority = 10;

        GenerateNewRandomRoute();// generate a new random route

        if (GetComponent<ScaleCurvesController>()) GetComponent<ScaleCurvesController>().enabled = false;
    }

    IEnumerator seekPlayerBehaviour()// attack player
    {
        anim.SetBool("attacking", true);
        float timeToAttack = 0;
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        ShouldUseRoutes(false);// dont use pathing routes
        while (true)
        {
            if (IsDead)
            {
                break;
            }
            distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);// get the diatcne to the player

            if(distanceToPlayer > agroRange || player.currentHealth <= 0)// if the player is out of range or dead, return to seeking crystal
            {
                currentBehaviour = seekCrystalBehaviour();
                StartCoroutine(currentBehaviour);
                break;
            }

            agent.SetDestination(player.transform.position);//track the player


            if (timeToAttack <= 0)// if we can attack and in range
            {
                if (distanceToPlayer < damageRange)
                {
                    anim.SetTrigger("attack");// play the animation
                    yield return new WaitForSeconds(damageDelay);
                    distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);// get the diatcne to the player
                    if (distanceToPlayer < damageRange)// wait for the attack delay, and if were still in range, damage the player
                    {
                        if (attackAudioSource && attackImpactAudioClip.Count > 0) attackAudioSource.PlayOneShot(attackImpactAudioClip[Random.Range(0, attackImpactAudioClip.Count-1)]);
                            player.InflictDamage(attackDamage, (int)elementType);
                        timeToAttack = attackCooldown;
                    }
                }
            }
            else
            {
                timeToAttack -= Time.deltaTime;
            }
            yield return null;

        }

        anim.SetBool("attacking", false);
    }

    IEnumerator seekCrystalBehaviour()// enemy should go to the crystal
    {
        anim.SetBool("hasCrystal", false);
        bool shrineIsCloser = false;// should we seek the shrine
        agent.speed = runSpeed;
        ShouldUseRoutes(true);// use our pathing routes


        while (true)
        {
            if (IsDead)
            {
                break;
            }

            if(Vector3.Distance(transform.position, player.transform.position) < agroRange && player.currentHealth > 0)// if the player is in agro range and alive attack the player
            {
                currentBehaviour = seekPlayerBehaviour();
                StartCoroutine(currentBehaviour);
                break;
            }


            if(carriedCrystal == null)
            {
                CrystalChunk closestChunk = CrystalChunk.GetClosestCrystal(transform.position);// get the closest crystal

                if (closestChunk && alwaysSeekEgg == false)// if we have a chunk and should be forming eggs
                {

                    if (closestChunk.myShrine)// if the shrine is the has the closest crystal
                    {
                        if(shrineIsCloser == false)// only update our position once to the shrine
                        {
                            shrineIsCloser = true;
                            agent.SetDestination(closestChunk.myShrine.transform.position);
                        }
                        
                    }
                    else
                    {
                        shrineIsCloser = false;// update our position to the chunk
                        agent.SetDestination(closestChunk.transform.position);
                    }

                    // if we have a path, and we have arrived at the chunk/shrine
                    if (agent.pathPending == false && ( (shrineIsCloser == false && agent.remainingDistance <= takeCrystalRange) || (shrineIsCloser == true && Vector3.Distance(closestChunk.myShrine.transform.position, transform.position) < takeCrystalRange)))
                    {
                        agent.SetDestination(transform.position);// stop moving
                        closestChunk.PickedUpByStealer(this);// pickup the crystal and seek home                        
                        currentBehaviour = seekHomeWithCrystal();
                        StartCoroutine(currentBehaviour);
                        break;
                    }

                }
                else //  if we dont have a crystal to get lets form eggs
                {
                    currentBehaviour = seekEggBehaviour();
                    StartCoroutine(currentBehaviour);
                    break;
                }
            }

            yield return null;
        }
    }

    IEnumerator seekHomeWithCrystal()// seek home if we have a chunk
    {
        anim.SetTrigger("pickUpCrystal");
        ShouldUseRoutes(false);// go straight home, dont use paths
        agent.speed = walkWithCrystalSpeed;
        while (carriedCrystal == null)// wait untill we have a crystal in our hands before we start moving
        {
            yield return null;
        }
        anim.SetBool("hasCrystal", true);
        agent.avoidancePriority = 1;// we want to prioritize the mvoement of stealers with chunks
        if (myShrine)
        {
            agent.SetDestination(myShrine.transform.position);// set destination to home

            while (true)
            {
                if (IsDead)
                {
                    break;
                }

                if (agent.pathPending == false && agent.remainingDistance <= depositCrystalRange)// if we reached home
                {
                    carriedCrystal.TeatherToShrine(myShrine);// deposit the crystal to our base
                    agent.avoidancePriority = 10;// we shouldnt prioritize this stealer any more since it doesnt have a cyrstal
                    GenerateNewRandomRoute();// generate a new route for it to take
                    currentBehaviour = seekCrystalBehaviour();// seek crystal once again
                    StartCoroutine(currentBehaviour);
                    break;
                }
                yield return null;
            }
        }

    }

    IEnumerator seekEggBehaviour()// seek the egg
    {
        if (!agent)
        {
            yield break;
        }
        ShouldUseRoutes(true);// use routes to get tot the egg

        recalculateEggPath = true;// do we need to recalculate the path for the egg

        while (true)
        {
            if (IsDead)
            {
                break;
            }

            if (Vector3.Distance(transform.position, player.transform.position) < agroRange && player.currentHealth > 0)// if the player is in agro range and alive attack the player
            {
                currentBehaviour = seekPlayerBehaviour();
                StartCoroutine(currentBehaviour);
                break;
            }

            if (Shrine.playerShrine.thisShrineCrystals.Count > 0 && alwaysSeekEgg == false)// if we have cr5ystals to get, go get em
            {
                yield return new WaitForSeconds(1);
                if (Shrine.playerShrine.thisShrineCrystals.Count > 0)
                {
                    currentBehaviour = seekCrystalBehaviour();
                    StartCoroutine(currentBehaviour);
                    break;
                }
            }



            if (agent.pathPending == false)
            {
                if (recalculateEggPath)// if we need to recalculate the egg path ie: the egg created an attacker and has moved/respawned
                {
                    agent.SetDestination(egg.transform.position);
                    recalculateEggPath = false;
                    yield return null;
                    continue;
                }

                if (agent.remainingDistance <= 1.0f)// if we have reached the egg
                {
                    egg.ConsumeStealer();// consume the stealer
                    Die();
                    Destroy(gameObject);
                }
            }

            yield return null;
        }
    }
    void GenerateNewRandomRoute()// this generates a new set of routes to use when pathing
    {
        if (myShrine)
        {
            Random.State state = Random.state;
            Random.InitState(groupSeed);// seed random so all groups are the same
            groupSeed = (int)(Random.value * int.MaxValue);

            int initilRoute = Random.Range(0, myShrine.routes.Count);// randomly calculate our paths from the shrines avaliable route options
            navmeshRouteCostIndexes[0] = (int)myShrine.routes[initilRoute].route;
            navmeshRouteCostIndexes[1] = (int)myShrine.routes[initilRoute].routes[Random.Range(0, myShrine.routes[initilRoute].routes.Count)];

            ShouldUseRoutes(true);


            Random.state = state;
        }
    }

    void ShouldUseRoutes(bool useRoutes)// updates the ai to either use or not use the routes
    {
        int areaCostForNonZones = 100;

        for (int i = 3; i < 8; i++)
        {
            if (i == navmeshRouteCostIndexes[0] || i == navmeshRouteCostIndexes[1] || useRoutes == false)
            {
                agent.SetAreaCost(i, NavMesh.GetAreaCost(i));
            }
            else
            {
                agent.SetAreaCost(i, areaCostForNonZones);
            }
        }
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.1f);
        Gizmos.DrawSphere(transform.position, damageRange);
        Gizmos.color = new Color(1, 0.92f, 0.016f, 0.1f);
        Gizmos.DrawSphere(transform.position, agroRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, takeCrystalRange);
    }
    

   

    public void OnEggRecalculateEggPath()// just so we can call to update our recalculate egg path from the egg
    {
        recalculateEggPath = true;
    }
    public virtual void Die()// if we die
    {
        if (agent.enabled)
        {
            currentHealth = 0;
            anim.SetTrigger("dead");
            agent.enabled = false;
            GetComponent<Collider>().enabled = false;
            if (onDieParticle) onDieParticle.Play();
            if (audioSource && onDieSound.Count > 0) audioSource.PlayOneShot(onDieSound[Random.Range(0, onDieSound.Count-1)]);
            if (carriedCrystal)// if were carrying a crystal drop it
            {
                carriedCrystal.DropChunk();
            }

            if(egg)
                egg.OnEggReset.RemoveListener(OnEggRecalculateEggPath);// we no longer care about eggs

            if (OnDieEvent != null)
                OnDieEvent.Invoke(gameObject);


            if (myShrine)// we no longer care if the shrine dies
                myShrine.OnShrineDie.RemoveListener(Die);

            if (GetComponent<ScaleCurvesController>()) GetComponent<ScaleCurvesController>().enabled = true;
        }
     
    }

    public void InflictDamage(float damage, int damageType)
    {
    
        bool wasEffective = false;
        if ((damageType == 0 && (int)elementType == 2) || damageType - 1 == (int)elementType)// we used the effective element so the damage is increased
        {
            //double damage
            damage *= 3.0f;
            wasEffective = true;

        }

        currentHealth -= damage;// reduce our health

        if(OnTakeDamageStaticEvent != null)// call the static on damaged event (used for damage numbers)
        {
            OnTakeDamageStaticEvent.Invoke((damageNumberPoint? damageNumberPoint : transform), damage, (EElementType)damageType, wasEffective);
        }

        if (currentHealth <= 0)// check if we die
        {
            Die();
        }
        else
        {
            if (otherAudio && hitReactionAudio.Count > 0)
            {
                otherAudio.PlayOneShot(hitReactionAudio[Random.Range(0, hitReactionAudio.Count - 1)]);
            }
        }
    }

    public float GetHealthPercentage()
    {
        return Mathf.InverseLerp(0, maxHealth, currentHealth);
    }

    public EElementType GetElement()
    {
        return elementType;
    }

}

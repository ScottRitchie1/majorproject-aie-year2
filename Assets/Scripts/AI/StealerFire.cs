﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealerFire : Stealer
{
    public new Renderer renderer;

    public override void Die()
    {
        base.Die();

        if (renderer)
        {
            renderer.materials[1].SetColor("_EmissionColour", Color.black);
            renderer.materials[1].SetFloat("Vector1_2945E47B", 1);

        }
    }
}

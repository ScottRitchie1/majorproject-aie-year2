﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class WaveManager : MonoBehaviour
{
    public enum E_WaveType { Random, Fire, Water, Earth };
    public enum E_ShrineEvent { Inactive, Fire, Water, Earth };
    static E_ShrineEvent currentShrineEvent = E_ShrineEvent.Inactive;// the event thats currently active

    public static E_ShrineEvent CurrentShrineEvent
    {
        get {
            return currentShrineEvent;
        }
    }


    [Min(1)]
    public float shrineEventActiveTime = 45;// the time an event is active for
    [Min(1)]
    public float shrineEventSpawnScailer = 2;// the amount of stealers that spawn during a shrine event

    float shrineEventStartTimeStamp;

    public float ShrineEventRemainingActiveTime {
        get
        {
            return shrineEventStartTimeStamp + shrineEventActiveTime + 2 - Time.time;
        }
    }

    public float bonusSpawnPercentagePerKilledShrine = 0.25f;// the amount of extra stealers that will spawn per shreine destroyed
    int currentWave = 0;// current wave/interval
    int currentInterval = 0;
    public int CurrentWave
    {
        get
        {
            return currentWave;
        }
    }

    

    [System.Serializable]
    public class Wave// a wave: consists of intervals
    {
        [Min(0)]
        public float intervalTime = 30;// the time between each interval
        [Min(0)]
        public float waveTime = 60;// the time between the last interval of this wave, and the first interval of the next wave
        public List<WaveInterval> intervals = new List<WaveInterval>();
    }
    [System.Serializable]
    public class WaveInterval// an interval in a wave
    {
        public E_WaveType waveType;// the type of enemy that will spawn during this wave
        [Min(0)]
        public Vector2Int baseEnemyAmount = new Vector2Int();// the random amount of enemies that will spawn by default
        [Range(0,100)]
        public float firstDeviationChance = 0;// the chance that a group of enemies from a different shrine will also spawn this wave
        [Min(0)]
        public Vector2Int firstDeviationEnemyAmount = new Vector2Int();// the amount of enemies that will spawn from another shrine
        [Range(0, 100)]
        public float secondDeviationChance = 0;// the chance for a second lot of enemies that will spawn from the final shrine
        [Min(0)]
        public Vector2Int secondDeviationEnemyAmount = new Vector2Int();
        [Range(0, 100)]
        public float eggWaveChance = 0;// the chance that enemies will spawn purly to create an attacker egg
        [Min(0)]
        public Vector2Int eggWaveEnemyAmount = new Vector2Int();// the amopunt that will spawn to create the egg
    }

    public List<Wave> waves = new List<Wave>();// list of waves

    Task waveManagerCoroutine = null;

    private void Awake()
    {
        waveManagerCoroutine = new Task(waveCoroutine());
    }

    private void Start()
    {
        Shrine.OnPlayerShrineDie.AddListener(StopSpawning);
        Shrine.OnEnemyShrinesDie.AddListener(StopSpawning);
    }

    public IEnumerator waveCoroutine()
    {
        currentWave = 0;

        while (currentWave < waves.Count)
        {
            // new wave
            currentInterval = 0;// reset interval
            E_WaveType waveType;
            while (currentInterval < waves[currentWave].intervals.Count)
            {
                // new interval
                List<E_WaveType> possibleWaveDeviations = new List<E_WaveType>();// this checks alive shrines to determine deviations in spawn from other alive shrines
                if(Shrine.fireShrine.currentHealth > 0)
                {
                    possibleWaveDeviations.Add(E_WaveType.Fire);
                }

                if(Shrine.earthShrine.currentHealth > 0)
                {
                    possibleWaveDeviations.Add(E_WaveType.Earth);
                }

                if (Shrine.iceShrine.currentHealth > 0)
                {
                    possibleWaveDeviations.Add(E_WaveType.Water);
                }

                // get the element wave
                waveType = (waves[currentWave].intervals[currentInterval].waveType == E_WaveType.Random ? possibleWaveDeviations[Random.Range(0, possibleWaveDeviations.Count)] : waves[currentWave].intervals[currentInterval].waveType);
                // remove this element type from the possible wave deviations
                possibleWaveDeviations.Remove(waveType);

                //calculate the random amount of enemies to spawn and spawn them
                int spawnAmount = Random.Range(waves[currentWave].intervals[currentInterval].baseEnemyAmount.x, waves[currentWave].intervals[currentInterval].baseEnemyAmount.y);
                SpawnEnemies(spawnAmount, waveType);



                //Egg Wave
                if(Random.Range(0, 100) < waves[currentWave].intervals[currentInterval].eggWaveChance)// check if the egg wave triggers
                {
                    // spawn a random amount of enemies that will form an egg.
                    spawnAmount = Random.Range(waves[currentWave].intervals[currentInterval].eggWaveEnemyAmount.x, waves[currentWave].intervals[currentInterval].eggWaveEnemyAmount.y);
                    SpawnEnemies(spawnAmount, waveType, true);
                }

                if (possibleWaveDeviations.Count > 0 && Random.Range(0,100) < waves[currentWave].intervals[currentInterval].firstDeviationChance)// check if the first deviation is triggered
                {
                    // remove this element type from the possible deviations
                    waveType = possibleWaveDeviations[Random.Range(0, possibleWaveDeviations.Count)];
                    possibleWaveDeviations.Remove(waveType);

                    //spawn a random amount of enemies of the new deviation element type
                    spawnAmount = Random.Range(waves[currentWave].intervals[currentInterval].firstDeviationEnemyAmount.x, waves[currentWave].intervals[currentInterval].firstDeviationEnemyAmount.y);
                    SpawnEnemies(spawnAmount, waveType);

                    if (possibleWaveDeviations.Count > 0 && Random.Range(0, 100) < waves[currentWave].intervals[currentInterval].secondDeviationChance)// check if the last deviation is triggered
                    {
                        waveType = possibleWaveDeviations[0];// it wil always be the last remaining element type in the list

                        // spawn a random amount of enemies of that element type
                        spawnAmount = Random.Range(waves[currentWave].intervals[currentInterval].secondDeviationEnemyAmount.x, waves[currentWave].intervals[currentInterval].secondDeviationEnemyAmount.y);
                        SpawnEnemies(spawnAmount, waveType);
                    }
                }

                


                // at the end of the wave, wait for a cool down before starting a new wave interval
                yield return new WaitForSeconds(waves[currentWave].intervalTime);
                while(currentShrineEvent != E_ShrineEvent.Inactive)// also wait if we are currently in a shrine event
                {
                    yield return null;
                }
                currentInterval++;
            }

            // at the end of the whole wave, wait for a new wave to start
            yield return new WaitForSeconds(waves[currentWave].waveTime);
            while (currentShrineEvent != E_ShrineEvent.Inactive)// again wait if we are currently in a shrine event
            {
                yield return null;
            }
            currentWave++;
        }

        //win game?
    }

    public IEnumerator eventCoroutine(EElementType eElementType)
    {
        currentShrineEvent = (E_ShrineEvent)((int)eElementType + 1);// the current shrine event element
        Shrine shrine = null;
        shrineEventStartTimeStamp = Time.time;
        if (eElementType == EElementType.FIRE)// if the shrine event element was fire
        {
            shrine = Shrine.fireShrine;

            //stop spawning attackers from other shrines
            Shrine.earthShrine.canSpawnAttackers = false;
            Shrine.iceShrine.canSpawnAttackers = false;

            if (Shrine.iceShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
            {
                ParticleSystem.EmissionModule em = Shrine.iceShrine.shrinePassiveParticleSystem.emission;
                em.enabled = false;
            }

            if (Shrine.earthShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
            {
                ParticleSystem.EmissionModule em = Shrine.earthShrine.shrinePassiveParticleSystem.emission;
                em.enabled = false;
            }

            //kill other enemies from their shrines
            if (Shrine.earthShrine.OnShrineDie != null)
                Shrine.earthShrine.OnShrineDie.Invoke();
            if (Shrine.iceShrine.OnShrineDie != null)
                Shrine.iceShrine.OnShrineDie.Invoke();
        }else if(eElementType == EElementType.WATER)// same for water
        {
            shrine = Shrine.iceShrine;
            Shrine.earthShrine.canSpawnAttackers = false;
            Shrine.fireShrine.canSpawnAttackers = false;

            if (Shrine.fireShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
            {
                ParticleSystem.EmissionModule em = Shrine.fireShrine.shrinePassiveParticleSystem.emission;
                em.enabled = false;
            }

            if (Shrine.earthShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
            {
                ParticleSystem.EmissionModule em = Shrine.earthShrine.shrinePassiveParticleSystem.emission;
                em.enabled = false;
            }
            if (Shrine.earthShrine.OnShrineDie != null)
                Shrine.earthShrine.OnShrineDie.Invoke();
            if (Shrine.fireShrine.OnShrineDie != null)
                Shrine.fireShrine.OnShrineDie.Invoke();
        }
        else// same for earth
        {
            shrine = Shrine.earthShrine;
            Shrine.fireShrine.canSpawnAttackers = false;
            Shrine.iceShrine.canSpawnAttackers = false;

            if (Shrine.fireShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
            {
                ParticleSystem.EmissionModule em = Shrine.fireShrine.shrinePassiveParticleSystem.emission;
                em.enabled = false;
            }

            if (Shrine.iceShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
            {
                ParticleSystem.EmissionModule em = Shrine.iceShrine.shrinePassiveParticleSystem.emission;
                em.enabled = false;
            }
            if (Shrine.iceShrine.OnShrineDie != null)
                Shrine.iceShrine.OnShrineDie.Invoke();
            if (Shrine.fireShrine.OnShrineDie != null)
                Shrine.fireShrine.OnShrineDie.Invoke();
        }

        //Fade in our post processing for that shrines zone
        float ppeFadeTime = 1;
        float ppeFadeTimeStamp = Time.time;
        if (shrine.shrinePostProcessing != null)
        {
            shrine.shrinePostProcessing.isGlobal = true;

            while (ppeFadeTimeStamp + ppeFadeTime > Time.time)
            {
                shrine.shrinePostProcessing.weight = Mathf.InverseLerp(ppeFadeTimeStamp, ppeFadeTimeStamp + ppeFadeTime, Time.time);
                yield return null;
            }
        }

        if (shrine.shrineEventActiveParticleSystem)// if we have a shrine event particle lets start playing it
        {
            ParticleSystem.EmissionModule em = shrine.shrineEventActiveParticleSystem.emission;
            em.enabled = true;
        }

        // allow the shrine to take damage
        shrine.canDamage = true;

        // calculate the rate at which we spawn enemies and spawn them while we are in the shrine event
        if (waves.Count > 0)
        {
            float startTimeStamp = Time.time;
            float timeBetweenSpawning = ((60 / waves[currentWave].intervals[0].baseEnemyAmount.x) / shrineEventSpawnScailer);
            float lastSpawnTimeStamp = 0;
            while (startTimeStamp + shrineEventActiveTime > Time.time && shrine.currentHealth > 0)
            {
                if (Time.time > lastSpawnTimeStamp + timeBetweenSpawning)
                {
                    SpawnEnemies(1, (E_WaveType)currentShrineEvent);
                    lastSpawnTimeStamp = Time.time;
                }
                //spawning
                yield return null;

            }
        }
        else
        {
            yield return new WaitForSeconds(shrineEventActiveTime);
        }

        // the shrine event is over, dont allow us to take damage
        shrine.canDamage = false;

        if (shrine.shrinePostProcessing != null) {// if we have post processing set it back to the default state
            shrine.shrinePostProcessing.isGlobal = false;
            shrine.shrinePostProcessing.weight = 1;
        }

        if (shrine.shrineEventActiveParticleSystem)// stop playing the particle
        {
            ParticleSystem.EmissionModule em = shrine.shrineEventActiveParticleSystem.emission;
            em.enabled = false;
        }

        if (Shrine.iceShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
        {
            ParticleSystem.EmissionModule em = Shrine.iceShrine.shrinePassiveParticleSystem.emission;
            em.enabled = true;
        }

        if (Shrine.earthShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
        {
            ParticleSystem.EmissionModule em = Shrine.earthShrine.shrinePassiveParticleSystem.emission;
            em.enabled = true;
        }

        if (Shrine.fireShrine.shrinePassiveParticleSystem)// if we have a shrine event particle lets start playing it
        {
            ParticleSystem.EmissionModule em = Shrine.fireShrine.shrinePassiveParticleSystem.emission;
            em.enabled = true;
        }

        currentShrineEvent = E_ShrineEvent.Inactive;

        //end of shrine event
    }

    public void StartShrineEvent(EElementType type)// starts a shrine event
    {
        if(currentShrineEvent == E_ShrineEvent.Inactive)
        {
            StartCoroutine(eventCoroutine(type));
        }
    }

    public void SpawnEnemies(int amount, E_WaveType waveType, bool seekEgg = false)// spawn enemies
    {
        int seed = (int)(Random.value * int.MaxValue);// calculates their group seed, this is used to make them all path along a spesific path together
        Transform spawnPoint;
        Shrine spawnShrine;
        float spawnAmountMultiplierFromKilledShrines = 1;// this is the extra multiplier gained from killing shrines

        if(Shrine.fireShrine.currentHealth <= 0)
        {
            spawnAmountMultiplierFromKilledShrines += bonusSpawnPercentagePerKilledShrine;
        }

        if (Shrine.earthShrine.currentHealth <= 0)
        {
            spawnAmountMultiplierFromKilledShrines += bonusSpawnPercentagePerKilledShrine;
        }

        if (Shrine.iceShrine.currentHealth <= 0)
        {
            spawnAmountMultiplierFromKilledShrines += bonusSpawnPercentagePerKilledShrine;
        }

        amount = (int)(amount * spawnAmountMultiplierFromKilledShrines);// calculate the amount to spawn

        spawnShrine = Shrine.GetShrineByElement((EElementType)((int)waveType-1));// get the shrine to spawn them from

        if (spawnShrine.ShrineAudioSource && spawnShrine.spawnEnemiesSound) spawnShrine.ShrineAudioSource.PlayOneShot(spawnShrine.spawnEnemiesSound);
        

        for (int i = 0; i < amount; i++)//spawn that amount of enemies at their shrine
        {
            spawnPoint = spawnShrine.spawnPoints[Random.Range(0, spawnShrine.spawnPoints.Count)];//calcualte the spawn position
            

            Stealer stealer = Instantiate(spawnShrine.stealer, spawnPoint.position, spawnPoint.rotation);
            if (spawnShrine.spawnEnemiesParticle) Instantiate(spawnShrine.spawnEnemiesParticle, stealer.transform.position, stealer.transform.rotation, stealer.transform).Play();
            stealer.groupSeed = seed;//assign group seed
            stealer.alwaysSeekEgg = seekEgg;// set if they are always seeking egg or not
        }
    }

    public void StopSpawning()
    {
        waveManagerCoroutine.Stop();
    }
}

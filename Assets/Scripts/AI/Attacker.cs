﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Collider))]
public class Attacker : MonoBehaviour, IEntity, Elemental
{
    NavMeshAgent agent;
    Animator anim;
    Rigidbody rb;
    AudioSource audioSource;
    Shrine myShrine = null;
    
    static PlayerController player;
    public bool alwaysAgro = true;// should we always attack the player
    public float agroTimer = 20;// the amount of seconds we attack the player after they attack us
    public float currentAgroTimer = 0;

    [Header("General")]
    public EElementType elementType;// elemnt type of the attaker
    public float maxHealth = 100;
    public float currentHealth = 0;
    public bool IsDead { get { return currentHealth <= 0; } }

    IEnumerator currentAttackBehaviour = null;
    [Header("Ranged Attack")]
    public float rangedAttackRange = 20;// range we need to be to do a ranged attack
    public float rangedAttackWaitToSpawnProjectile = 1;//the delay after wew start the attack to when the projectile is created
    public float rangedAttackTotalTime = 2;// the total time of the attack
    public float rangedAttackCooldown = 10;// how frequent we can attack
    public Projectile projectile;// the projectile
    public Transform castPosition;// where the projectile is created

    [Header("Melee Attack")]
    public float meleeAttackRange = 2;// the range of the melee attack
    public float meleeDamage = 10;// the damage of the melee attack
    public float meleeAttackDamageWait = 1;// the time that we must wait for the damage to be applied
    public float meleeAttackTotalTime = 2;// the total time of the attack
    public float meleeAttackCoolDown = 2;// the time betweek attacks
    [Space]
    public float destroyShrineRange = 2;//range to destroy the shrine
    [Space]
    public AudioSource punchAudioSource;
    public List<AudioClip> punchImpactSound = new List<AudioClip>();


    [Header("Other")]
    public Transform damageNumberPoint = null;

    public static StaticTakeDamageEvent OnTakeDamageStaticEvent = new StaticTakeDamageEvent();
    public static StaticOnDieEvent OnDieEvent = new StaticOnDieEvent();

    [Space]
    public AudioSource otherAudio;
    public List<AudioClip> hitReactionAudio = new List<AudioClip>();

    [Space]
    public List<AudioClip> onDieSound = new List<AudioClip>();
    public ParticleSystem onDieParticle;


    void Start()
    {
        if (!player)
            player = FindObjectOfType<PlayerController>();
        currentHealth = maxHealth;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();

        if (myShrine == null)
        {
            myShrine = Shrine.GetShrineByElement(elementType);
            if (myShrine)
                myShrine.OnShrineDie.AddListener(Die);
        }

        StartCoroutine(SeekPlayerBehaviour());

        if (GetComponent<ScaleCurvesController>()) GetComponent<ScaleCurvesController>().enabled = false;
    }

    IEnumerator SeekPlayerShrineBehaviour()
    {

        if (Shrine.playerShrine)
            agent.SetDestination(Shrine.playerShrine.transform.position);

        while (true)
        {
            if (IsDead)
            {
                break;
            }
            anim.SetFloat("velocity", agent.velocity.magnitude);//update animation

            if (currentAgroTimer > 0 || alwaysAgro)// if we should attack the player
            {
                StartCoroutine(SeekPlayerBehaviour());
                break;
            }

            //seek crystal
            if (agent.pathPending == false)
            {
                if (Shrine.playerShrine)// if there is a player shrine
                {
                    if (Vector3.Distance(agent.pathEndPosition, Shrine.playerShrine.transform.position) > destroyShrineRange)// if we cant reach the shrine // revert to attacking the player
                    {
                        currentAgroTimer = agroTimer;
                        StartCoroutine(SeekPlayerBehaviour());
                        break;
                        //attack player
                    }
                    else if (agent.remainingDistance < destroyShrineRange)// if we are inrange of the shrine
                    {
                        StartCoroutine(MeleeAttackBehaviour());//melee attack
                        yield return new WaitForSeconds(meleeAttackDamageWait);
                        if (currentHealth > 0)// if were still alive
                        {
                            Shrine.playerShrine.InflictDamage(999999, 0);// deal alot of damage to the shrine
                        }
                    }
                }
                else// if there is no player shrine, always attack the player
                {
                    alwaysAgro = true;
                    StartCoroutine(SeekPlayerBehaviour());
                }

            }
            yield return null;
        }

    }

    IEnumerator SeekPlayerBehaviour()
    {
        float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);

        float nextMeleeAttackTime = 0;// the time stamps when the ai can do an attack again
        float nextRangedAttackTime = 0;
        while (true)
        {
            if (IsDead)
            {
                break;
            }
            currentAgroTimer -= Time.deltaTime;

            anim.SetFloat("velocity", agent.velocity.magnitude);
            distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);

            if (currentAgroTimer < 0 && alwaysAgro == false)// if we are no longer agro lets go back to attacking the player shrine
            {
                StartCoroutine(SeekPlayerShrineBehaviour());
                break;
            }

            if (Time.time > nextMeleeAttackTime && currentAttackBehaviour == null && distanceToPlayer < meleeAttackRange)// if we are able to perform a melee attack
            {
                currentAttackBehaviour = MeleeAttackBehaviour();//melee attack
                StartCoroutine(currentAttackBehaviour);
                nextMeleeAttackTime = Time.time + meleeAttackCoolDown + meleeAttackTotalTime;
            }
            else if (Time.time > nextRangedAttackTime && currentAttackBehaviour == null && distanceToPlayer < rangedAttackRange)// if were able to perform a ranged attack
            {
                currentAttackBehaviour = RangedAttackBehaviour();//ranged attack
                StartCoroutine(currentAttackBehaviour);

                nextRangedAttackTime = Time.time + rangedAttackCooldown + rangedAttackTotalTime;
            }
            else // if we cant attack, lets just walk to the player
            {
                agent.SetDestination(player.transform.position);
            }

            yield return null;
        }
    }

    IEnumerator MeleeAttackBehaviour()
    {
        //set animation
        anim.SetTrigger("Melee");
        IEntity entity = player as IEntity;
        yield return new WaitForSeconds(meleeAttackDamageWait);
        if (Vector3.Distance(player.transform.position, transform.position) < meleeAttackRange)
        {// if the player is still in range of the attack
            entity.InflictDamage(meleeDamage, 0);// deal damage\
            if (punchAudioSource && punchImpactSound.Count > 0) punchAudioSource.PlayOneShot(punchImpactSound[Random.Range(0, punchImpactSound.Count-1)]);
        }

        yield return new WaitForSeconds(meleeAttackTotalTime);// wait for the attack to finsih
        currentAttackBehaviour = null;
    }

    IEnumerator RangedAttackBehaviour()//
    {
        anim.SetTrigger("Cast");
        yield return new WaitForSeconds(rangedAttackWaitToSpawnProjectile);
        Projectile p = Instantiate(projectile, castPosition.position, castPosition.rotation) as Projectile;// create a projkectile
        p.target = player.gameObject;// set the target to the player
        p.transform.LookAt(p.target.transform);
        p.creator = gameObject;
        yield return new WaitForSeconds(rangedAttackTotalTime - rangedAttackWaitToSpawnProjectile);// waiot for the attack to finish
        currentAttackBehaviour = null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.1f);
        Gizmos.DrawSphere(transform.position, meleeAttackRange);
        Gizmos.color = new Color(0.5f, 0, 0, 0.1f);
        Gizmos.DrawSphere(transform.position, rangedAttackRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, destroyShrineRange);
    }


    public void Die()// if we die
    {
        if (agent.enabled)
        {
            currentHealth = 0;
            anim.SetBool("dead", true);
            agent.enabled = false;
            GetComponent<Collider>().enabled = false;
            OnDieEvent.Invoke(gameObject);

            if (onDieParticle) onDieParticle.Play();
            if (audioSource && onDieSound.Count > 0) audioSource.PlayOneShot(onDieSound[Random.Range(0, onDieSound.Count - 1)]);

            if (GetComponent<FootStepAudioController>())
                GetComponent<FootStepAudioController>().enabled = false;

            if (GetComponent<ScaleCurvesController>()) GetComponent<ScaleCurvesController>().enabled = true;
        }
    }

    public void InflictDamage(float damage, int damageType)// take damage
    {
        currentAgroTimer = agroTimer;
        bool wasEffective = false;
        if ((damageType == 0 && (int)elementType == 2) || damageType - 1 == (int)elementType)// damage type was effective
        {
            wasEffective = true;
            damage *= 3.0f;
        }

        currentHealth -= damage;

        if (OnTakeDamageStaticEvent != null)// call the static event (used for damage numbers)
            OnTakeDamageStaticEvent.Invoke((damageNumberPoint ? damageNumberPoint : transform), damage, (EElementType)damageType, wasEffective);

        if (currentHealth <= 0)
        {
            Die();
        }
        else
        {
            if (otherAudio && hitReactionAudio.Count > 0)
            {
                otherAudio.PlayOneShot(hitReactionAudio[Random.Range(0, hitReactionAudio.Count - 1)]);
            }
        }
    }

    public float GetHealthPercentage()
    {
        return Mathf.InverseLerp(0, maxHealth, currentHealth);
    }
    public EElementType GetElement()
    {
        return elementType;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SentryTower : MonoBehaviour
{
    public Projectile projectile;
    [Min(0.1f)]
    public float fireRate = 1;
    float nextFireTime = 0;
    public float range = 10;
    public Transform firePoint = null;
    static PlayerController player;
    public Transform playerTargetBone;
    public ParticleSystem spawnProjectileEffect = null;
    // Start is called before the first frame update
    void Awake ()
    {
        if (!player)
        {
            player = FindObjectOfType<PlayerController>();
        }
    }

    private void Start()
    {
        if (player)
        {
            playerTargetBone = player.anim.GetBoneTransform(HumanBodyBones.Chest);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (projectile && player)
        {
            RaycastHit hit;

            if(Vector3.Distance(player.transform.position, (firePoint?firePoint.transform.position:transform.position)) <= range && Time.time > nextFireTime && (!Physics.Linecast((firePoint ? firePoint.transform.position : transform.position), playerTargetBone.position, out hit) || hit.collider == player.collider))
            {
                nextFireTime = Time.time + fireRate;
                Projectile newProjectile = Instantiate(projectile, (firePoint ? firePoint.transform.position : transform.position), Quaternion.identity);
                newProjectile.target = playerTargetBone.gameObject;
                newProjectile.transform.LookAt(newProjectile.target.transform);
                newProjectile.creator = gameObject;
                if (spawnProjectileEffect) spawnProjectileEffect.Play();
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.1f);
        Gizmos.DrawSphere((firePoint ? firePoint.transform.position : transform.position), range);
    }


}

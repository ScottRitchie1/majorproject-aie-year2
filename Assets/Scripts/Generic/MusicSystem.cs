﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicSystem : MonoBehaviour
{

    public AudioClip _0deadShrine_NoEvent;
    public AudioClip _0deadShrine_Event;
    public AudioClip _1deadShrine_NoEvent;
    public AudioClip _1deadShrine_Event;
    public AudioClip _2deadShrine_NoEvent;
    public AudioClip _2deadShrine_Event;

    public AudioClip gameWin;
    public AudioClip gameLose;

    PlayerController player;
    AudioClip shouldBePlaying = null;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        int deadShrines = 0;
        if (Shrine.fireShrine.currentHealth <= 0)
        {
            deadShrines++;
        }

        if (Shrine.earthShrine.currentHealth <= 0)
        {
            deadShrines++;
        }

        if (Shrine.iceShrine.currentHealth <= 0)
        {
            deadShrines++;
        }

        if (player.currentHealth > 0)
        {
            if (deadShrines == 0)
            {
                if (WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Inactive)
                {
                    shouldBePlaying = _0deadShrine_NoEvent;
                }
                else
                {
                    shouldBePlaying = _0deadShrine_Event;

                }
            }
            else if (deadShrines == 1)
            {
                if (WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Inactive)
                {
                    shouldBePlaying = _1deadShrine_NoEvent;
                }
                else
                {
                    shouldBePlaying = _1deadShrine_Event;
                }
            }
            else if (deadShrines == 2)
            {
                if (WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Inactive)
                {
                    shouldBePlaying = _2deadShrine_NoEvent;
                }
                else
                {
                    shouldBePlaying = _2deadShrine_Event;
                }
            }
            else
            {
                shouldBePlaying = gameWin;
            }
        }
        else
        {
            shouldBePlaying = gameLose;
        }

        if (shouldBePlaying != null && audioSource.isPlaying == false) audioSource.PlayOneShot(shouldBePlaying);
    }
}

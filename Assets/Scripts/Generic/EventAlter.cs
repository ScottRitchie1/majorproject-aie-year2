﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;

public class EventAlter : MonoBehaviour
{
    public EElementType eventElement;
    public float timeBetweenActivations = 120;
    public static float lastActivatedTimeStamp = 0;
    public Image alterIcon;
    public MeshRenderer renderer;
    public Text interactText;
    public Color activeColour = Color.grey;
    public Color inactiveColour = Color.white;
    public float activateRange = 1;
    static WaveManager waveManager = null;
    static PlayerController player = null;
    // Start is called before the first frame update
    void Awake()
    {
        if (!waveManager)
            waveManager = FindObjectOfType<WaveManager>();

        if (!player)
            player = FindObjectOfType<PlayerController>();

        if(interactText && interactText.GetComponent<LookAtConstraint>())
        {
            ConstraintSource cs = new ConstraintSource();
            cs.sourceTransform = Camera.main.transform;
            cs.weight = 1;
            interactText.GetComponent<LookAtConstraint>().AddSource(cs);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Inactive && Vector3.Distance(player.transform.position, transform.position) < activateRange && Time.time > lastActivatedTimeStamp + timeBetweenActivations)
        {
            float pingPong = Mathf.PingPong(Time.time * 5, 1);
            renderer.material.SetColor("_EmissionColor", Color.Lerp(inactiveColour, activeColour * 2, pingPong));
            if(interactText && interactText.gameObject.activeInHierarchy == false)
            {
                interactText.gameObject.SetActive(true);
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                ActivateAlter();
            }

        }
        else
        {
            if (interactText && interactText.gameObject.activeInHierarchy == true)
            {
                interactText.gameObject.SetActive(false);
            }
            Color currentColour = renderer.material.GetColor("_EmissionColor");
            bool thisAlterShrineEventActive = (int)eventElement + 1 == (int)WaveManager.CurrentShrineEvent;
            if (WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Inactive || thisAlterShrineEventActive)
            {
                renderer.material.SetColor("_EmissionColor", Color.Lerp(currentColour, activeColour * (thisAlterShrineEventActive? 2 : 1), Time.deltaTime));
            }
            else
            {
                renderer.material.SetColor("_EmissionColor", Color.Lerp(currentColour, inactiveColour, Time.deltaTime));
            }
        }
    }

    [ContextMenu("ActivateAlter")]
    public void ActivateAlter()
    {
        if (WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Inactive)
        {
            waveManager.StartShrineEvent(eventElement);
            lastActivatedTimeStamp = Time.time + waveManager.shrineEventActiveTime;
            //dim other alters
        }
    }
}

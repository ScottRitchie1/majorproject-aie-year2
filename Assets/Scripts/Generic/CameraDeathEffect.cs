﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
public class CameraDeathEffect : MonoBehaviour
{
    public PlayerController player;
    public PostProcessVolume volume;
    // Start is called before the first frame update
    void Awake()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (volume && player) volume.weight = Mathf.Lerp(volume.weight, (player.currentHealth > 0 ? 0 : 1), Time.deltaTime);
    }
}

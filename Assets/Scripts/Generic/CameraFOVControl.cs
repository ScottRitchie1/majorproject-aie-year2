﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CamController))]
public class CameraFOVControl : MonoBehaviour
{
    Camera cam;

    [Range(1, 179)]
    public float stationaryFOV = 70;
    [Range(1, 179)]
    public float movementFOV = 80;

    [Min(0)]
    public float moveSpeedThreshhold = 10;

    Vector3 prevPos;

    public Rigidbody target;
    float currentWeight;
    public float smoothing;
    // Start is called before the first frame update
    void Awake()
    {
        cam = GetComponentInChildren<Camera>();
        target = FindObjectOfType<PlayerController>().GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        currentWeight = Mathf.Lerp(currentWeight, Mathf.InverseLerp(0, moveSpeedThreshhold, target.velocity.magnitude), Time.deltaTime * smoothing);
        cam.fieldOfView = Mathf.Lerp(stationaryFOV, movementFOV, currentWeight);
        prevPos = transform.position;
    }
}

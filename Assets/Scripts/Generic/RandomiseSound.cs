﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomiseSound : MonoBehaviour
{
    public List<AudioClip> sounds = new List<AudioClip>();
    AudioSource audioSource;

    public bool randomPitch = false;
    public Vector2 pitchRange = new Vector2(0.9f, 1.1f);
    // Start is called before the first frame update
    void OnEnable()
    {

        PlaySound();
        
    }

    public void PlaySound()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource)
        {
            if (randomPitch)
            {
                audioSource.pitch = Random.Range(pitchRange.x, pitchRange.y);
            }
            if (sounds.Count > 0)
                audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Count - 1)]);
        }
    }
}

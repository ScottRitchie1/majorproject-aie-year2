﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ForceController : MonoBehaviour
{
    public Vector3 direction = Vector3.one;
    public Vector3 rotation = Vector3.one;
    public float force = 10;
    void Awake()
    {
        GetComponent<Rigidbody>().AddForce(direction * force, ForceMode.VelocityChange);
        GetComponent<Rigidbody>().AddTorque(rotation);
    }

    private void OnDrawGizmosSelected()
    {
        direction = direction.normalized;
        Gizmos.DrawRay(transform.position, direction * force);
    }
}

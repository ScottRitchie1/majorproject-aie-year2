﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraDamageEffect : MonoBehaviour
{
    PlayerController player;

    public Vector2 vignetteRange;
    public Gradient vignetteColour;

    public int vignetteIncreaseAmount = 1;
    public float vignetteRecoveryTime = 10;
    public float vignetteOnDamageWait = 1;
    float vignetteWaitTimeStamp;
    float vignetteTimeTillRecovery;


    float vignetteAmount;

    public PostProcessVolume postProcessing;
    Vignette vignette;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        if (player)
            player.OnDamagedEvent.AddListener(IncreaseVignette);

        if (postProcessing)
        {
            postProcessing.profile.TryGetSettings(out vignette);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //player.damage
        if(vignetteWaitTimeStamp + vignetteOnDamageWait < Time.time)
            vignetteTimeTillRecovery -= Time.deltaTime;

        if (vignette)
        {
            float weight = Mathf.InverseLerp(0, vignetteRecoveryTime, vignetteTimeTillRecovery);
            vignette.intensity.value = Mathf.Lerp(vignetteRange.x, vignetteRange.y, weight);
            vignette.color.value = vignetteColour.Evaluate(weight);
        }
    }

    public void IncreaseVignette (float damage)
    {
        vignetteWaitTimeStamp = Time.time;
        if (vignetteTimeTillRecovery < 0)
            vignetteTimeTillRecovery = vignetteIncreaseAmount;
        else if(vignetteTimeTillRecovery + vignetteIncreaseAmount > vignetteRecoveryTime)
            vignetteTimeTillRecovery = vignetteRecoveryTime;
        else
            vignetteTimeTillRecovery += vignetteIncreaseAmount;
    }
}

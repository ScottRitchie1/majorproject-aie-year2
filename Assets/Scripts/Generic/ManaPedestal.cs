﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPedestal : MonoBehaviour
{
    public float manaRecieved = 10;
    public float consumeRange = 2;
    public float infiniteManaTime = 30;

    bool consumed = false;
    static PlayerSpellCaster player;

    [Header("Effects")]
    public ParticleSystem absorbParticle = null;
    public ParticleSystem circleParticle = null;
    public Light particleLight = null;

    private void Start()
    {
        if (!player)
            player = FindObjectOfType<PlayerSpellCaster>();
    }

    // Update is called once per frame
    void Update()
    {
        if(consumed == false)
        {
            if(Vector3.Distance(player.transform.position, transform.position) < consumeRange)
            {
                
                StartCoroutine(OnConsumed());
                //trigger effect
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, consumeRange);
    }
    IEnumerator OnConsumed()
    {
        consumed = true;
        player.maxMana += manaRecieved;
        player.infiniteManaTimeRemaining = infiniteManaTime;




        if (circleParticle)
        {
            ParticleSystem.EmissionModule em = circleParticle.emission;
            em.enabled = false;
            //ParticleSystem.SizeOverLifetimeModule solm = circleParticle.sizeOverLifetime;
            //em.enabled = false;
        }

        if (absorbParticle)
        {
            ParticleSystem.Particle[] particles;

            ParticleSystem.VelocityOverLifetimeModule VOLM = absorbParticle.velocityOverLifetime;
            ParticleSystem.EmissionModule EM = absorbParticle.emission;
            VOLM.enabled = false;
            EM.enabled = false;

            particles = new ParticleSystem.Particle[absorbParticle.particleCount];
            absorbParticle.GetParticles(particles);
            for (int i = 0; i < particles.GetUpperBound(0); i++)
            {
                particles[i].remainingLifetime = particles[i].startLifetime;
                particles[i].velocity = Random.insideUnitSphere.normalized * 2;
            }
            absorbParticle.SetParticles(particles, particles.Length);

            while (absorbParticle.particleCount > 0)
            {
                particles = new ParticleSystem.Particle[absorbParticle.particleCount];
                absorbParticle.GetParticles(particles);
                for (int i = 0; i < particles.GetUpperBound(0); i++)
                {
                    Vector3 particlePoint = (absorbParticle.main.simulationSpace == ParticleSystemSimulationSpace.Local ? absorbParticle.transform.TransformPoint(particles[i].position) : particles[i].position);
                    float ForceToAdd = (particles[i].startLifetime - particles[i].remainingLifetime) * (Vector3.Distance(player.transform.position + Vector3.up, particlePoint));
                    particles[i].velocity = Vector3.Lerp(particles[i].velocity, (player.transform.position + Vector3.up - particlePoint).normalized * ForceToAdd, Time.deltaTime * 10/* * trackingWeight.Evaluate(1 - Mathf.InverseLerp(0, particles[i].startLifetime, particles[i].remainingLifetime))*/);
                }
                absorbParticle.SetParticles(particles, particles.Length);


                yield return null;
            }
            Destroy(absorbParticle.gameObject);

        }

        if (particleLight)
        {
            float initialLightIntensity = particleLight.intensity;
            float reduceLightIntensityTime = 2;
            float startTime = Time.time;
            while (startTime + reduceLightIntensityTime > Time.time)
            {
                particleLight.intensity = Mathf.InverseLerp(startTime + reduceLightIntensityTime, startTime, Time.time) * initialLightIntensity;
                yield return null;
            }
            Destroy(particleLight);
        }

        if (circleParticle)
        {
            while (circleParticle.particleCount > 0)
            {
                yield return null;

            }
            Destroy(circleParticle.gameObject);
        }
        Destroy(this);

    }
}

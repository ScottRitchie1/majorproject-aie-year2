﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerIkController : MonoBehaviour {
    PlayerEngine playerEngine;//reference player engine
    Animator anim;//reference aniamtor
    Transform m_rightFootBone;// foot bone
    Transform m_leftFootBone;//foot bone


    [Header("Look IK")]
    public bool enableLookIK = true;// enable look ik
    public float lookAtSmoothing = 1;// rate our looking is smothed
    Vector3 lastLookIkPosition = Vector3.zero;// this is used for smothing

    [HideInInspector]

    //position the character will look at
    public Vector3 lookatIkPosition = Vector3.zero;// the position we should be looking at
    [Range(0, 1)]
    //the amount the head will be ffected by the lookat
    public float lookatHeadRatio = 0.5f;
    [Range(0, 1)]
    //thea mount the body will be ffected by the lookat
    public float lookatBodyRatio = 0.5f;

    [Header("Foot Ik")]
    public bool enableFootIK = true;

    //the radius of the sphere cast
    public float footWidth = 0.05f;
    //the vertical offset for the foot positioning
    public float footPadding = 0.1f;
    

    [Range(0.0f, 10.0f)]
    public float rootHeightAdjustSpeed = 1;// the rate the characters root move to the correct height at
    float rightFootHityY = float.NaN;//hit heights for the feet , calculated via raycasts
    float leftFootHitY = float.NaN;
    public LayerMask footIkIgnoreLayers;// what the feet should hit
    // Use this for initialization
    private void Awake()
    {
        playerEngine = GetComponentInParent<PlayerEngine>();
    }

    void Start () {
        anim = GetComponent<Animator>();// get bones etc
        m_rightFootBone = anim.GetBoneTransform(HumanBodyBones.RightFoot);
        m_leftFootBone = anim.GetBoneTransform(HumanBodyBones.LeftFoot);
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (enableLookIK)
        {
            //calculate our look position
            Vector3 tempLookPos = (lookatIkPosition != Vector3.zero ? lookatIkPosition : transform.position + Camera.main.transform.forward * 100);
            anim.SetLookAtWeight(1, lookatBodyRatio, lookatHeadRatio, 0);
            lastLookIkPosition = Vector3.Slerp(lastLookIkPosition, tempLookPos - transform.position, lookAtSmoothing * Time.deltaTime);
            anim.SetLookAtPosition(transform.position + lastLookIkPosition);
        }
        else
        {
            anim.SetLookAtWeight(0);
        }

        if (enableFootIK)
        {
            //calculate positions and lenghts for foot raycasts
            float footCheckRayStartY = playerEngine.transform.position.y + footWidth + playerEngine.stepHeight;
            Vector3 rightFootRayStart = new Vector3(m_rightFootBone.position.x, footCheckRayStartY, m_rightFootBone.position.z);
            Vector3 leftFootRayStart = new Vector3(m_leftFootBone.position.x, footCheckRayStartY, m_leftFootBone.position.z);
            float footRayLenght = playerEngine.stepHeight * 2;

            RaycastHit leftFootRayHit;
            RaycastHit rightFootRayHit;
            rightFootHityY = float.NaN;
            leftFootHitY = float.NaN;

            //raycast our right foot
            if (Physics.SphereCast(rightFootRayStart, footWidth, Vector3.down, out rightFootRayHit, footRayLenght, footIkIgnoreLayers, QueryTriggerInteraction.Ignore))
                rightFootHityY = rightFootRayHit.point.y;

            //raycastour left foot
            if (Physics.SphereCast(leftFootRayStart, footWidth, Vector3.down, out leftFootRayHit, footRayLenght, footIkIgnoreLayers, QueryTriggerInteraction.Ignore))
                leftFootHitY = leftFootRayHit.point.y;


            
            

            // if we are grounded and one of our feet hit something
            if (playerEngine.isGrounded && (!float.IsNaN(leftFootHitY) || !float.IsNaN(rightFootHityY)))
            {
                //find the lowest foot y position
                float lowestFootHeight = Mathf.Min((float.IsNaN(rightFootHityY) ? float.MaxValue : rightFootHityY), (float.IsNaN(leftFootHitY) ? float.MaxValue : leftFootHitY));
                //calculate the lerped local y position of the root character
                float rootHeight = Mathf.MoveTowards(transform.localPosition.y, Mathf.Clamp(lowestFootHeight - playerEngine.transform.position.y, -playerEngine.stepHeight, 0), rootHeightAdjustSpeed * Time.deltaTime);
                transform.localPosition = Vector3.up * rootHeight;

                // if right foot ik
                if (!float.IsNaN(rightFootHityY) && rightFootHityY + (footPadding * 1.1) >= m_rightFootBone.position.y)
                {
                    anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
                    anim.SetIKPosition(AvatarIKGoal.RightFoot, rightFootRayHit.point + Vector3.up * footPadding);
                    anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);
                    anim.SetIKRotation(AvatarIKGoal.RightFoot, Quaternion.LookRotation(Vector3.Cross(m_rightFootBone.right, rightFootRayHit.normal), rightFootRayHit.normal));//rightFootRayHit.normal
                }
                else
                {
                    anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0);
                    anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0);
                }

                //if left foot ik
                if (!float.IsNaN(leftFootHitY) && leftFootHitY + (footPadding * 1.1) >= m_leftFootBone.position.y)
                {
                    anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
                    anim.SetIKPosition(AvatarIKGoal.LeftFoot, leftFootRayHit.point + Vector3.up * footPadding);
                    anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
                    anim.SetIKRotation(AvatarIKGoal.LeftFoot, Quaternion.LookRotation(Vector3.Cross(m_leftFootBone.right, leftFootRayHit.normal), leftFootRayHit.normal));
                }
                else
                {
                    anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 0);
                    anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 0);
                }
            }
            else
            {
                transform.localPosition = Vector3.zero;
                anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0);
                anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 0);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 0);
                anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0);
            }
        }
        else
        {
            transform.localPosition = Vector3.zero;
            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 0);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 0);
            anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0);
        }
    }

    private void OnDrawGizmosSelected()
    {
        //draw gizmo things
        if (playerEngine)
        {
            Gizmos.color = Color.white;

            float footCheckRayStartY = playerEngine.transform.position.y + footWidth + playerEngine.stepHeight;
            if (m_rightFootBone) {
                Vector3 rightFootRayStart = new Vector3(m_rightFootBone.position.x, footCheckRayStartY, m_rightFootBone.position.z);
                Gizmos.DrawRay(rightFootRayStart, Vector3.down * playerEngine.stepHeight * 2);
            }

            if (m_leftFootBone)
            {
                Vector3 leftFootRayStart = new Vector3(m_leftFootBone.position.x, footCheckRayStartY, m_leftFootBone.position.z);
                Gizmos.DrawRay(leftFootRayStart, Vector3.down * playerEngine.stepHeight * 2);
            }
        }

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere((transform.position - lookatIkPosition).normalized * 10, 1);
        if (CamController.instance)
        Gizmos.DrawLine(CamController.instance.transform.position, lookatIkPosition);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamagedEvent : UnityEvent<float> { }

public class PlayerController : PlayerEngine, IEntity
{
    PlayerIkController ikController;// referce to ik controller

    public bool isControllable = true;

    bool m_isCrouched = false;
    public bool isCrouched{get { return m_isCrouched; }}
    public Transform centerOfMassTransform { get { return (anim ? anim.GetBoneTransform(HumanBodyBones.Chest) : transform); } }
    public Vector2 OVERRIDEINPUTTEST;
    public bool FORCESPRNTTEST = false;

    [HideInInspector]
    public Vector3 characterForwardFacingOverride = Vector3.zero;//set the rotation of the character independant of the look and facing

    [Header("Health")]
    public float maxHealth = 100;
    public float currentHealth = 0;

    [Header("Grounded")]
    //speeds
    public float runSpeed = 7;
    public float walkSpeed = 2;
    public float crouchedSpeed = 1.5f;
    [Space]
    //accelerations
    public float accelerationRunning = 6;
    public float accelerationWalking = 3;
    public float accelerationCrouched = 3;
    public float deaccelerationGrounded = 5;
    [Space]
    //turn speed
    public float turnSpeedGrounded = 5;
    [Space]
    //smoothing ie how quickly the player changes course
    public float groundedMoveSmoothingRunning = 0.5f;
    public float groundedMoveSmoothingWalking = 0.2f;
    public float groundedMoveSmoothingCrouched = 0.3f;


    [Header("Slipping")]
    // if the player is standing on a slop that it cant, they will move down the slop
    public float slipSpeed = 8;
    public float slipAcceleration = 2;
    [Header("AirTime"), Space]
    //speed in the air
    public float airTimeSpeed = 10;
    [Space]
    //turn speed in the air
    public float turnSpeedAirTime = 10;
    [Space]
    public float accelerationAirTime = 1;
    public float deaccelerationAirTime = 0;
    public float groundedMoveSmoothingAirTime = 1;

    [Header("Jumping"), Space]
    public float jumpForce = 7;
    public float jumpWaitTime = 0.2f;// the time hte player has to wait before then can jump again after landing
    float curJumpWaitTime = 0;
    public float hardLandingVelocity = -5;// the negitive y velocity required for the cahracter the play the landing aniamtion after then land
    [Space]
    public AudioSource jumpAudioSource;
    public List<AudioClip> jumpSounds = new List<AudioClip>();
    public List<AudioClip> jumpLandSounds = new List<AudioClip>();
    public Vector2 jumpSoundsPitchRange = new Vector2(0.9f, 1.1f);

    [Header("Double Jump")]
    public float doubleJumpVelocityThreshhold = -0.1f;// the y velocity the player must be < to allow for them to jump
    public float doubleJumpForceUp = 7;// upwards force
    public float doubleJumpForceForward = 4;// forwards force
    bool hasDoubleJumped = false;// have double jumped this frame
    public ParticleSystem doubleJumpEffect;// particle to play
    public List<AudioClip> doubleJumpSounds = new List<AudioClip>();



    [Header("Crouched")]
    public float uncrouchCheckDistance = 1.7f;// the distance we check above the players head to see if we can uncrouch ie is there something above the player???

    [HideInInspector, System.NonSerialized]
    public bool alignToMoveDirection = true;// should our bodys orientation align to the direction were moving or a custom direction
    [HideInInspector, System.NonSerialized]
    public bool forceWalk = false;


    [Header("Other Sounds")]//sounds
    public AudioSource hitSoundAudioSource;
    public List<AudioClip> healthyHitSounds = new List<AudioClip>();
    public List<AudioClip> unhealthyHitSounds = new List<AudioClip>();
    [Range(0, 100)]
    public float healthyHitSoundsPercentageThreshhold = 20;
    public Vector2 hitSoundsPitchRange = new Vector2(0.9f, 1.1f);

    [Space]
    public List<AudioClip> deathSounds = new List<AudioClip>();
    public Vector2 deathSoundsPitchRange = new Vector2(0.9f, 1.1f);
    [Space]
    public List<AudioClip> reviveSounds = new List<AudioClip>();
    public Vector2 reviveSoundsPitchRange = new Vector2(0.9f, 1.1f);
    public ParticleSystem reviveParticle;

    public DamagedEvent OnDamagedEvent = new DamagedEvent();

    protected override void Awake()
    {
        base.Awake();
        //Stealer.OnDieEvent.AddListener(HealWhenEnemyDies);
        //Attacker.OnDieEvent.AddListener(HealWhenEnemyDies);
        ikController = GetComponentInChildren<PlayerIkController>();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        OnGroundEvent += OnGroundedStatusChanged;
        currentHealth = maxHealth;
    }

    protected override void Update()
    {
        //update our player engine
        base.Update();

        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            InflictDamage(999999, 0);
        }

        if (anim)
        {
            Vector3 localVel = transform.InverseTransformDirection(rb.velocity);
            //update our animation controller
            if (isSlipping)
            {
                anim.SetFloat("forwardVel", Mathf.Lerp(anim.GetFloat("forwardVel"), 0, Time.deltaTime * 10));
                anim.SetFloat("sideVel", Mathf.Lerp(anim.GetFloat("sideVel"), 0, Time.deltaTime * 10));
            }
            else{
                anim.SetFloat("forwardVel", Mathf.Lerp(anim.GetFloat("forwardVel"), localVel.z, Time.deltaTime * 10));
                anim.SetFloat("sideVel", Mathf.Lerp(anim.GetFloat("sideVel"), localVel.x, Time.deltaTime * 10));
            }
            if(!isGrounded)
                anim.SetFloat("verticelVel", localVel.y);
            anim.SetFloat("distToGround", distanceToGround);
            anim.SetBool("isGrounded", isGrounded);
            anim.SetBool("isCrouched", isCrouched);
        }

        
    }

    // Update is called once per frame
    public override void UpdateControllers()
    {
        // if we are in an override state ie cant controller the player
        if (!anim.GetCurrentAnimatorStateInfo(2).IsName("None") || isControllable == false)
        {
            controlVariables.maxSpeed = 0;
            controlVariables.deacceleration = 10;
            controlVariables.turnRate = 0;
            if (ikController)
                ikController.enableLookIK = false;
            return;
        }
        else
        {
            if (ikController)
                ikController.enableLookIK = true;
        }

        //camera and input direction
        Vector3 cameraDirection = Vector3.forward;
        if (CamController.instance)
            cameraDirection = CamController.instance.GetCameraDirectionAligned();

        Vector3 inputDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized * Mathf.Max(Mathf.Abs(Input.GetAxis("Horizontal")), Mathf.Abs(Input.GetAxis("Vertical")));// gets the move input

        if(OVERRIDEINPUTTEST.magnitude > 0)
        {
            inputDirection = new Vector3(OVERRIDEINPUTTEST.x, 0, OVERRIDEINPUTTEST.y).normalized * Mathf.Max(Mathf.Abs(OVERRIDEINPUTTEST.x), Mathf.Abs(OVERRIDEINPUTTEST.y));
        }

        RaycastHit crouchedHit;

        //crouched state
        if (Input.GetKey(KeyCode.C))
        {
            m_isCrouched = true;
        }
        else if(!Physics.SphereCast(transform.position + Vector3.up * collider.radius, collider.radius, Vector3.up, out crouchedHit, uncrouchCheckDistance - collider.radius * 2))
        {
            m_isCrouched = false;
        }

        // move direction
        Vector3 m_targetMoveDirection = Quaternion.LookRotation(cameraDirection) * inputDirection;
        SetMoveDirection(m_targetMoveDirection);

        
        //GROUNDED
        if (isGrounded)
        {
            anim.SetBool("Jumping", false);
            curJumpWaitTime -= Time.deltaTime;
            //moveSpeeds & move smoothing
            if (inputDirection.magnitude > 0.2f)// if we are trying to move
            {
                //set our accelerations
                if (isCrouched)
                {
                    controlVariables.maxSpeed = crouchedSpeed;
                    controlVariables.moveDirectionSmoothing = groundedMoveSmoothingCrouched;
                    controlVariables.acceleration = accelerationCrouched;
                }
                else
                {
                    if ((Input.GetKey(KeyCode.LeftShift) || FORCESPRNTTEST) && !forceWalk)
                    {
                        controlVariables.maxSpeed = runSpeed;
                        controlVariables.moveDirectionSmoothing = groundedMoveSmoothingRunning;
                        controlVariables.acceleration = accelerationRunning;
                    }
                    else
                    {
                        controlVariables.acceleration = accelerationWalking;
                        controlVariables.maxSpeed = walkSpeed;
                        controlVariables.moveDirectionSmoothing = groundedMoveSmoothingWalking;
                    }
                }
            }
            else
            {
                // set our speed to 0 if we arnt moving
                controlVariables.maxSpeed = 0;
                controlVariables.moveDirectionSmoothing = groundedMoveSmoothingWalking;
            }

            //deacceleration
            controlVariables.deacceleration = deaccelerationGrounded;
            //turn speed
            controlVariables.turnRate = turnSpeedGrounded;

            if (isSlipping)// if were slipping set our direction and move values
            {
                controlVariables.acceleration = slipAcceleration;
                controlVariables.maxSpeed = slipSpeed;
                SetTargetForwards(rb.velocity.normalized);
            }
            else
            {
                if (!alignToMoveDirection)// set target forwards to either movedirection or facing direction (controlled via spell manager for example)
                {
                    SetTargetForwards((characterForwardFacingOverride == Vector3.zero ? cameraDirection : characterForwardFacingOverride));
                }
                else
                {
                    SetTargetForwards(m_targetMoveDirection);
                }
            }

            if (Input.GetKeyDown(KeyCode.Space) && isCrouched == false && curJumpWaitTime <= 0)
            {
                //Single Jump
                if (anim) 
                    anim.SetBool("Jumping", true);
                AddForce(Vector3.up * jumpForce);
                curJumpWaitTime = jumpWaitTime;
                if (jumpAudioSource && jumpSounds.Count > 0)
                {
                    jumpAudioSource.PlayOneShot(jumpSounds[Random.Range(0, jumpSounds.Count - 1)]);
                    jumpAudioSource.pitch = Random.Range(jumpSoundsPitchRange.x, jumpSoundsPitchRange.y);
                }
            }
        }
        else// NOT GROUNDED
        {
            //speed
            controlVariables.maxSpeed = airTimeSpeed;
            //move smoothing
            controlVariables.moveDirectionSmoothing = groundedMoveSmoothingAirTime;
            //acceleration / deacceleration
            controlVariables.acceleration = accelerationAirTime;
            controlVariables.deacceleration = deaccelerationAirTime;
            //turn speed
            controlVariables.turnRate = turnSpeedAirTime;

            SetTargetForwards(new Vector3(rb.velocity.x, 0, rb.velocity.z).normalized);

            if(hasDoubleJumped == false && Input.GetKeyDown(KeyCode.Space) && rb.velocity.y < doubleJumpVelocityThreshhold)
            {
                //Double Jump
                anim.SetBool("Jumping", true);
                hasDoubleJumped = true;
                AddForce((Vector3.down * rb.velocity.y) + (Vector3.up * doubleJumpForceUp) + (cameraDirection * doubleJumpForceForward));
                if (doubleJumpEffect)
                {
                    doubleJumpEffect.Play();
                }
                if (jumpAudioSource && doubleJumpSounds.Count > 0)
                {
                    jumpAudioSource.PlayOneShot(doubleJumpSounds[Random.Range(0, doubleJumpSounds.Count - 1)]);
                    jumpAudioSource.pitch = Random.Range(jumpSoundsPitchRange.x, jumpSoundsPitchRange.y);
                }
            }
        }
    }

    // when our grounded state changed
    void OnGroundedStatusChanged(bool wasGroundedFromLanding)
    {
        if (wasGroundedFromLanding)
        {
            hasDoubleJumped = false;
            if (rb.velocity.y <= -Mathf.Abs(hardLandingVelocity))// trigger animation / no control state if we hit the ground to hard
            {
                anim.SetTrigger("HardLanding");
            }

            if (jumpAudioSource && jumpLandSounds.Count > 0)
            {
                jumpAudioSource.PlayOneShot(jumpLandSounds[Random.Range(0, jumpLandSounds.Count - 1)]);
                jumpAudioSource.pitch = Random.Range(jumpSoundsPitchRange.x, jumpSoundsPitchRange.y);
            }
        }
    }

    public void Die()// if we die
    {
        if (anim.GetBool("Dead") == false)// check if we are in the dead state already
        {
            anim.SetBool("Dead", true);
            currentHealth = 0;
            Debug.Log("Player Died");
            if (hitSoundAudioSource && deathSounds.Count > 0)
            {
                jumpAudioSource.pitch = Random.Range(deathSoundsPitchRange.x, deathSoundsPitchRange.y);
                hitSoundAudioSource.PlayOneShot(deathSounds[Random.Range(0, deathSounds.Count - 1)]);
            }
            CrystalChunk.RevivePlayer(this);
        }
    }
    public void Revive()// revive (via crystal)
    {
        Debug.Log("REVIVED");

        currentHealth = maxHealth;
        anim.SetBool("Dead", false);
        if (hitSoundAudioSource && reviveSounds.Count > 0)
        {
            jumpAudioSource.pitch = Random.Range(reviveSoundsPitchRange.x, reviveSoundsPitchRange.y);
            hitSoundAudioSource.PlayOneShot(reviveSounds[Random.Range(0, reviveSounds.Count - 1)]);
        }

        if (reviveParticle) reviveParticle.Play();
    }

    public void Heal(float amount)// when we heal
    {
        if (currentHealth > 0)
        {
            currentHealth += amount;
            if (currentHealth > maxHealth)
                currentHealth = maxHealth;
        }
    }
    public void InflictDamage(float damage, int damageType)// inflict damage
    {
        currentHealth -= damage;

        if (OnDamagedEvent != null)
            OnDamagedEvent.Invoke(damage);
        

        if (currentHealth <= 0)// if we die
        {
            Die();
        }
        else
        {
            if (hitSoundAudioSource && !hitSoundAudioSource.isPlaying)// if we dont die play a hit sounds
            {
                hitSoundAudioSource.pitch = Random.Range(hitSoundsPitchRange.x, hitSoundsPitchRange.y);

                if (GetHealthPercentage() > healthyHitSoundsPercentageThreshhold && healthyHitSounds.Count > 0)
                    hitSoundAudioSource.PlayOneShot(healthyHitSounds[Random.Range(0, healthyHitSounds.Count - 1)]);
                else if (unhealthyHitSounds.Count > 0)
                    hitSoundAudioSource.PlayOneShot(unhealthyHitSounds[Random.Range(0, unhealthyHitSounds.Count - 1)]);

            }
        }
    }

    void HealWhenEnemyDies(GameObject enemy)
    {
        Heal(1);
    }

    public float GetHealthPercentage()
    {
        return Mathf.InverseLerp(0, maxHealth, currentHealth);
    }
}

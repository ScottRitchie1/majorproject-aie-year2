﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveHeal : MonoBehaviour
{
    PlayerController player;
    public float healAmount = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        player.Heal(healAmount * Time.deltaTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FootStepAudioController : MonoBehaviour
{
    [System.Serializable]
    public class footStepDefinition
    {
        public List<AudioClip> audioClipsClose = new List<AudioClip>();
        public bool useFarClips = false;
        public List<AudioClip> audioClipsFar = new List<AudioClip>();
        public Vector2 pitchRange = new Vector2(0.9f, 1.1f);
        public float maxVolumeThreshholdBasedOnVelocity = 2;

        public List<string> tagMappings = new List<string>();
        public List<int> terrainMappings = new List<int>();
    }

    Animator anim;

    public List<footStepDefinition> footStepDefinitions = new List<footStepDefinition>();

    public AudioSource audioSourceClose;
    public AudioSource audioSourceFar;
    public float groundCheckDistance;
    Transform rightFoot;
    Transform leftFoot;

    public string footStepCurveParameter = "footstep";
    // Start is called before the first frame update
    float prevFootStep;
    Vector2 prevPosition;
    private void Awake()
    {
        if (!audioSourceClose)
        {
            audioSourceClose = gameObject.AddComponent<AudioSource>();
        }

        if (!audioSourceFar)
        {
            audioSourceFar = gameObject.AddComponent<AudioSource>();
        }

        anim = GetComponent<Animator>();
        rightFoot = anim.GetBoneTransform(HumanBodyBones.RightFoot);
        leftFoot = anim.GetBoneTransform(HumanBodyBones.LeftFoot);
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //if(footStepDefinitions.Count > 0) {
        //Vector3 pos = new Vector3((rightFoot.position.x + leftFoot.position.x) * 0.5f, Mathf.Max(rightFoot.position.y, leftFoot.position.y), (rightFoot.position.z + leftFoot.position.z) * 0.5f);
        //float radius = Vector2.Distance(new Vector2(rightFoot.position.x, rightFoot.position.z), new Vector2(leftFoot.position.x, leftFoot.position.z));
        //int surfaceIndex = 0;
        //RaycastHit hit;
        //    if (Physics.Raycast(pos + Vector3.up * radius, Vector3.down, out hit, radius + groundCheckDistance))
        //    {
        //        Terrain terrain = hit.collider.GetComponent<Terrain>();
        //        if (terrain)
        //        {
        //
        //        }
        //        else
        //        {
        //            for (int i = 0; i < footStepDefinitions.Count; i++)
        //            {
        //                for (int j = 0; j < footStepDefinitions[i].tagMappings.Count; j++)
        //                {
        //                    if (hit.collider.gameObject.tag == footStepDefinitions[i].tagMappings[j])
        //                    {
        //                        surfaceIndex = i;
        //                        i = footStepDefinitions.Count + 1;
        //                        break;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        if (anim)
        {
            float curFootStep = Mathf.Round(anim.GetFloat("footStep") * 100) / 100;
            if (prevFootStep < 0)
            {
                if (curFootStep >= 0)
                {
                    PlayFootStep(0);
                }
            }
            else if (prevFootStep >= 0)
            {
                if (curFootStep < 0)
                {
                    PlayFootStep(0);
                }
            }
            prevFootStep = curFootStep;
        }

        prevPosition = new Vector2(transform.position.x, transform.position.z);
    }

    void PlayFootStep(int index)
    {
        if (audioSourceClose)
        {
            if (index < 0 || index > footStepDefinitions.Count)
                return;

            if (footStepDefinitions[index].audioClipsClose.Count == 0)
                return;


            int clipIndex = Random.Range(0, footStepDefinitions[index].audioClipsClose.Count);
            float volume = Mathf.InverseLerp(0, footStepDefinitions[index].maxVolumeThreshholdBasedOnVelocity, Vector2.Distance(prevPosition, new Vector3(transform.position.x, transform.position.z)));
            float pitch = Random.Range(footStepDefinitions[index].pitchRange.x, footStepDefinitions[index].pitchRange.y);
            audioSourceClose.pitch = pitch;
            audioSourceClose.PlayOneShot(footStepDefinitions[index].audioClipsClose[clipIndex]);
            //audioSourceClose.volume = volume;

            if (footStepDefinitions[index].useFarClips && audioSourceFar)
            {
                audioSourceFar.pitch = pitch;
                audioSourceFar.PlayOneShot(footStepDefinitions[index].audioClipsFar[clipIndex]);
                audioSourceFar.volume = volume;

            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        
    }
}

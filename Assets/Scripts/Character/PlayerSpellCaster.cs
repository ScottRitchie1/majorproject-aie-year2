﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpellCaster : MonoBehaviour
{
    bool targetLock = false;
    public bool IsTargetLocking
    {
        get { return targetLock; }
    }
    public bool canCastSpells = true;
    PlayerController playerController;
    PlayerIkController playerIkController;

    public List<SpellDefinition> spellList = new List<SpellDefinition>();
    [SerializeField]
    public List<List<PlayerEffectManager>> spellObjects = new List<List<PlayerEffectManager>>();

    bool stopCastingSpell = false;

    public bool isCasting
    {
        get
        {
            return (currentSpell != null);
        }
    }
    IEnumerator currentSpell = null;
    public EElementType selectedElementType;

    public SpellDefinition fireSpell;
    public SpellDefinition iceSpell;
    public SpellDefinition earthSpell;

    public PlayerEffectManager pfm;

    public Targetable currentTarget = null;

    public float targetingRange = 20;
    public float targetingAngle = 45;


    [Header("Mana")]
    [Min(1)]
    public float maxMana = 100;
    [HideInInspector]
    public float currentMana = 0;
    public float manaRegen = 5;
    public float manaCooldown = 1;
    [HideInInspector]
    public float currentManaCooldown;

    public float infiniteManaTimeRemaining = 0;
    public bool HasInfiniteMana
    {
        get { return infiniteManaTimeRemaining > 0; }
    }

    [Header("Material Changes")]
    public List<Renderer> renderers = new List<Renderer>();
    public Material fireMaterial;
    public Material waterMaterial;
    public Material earthMaterial;

    private void Awake()
    {
        spellObjects = new List<List<PlayerEffectManager>>(new List<PlayerEffectManager>[9]);
        //spellObjects = new List<List<Object>>();
        spellObjects[0] = new List<PlayerEffectManager>();
        spellList = new List<SpellDefinition>(new SpellDefinition[9]);
        playerController = GetComponent<PlayerController>();
        playerIkController = GetComponentInChildren<PlayerIkController>();
        currentMana = maxMana;
    }

    // Start is called before the first frame update
    void Start()
    {
        EquipSpell(fireSpell);
        EquipSpell(iceSpell);
        EquipSpell(earthSpell);
        selectedElementType = EElementType.FIRE;
        UpdatePlayerMaterials(selectedElementType);  
    }

    // Update is called once per frame
    void Update()
    {
        infiniteManaTimeRemaining -= Time.deltaTime;
        if (HasInfiniteMana)
        {
            currentMana = maxMana;
        }

        if (currentTarget)
        {
            if (Input.GetKeyDown(KeyCode.E))
                targetLock = !targetLock;
        }
        else
        {
            targetLock = false;
        }

        if (currentTarget == null || currentTarget.isTargetDead || !targetLock || !currentTarget.isTargetVisible || !currentTarget.isTargetActive)
            currentTarget = Targetable.GetTargetedTargetable(targetingAngle, targetingRange);

        if (playerController.currentHealth > 0 && isCasting == false)
        {
            if (Input.mouseScrollDelta.y > 0)
            {
                selectedElementType = (EElementType)(((int)selectedElementType + 1) % 3);
                UpdatePlayerMaterials(selectedElementType);
            }
            else if (Input.mouseScrollDelta.y < 0)
            {
                selectedElementType = (EElementType)((int)selectedElementType - 1 < 0 ? 2 : (int)selectedElementType - 1);
                UpdatePlayerMaterials(selectedElementType);

            }
        }
        

        if (playerController.anim.GetCurrentAnimatorStateInfo(2).IsName("None") && playerController.isControllable)
        {
            if (currentSpell == null && canCastSpells)
            {
                int spellIndex = getSpellListIndex(ESpellSlotType.OFFENSIVE, selectedElementType);
                if (Input.GetMouseButton(0) && spellList[spellIndex] && currentMana - spellList[spellIndex].manaCost >= 0)
                {
                    currentSpell = CastSpell(spellList[spellIndex]);
                    StartCoroutine(currentSpell);
                }
                else
                {
                    currentManaCooldown += Time.deltaTime;
                    if(currentManaCooldown > manaCooldown)
                    {
                        currentMana += manaRegen * Time.deltaTime;
                        currentMana = Mathf.Clamp(currentMana, 1, maxMana);
                    }
                }
            }
        }
        else
        {
            StopCastingSpell();
        }

        
    }

    public void UpdatePlayerMaterials(EElementType element)
    {
        Material mat = (element == EElementType.FIRE? fireMaterial : element == EElementType.WATER? waterMaterial : earthMaterial);

        foreach(Renderer r in renderers)
        {
            r.material = mat;
        }

    }

    public int getSpellListIndex(ESpellSlotType slot, EElementType eElement)
    {
        return (int)eElement + (2 * (int)eElement) + (int)slot;
    }


    public void EquipSpell(SpellDefinition spell)
    {
        if (spell == null)
            return;

        int spellIndex = getSpellListIndex(spell.spellSlotType, spell.spellElementType);

        if(spellList[spellIndex] != null)
        {
            UnEquipSpell(spellIndex);
        }

        spellList[spellIndex] = spell;

        spellObjects[spellIndex] = new List<PlayerEffectManager>();

        foreach (SpellDefinition.SpellEffectObjects efectObject in spell.spellEffects)
        {
            if(efectObject.effectEnabled == false)
            {
                continue;
            }
            Transform effectRootBone = playerController.transform;
            if (efectObject.useRootBone)
            {
                effectRootBone = playerController.anim.GetBoneTransform(efectObject.rootBone);
            }else if (playerController.anim)
            {
                effectRootBone =  playerController.anim.transform;
            }
            PlayerEffectManager effect = Instantiate(efectObject.effectObject.gameObject, effectRootBone.position, effectRootBone.rotation, effectRootBone).GetComponent<PlayerEffectManager>();
            effect.transform.localPosition = efectObject.rootBoneLocalOffset;
            effect.transform.localEulerAngles = efectObject.rootBoneLocalRotation;
            spellObjects[spellIndex].Add(effect);
            pfm = effect;
        }

        //Debug.Log("Equipped Spell: " + spell.name);
    }

    public void UnEquipSpell(int index)
    {
        if (spellList[index] == null)
            return;

        if (spellObjects[index].Count > 0)
        {
            foreach (PlayerEffectManager obj in spellObjects[index])
            {
                Destroy(obj.gameObject);
            }
        }

        spellObjects[index].Clear();
        Debug.Log("Equipped Spell: " + spellList[index].name);

        spellList[index] = null;
    }

    IEnumerator CastSpell(SpellDefinition spell)
    {
        bool spawnedProjectile = false;
        //Debug.Log("Casting Spell " + spell.name);
        float elapsedTime = 0;
        List<ParticleSystem> particles = new List<ParticleSystem>();
        List<AudioSource> audio = new List<AudioSource>();
        playerController.anim.SetTrigger(spell.animationTrigger);

        foreach (PlayerEffectManager effect in spellObjects[spellList.IndexOf(spell)])
        {
            if (playerController.anim)
            {
                effect.ActivateEffect();
            }
        }
        float originalBodyIkWeight = playerIkController.lookatBodyRatio;
        while(spell.spellDuration > elapsedTime && stopCastingSpell == false)
        {


            playerController.alignToMoveDirection = (elapsedTime < spell.alignPlayerToCameraForwardStartTime || elapsedTime > spell.alignPlayerToCameraForwardEndTime) && !(Input.GetMouseButton((int)spell.spellSlotType) && spell.forceAlignWhenLooping);
            playerController.forceWalk = (elapsedTime > spell.forceWalkStartTime && elapsedTime < spell.forceWalkEndTime) || (Input.GetMouseButton((int)spell.spellSlotType) && spell.forceWalkWhenLooping);
            playerIkController.lookatBodyRatio = Mathf.Lerp(originalBodyIkWeight, 1, spell.playerBodyIkWeight.Evaluate(Mathf.InverseLerp(0, spell.spellDuration, elapsedTime)));
            if (currentTarget)
            {
                playerController.characterForwardFacingOverride = currentTarget.transform.position - transform.position;
                playerIkController.lookatIkPosition = currentTarget.targetPosition;
            }
            else{
                playerController.characterForwardFacingOverride = Vector3.zero;
                playerIkController.lookatIkPosition = transform.position + CamController.instance.GetCameraDirection() * 1000;

            }

            if (spawnedProjectile == false && spell.projectile && elapsedTime >= spell.projectileCreationTime)
            {
                spawnedProjectile = true;
                Transform effectRootBone = playerController.transform;
                if (spell.useProjectileRootBone)
                {
                    effectRootBone = playerController.anim.GetBoneTransform(spell.projectileRootBone);
                }
                else if (playerController.anim)
                {
                    effectRootBone = playerController.anim.transform;
                }

                GameObject projectile = Instantiate(spell.projectile, effectRootBone.TransformPoint(spell.projectileRootPositionOffset), effectRootBone.rotation);
               

                Vector3 rightVector = (spell.projectileForward == SpellDefinition.EForwardOrientation.CharacterForward ? playerController.transform.right : CamController.instance.transform.right);
                Vector3 upVector = (spell.projectileUp == SpellDefinition.EUpOrientation.CameraUp ? CamController.instance.transform.up : Vector3.up);
                if(spell.projectileForward != SpellDefinition.EForwardOrientation.TargetForward || currentTarget == null)
                    projectile.transform.rotation = Quaternion.LookRotation(Vector3.Cross(rightVector, upVector), upVector);
                else
                    projectile.transform.LookAt(currentTarget.transform);

                if (projectile.GetComponent<Projectile>())
                {
                    if (projectile.GetComponentInChildren<Collider>())
                    {
                        //Debug.Log("Ignore Collision");
                        Physics.IgnoreCollision(projectile.GetComponentInChildren<Collider>(), GetComponent<Collider>(), true);
                    }
                    projectile.GetComponent<Projectile>().creator = this.gameObject;
                    if (projectile.GetComponent<Projectile>().useAimAssist && currentTarget)
                    {
                        projectile.GetComponent<Projectile>().target = currentTarget.gameObject;
                    }
                }
                //Debug.Log("SPAWN PROJECTILE");
            }

            yield return null;
            elapsedTime += Time.deltaTime;

            if(!HasInfiniteMana)
                currentMana -= (spell.manaCost * Time.deltaTime) / spell.spellDuration;
        }
        playerController.characterForwardFacingOverride = Vector3.zero;
        playerController.alignToMoveDirection = true;
        playerController.forceWalk = false;
        playerIkController.lookatBodyRatio = originalBodyIkWeight;
        playerIkController.lookatIkPosition = Vector3.zero;
        currentSpell = null;
        stopCastingSpell = false;
        currentManaCooldown = 0;
        //Debug.Log("Finished Casting " + spell.name);
    }

    public void StopCastingSpell()
    {
        if(currentSpell != null)
        {
            stopCastingSpell = true;
        }
    }

    
}

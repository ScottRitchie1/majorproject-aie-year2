﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSpell : MonoBehaviour
{
    bool activated = false;
    public bool IsTrapActive
    {
        get
        {
            return activated;
        }
    }

    [Header("General Trap Settings")]
    public EElementType elementType;// elemtal type of the trap
    
    public GameObject activateEffects;// obejct that gets enabled when the trap is set off - ie effects and sounds
    [HideInInspector]
    public LayerMask enemyLayer = new LayerMask();//layer hit by the effect
    
    [Header("Activation")]
    public float activeTime = 10;//time the whole trap is active for
    public float activationRadius = 2;//radius enemys enter to avtivate the trap
    [Min(1)]
    public int requiredEnemysToActivate = 1;// amouhnt of enemies in the radius to activate teh trap

    [Header("Initial Danmage")]
    public float damageRadius = 2;//range that the trap damages enemies at
    public float initialStealerDamage = 4;//the damage to stealers
    public float initialAttackerDamage = 8;//damage to attackers
    // Start is called before the first frame update
    void Awake()
    {
        enemyLayer = LayerMask.GetMask("Enemies");
        LayerMask layer = LayerMask.GetMask("Default");
        RaycastHit hit;
        if(Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 2, layer))//align the trap with the ground normal
        {
            transform.position = hit.point;
            transform.rotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(activated == false)// activate the trap
        {
            if (Physics.OverlapSphere(transform.position, activationRadius, enemyLayer).Length >= requiredEnemysToActivate)
            Activate();
        }
        else
        {
            activeTime -= Time.deltaTime;

            if (activeTime < 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1,0,0,0.5f);
        Gizmos.DrawSphere(transform.position, damageRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, activationRadius);
    }

    public void Activate()//activate the trap
    {
        Collider[] hits = Physics.OverlapSphere(transform.position, damageRadius, enemyLayer);
        foreach (Collider hit in hits)
        {
            Stealer stealer = hit.GetComponent<Stealer>();
            if (stealer)
            {
                stealer.InflictDamage(initialStealerDamage, (int)elementType);
            }
            else
            {
                Attacker attacker = hit.GetComponent<Attacker>();
                if (attacker)
                {
                    attacker.InflictDamage(initialAttackerDamage, (int)elementType);
                }

            }
        }
        activateEffects.SetActive(true);
        activated = true;
    }
}

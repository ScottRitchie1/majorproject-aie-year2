﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targetable : MonoBehaviour
{
    public EElementType targetElement = EElementType.FIRE;// the element of this targetable

    public static List<Targetable> targetables = new List<Targetable>();//holds a list of targetable Obejcts
    public static LayerMask ignoreLayer = ~0;// the layer to ignore targets
    private IEntity entity;//hold the target entity if it has one
    public IEntity TargetEntity
    {
        get
        {
            return entity;
        }
    }
    public Transform customTarget;// set a custom target point
    private Rigidbody rbTarget;// if no custom target, use the center of mass of the rigidbody;
    private Collider colliderTarget;// if no custom target or rb target, use the center of the collider

    public Vector3 targetPosition// this gets the target position either, the custom target, rg center of mass, collider center or the position
    {
        get
        {
            return (customTarget ? customTarget.position : ((rbTarget && rbTarget.isKinematic == false) ? rbTarget.worldCenterOfMass : (colliderTarget ? colliderTarget.bounds.center : transform.position)));
        }
    }

    public bool isTargetDead// if we have a target, and the target is dead
    {
        get
        {
            return (entity == null ? false : entity.GetHealthPercentage() <= 0);
        }
    }

    public bool isTargetActive// if the target is active in the Hierarchy
    {
        get
        {
            return gameObject.activeInHierarchy && this.enabled;
        }
    }

    public bool isTargetVisible// if the target is visable
    {
        get
        {
            return !Physics.Raycast(targetPosition, (Camera.main.transform.position - targetPosition).normalized, Vector3.Distance(targetPosition, Camera.main.transform.position), ignoreLayer, QueryTriggerInteraction.Ignore);
        }
    }

    private void Awake()
    {
        if(ignoreLayer == ~0)// set the layer mask, to ignore line of site for enemies and the player
        {
            ignoreLayer = ~LayerMask.GetMask("Character", "Enemies", "RagDollBones");
        }
        targetables.Add(this);// add this to the targetables list

        entity = GetComponent<IEntity>();// get our compionets
        rbTarget = GetComponent<Rigidbody>();
        colliderTarget = GetComponent<Collider>();

        if (GetComponent<Elemental>() != null)// if were an elemental, lets get our element
            targetElement = GetComponent<Elemental>().GetElement();
    }


    private void OnDestroy()
    {
        targetables.Remove(this);
    }

    // gets the current target from the main camera
    public static Targetable GetTargetedTargetable(float maxAngle, float maxDistance)
    {
        Targetable currentTargetable = null;
        float currentTargetableAngle = 0;

        for (int i = 0; i < targetables.Count; i++)// check each targetable
        {
            if (targetables[i].isTargetActive == false)// if the taret is active
                continue;

            if (targetables[i].isTargetDead)// if the target is dead
                continue;

            if (Vector3.Distance(Camera.main.transform.position, targetables[i].transform.position) > maxDistance)// if were in range
                continue;

            float angle = Vector3.Angle(Camera.main.transform.forward, targetables[i].transform.position - Camera.main.transform.position);

            if (angle > maxAngle)// if the angle is in range
                continue;

            if (!targetables[i].isTargetVisible)// if the target is visable
                continue;
            

            if (currentTargetable == null || angle < currentTargetableAngle)// if the angle is less then the curret checked angle
            {
                currentTargetable = targetables[i];
                currentTargetableAngle = angle;
            }
        }

        return currentTargetable;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//predict
//follow


[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{
    LayerMask hitLayers = new LayerMask();
    public GameObject creator;
    public GameObject target;
    Rigidbody targetRb;
    Collider targetCol;
    Rigidbody rb;
    float creationTime = 0;
    Vector3 lastPosition;
    public bool inheritVelocity = true;
    [Space]

    [Header("Projectile")]
    public float damageStealer = 10;
    public float damageAttacker = 10;
    public float damageShrine = 10;
    public float damageEgg = 10;

    public float radius = 0.1f;
    public float speed = 10;
    public float lifeTime = 10;
    public EElementType elementType;

    [Header("Impact")]
    public GameObject impactObject;
    public enum EImpactRotation { WorldUp, ImpactNormal, ProjectileDirection};
    public EImpactRotation impactRotation;
    public bool parentOnCollision = false;

    [Header("Targeting")]
    public bool useAimAssist = true;
    [Range(0, 10f)]
    public float autoAimAssist = 0;
    [Range(0, 1)]
    public float predictionSlider = 0;
    public AnimationCurve angleAimAssistScailerCurve = new AnimationCurve();


    [Header("Gravity")]
    public float gravityMultiplier = 1;
    public AnimationCurve gravityLifeTimeCurve = new AnimationCurve();

    public Vector3 targetPosition
    {
        get
        {
            return (targetRb && targetRb.isKinematic == false ? targetRb.worldCenterOfMass : (targetCol ? targetCol.bounds.center : target.transform.position));
        }
    }
    private void Start()
    {
        hitLayers = ~LayerMask.GetMask("RagDollBones", "Debris", "Ignore Raycast");
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        if (target)
        {
            //Vector3 predictedPosition = Interception.FirstOrderIntercept(transform.position, Vector3.zero, speed, targetPosition, targetRb.velocity);
            //transform.LookAt((Vector3.Lerp(targetPosition, predictedPosition, predictionSlider)));
            targetRb = target.GetComponent<Rigidbody>();
            targetCol = target.GetComponent<Collider>();
        }
        rb.velocity = transform.forward * speed;
        if (creator.GetComponent<Rigidbody>())
            rb.AddForce(creator.GetComponent<Rigidbody>().velocity, ForceMode.Acceleration);
        lastPosition = transform.position;
        creationTime = Time.time;
    }

    private void FixedUpdate()
    {
        float lifeTimeScalier = Mathf.InverseLerp(creationTime, creationTime + lifeTime, Time.time);
        //gravity
        rb.AddForce(Physics.gravity * gravityMultiplier * gravityLifeTimeCurve.Evaluate(lifeTimeScalier), ForceMode.Acceleration);

        //targeting
        if (target != null)
        {
            Vector3 targetPos = targetPosition;
            Vector3 predictedPosition = targetPos;
            if (targetRb)
            {
                predictedPosition = Interception.FirstOrderIntercept(transform.position, Vector3.zero, speed, targetPos, targetRb.velocity);
            }
            Debug.DrawLine(transform.position, predictedPosition);
            Vector3 directionToTarget = (Vector3.Lerp(targetPos, predictedPosition, predictionSlider) - transform.position).normalized;

            rb.AddForce(((directionToTarget * speed) - rb.velocity) * (autoAimAssist * Time.fixedDeltaTime) * (1- lifeTimeScalier) * angleAimAssistScailerCurve.Evaluate(Vector3.Angle(transform.forward, (targetPos - transform.position).normalized)), ForceMode.VelocityChange);
        }


        RaycastHit hit;
        Vector3 direction = (transform.position - lastPosition);
        Debug.DrawRay(lastPosition, direction, Color.red);
        if (Physics.SphereCast(lastPosition - direction.normalized * radius, radius, direction.normalized, out hit, direction.magnitude + radius, hitLayers, QueryTriggerInteraction.Ignore))
        {
            if(creator == null || (creator && creator.GetComponentInParent<Collider>() != hit.collider))
            {
                IEntity entity = hit.transform.gameObject.GetComponent<IEntity>();
                Rigidbody hitRB = hit.transform.GetComponent<Rigidbody>();
                if (hitRB)
                {
                    hitRB.AddForce(rb.velocity * rb.mass);
                }

                if (entity != null)
                {

                    if (hit.transform.gameObject.GetComponent<Attacker>())
                    {
                        entity.InflictDamage(damageAttacker, (int)elementType);

                    }
                    else if (hit.transform.gameObject.GetComponent<AttackerEgg>())
                    {
                        entity.InflictDamage(damageEgg, (int)elementType);

                    }
                    else if (hit.transform.gameObject.GetComponent<Shrine>())
                    {
                        entity.InflictDamage(damageShrine, (int)elementType);

                    }
                    else
                    {
                        entity.InflictDamage(damageStealer, (int)elementType);
                    }

                    //Debug.Log("PROJECTILE HIT");
                }

                if (impactObject)
                {
                    if(impactObject.transform.root == transform.root)
                    {
                        impactObject.transform.SetParent((parentOnCollision ? hit.collider.transform : null));
                    }
                    else
                    {
                        impactObject = Instantiate(impactObject, (parentOnCollision ? hit.collider.transform : null));
                    }

                    impactObject.transform.position = hit.point;
                    impactObject.SetActive(true);
                    if(impactRotation == EImpactRotation.ImpactNormal)
                    {
                        impactObject.transform.rotation = Quaternion.LookRotation(-hit.normal);
                    }else if(impactRotation == EImpactRotation.ProjectileDirection)
                    {
                        impactObject.transform.rotation = Quaternion.LookRotation(rb.velocity.normalized);
                    }
                    else
                    {
                        impactObject.transform.rotation = Quaternion.LookRotation(Vector3.down);
                    }

                }
                //Debug.Log("Projectile hit " + hit.transform.gameObject.name);
                Destroy(gameObject);
            }
            

        }


        transform.rotation = Quaternion.LookRotation(rb.velocity.normalized);
        lastPosition = transform.position;
    }
    
}

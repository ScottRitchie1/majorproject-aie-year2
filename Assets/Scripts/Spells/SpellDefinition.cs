﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EElementType { FIRE, WATER, EARTH};
public enum ESpellSlotType { OFFENSIVE, DEFENSIVE, SUPPORT };
[CreateAssetMenu(fileName = "New Spell", menuName = "Create New Spell", order = 1)]
public class SpellDefinition : ScriptableObject
{
    [System.Serializable]
    public class SpellEffectObjects
    {
        public bool effectEnabled = true;
        public PlayerEffectManager effectObject;
        public bool useRootBone = true;
        public HumanBodyBones rootBone;
        public Vector3 rootBoneLocalOffset;
        public Vector3 rootBoneLocalRotation;
    }

    public enum EForwardOrientation { CharacterForward, CameraForward, TargetForward};
    public enum EUpOrientation { WorldUp, CameraUp };

    [Header("General")]
    public string spellName;
    public string spellDescription;
    public EElementType spellElementType;
    public ESpellSlotType spellSlotType;
    public float spellDuration;
    public bool Loopable = true;
    [Space]
    [Min(0)]
    public float manaCost = 10;

    [Header("Player")]
    public bool forceAlignWhenLooping = true;
    public float alignPlayerToCameraForwardStartTime = 0;
    public float alignPlayerToCameraForwardEndTime = 0;
    [Space]
    public bool forceWalkWhenLooping = true;
    public float forceWalkStartTime = 0;
    public float forceWalkEndTime = 0;
    [Space]
    public AnimationCurve playerBodyIkWeight = new AnimationCurve();

    [Header("Function")]
    public GameObject projectile;
    public float projectileCreationTime = 0;
    public bool useProjectileRootBone = true;
    public HumanBodyBones projectileRootBone;
    public Vector3 projectileRootPositionOffset;
    public EForwardOrientation projectileForward;
    public EUpOrientation projectileUp;

    [Header("Animation")]
    public string animationTrigger;
    public Vector2 animationTriggerActivationTimes;

    [Header("Effects")]
    public List<SpellEffectObjects> spellEffects;

    
}



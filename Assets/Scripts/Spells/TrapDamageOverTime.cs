﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrapSpell))]
public class TrapDamageOverTime : MonoBehaviour
{
    [Header("Damage Over Time")]
    public float activeTimeSeconds = 5;// how long the damage radius is active for
    public float damageRadius = 4;
    float activatedTime = 0;
    TrapSpell trap;
    public float damageIntervals = 0.5f;//when the damage is done
    public float stealerDamage = 3;//damage to a stealer per second
    public float attackerDamage = 6;//damage to an attacker per second
                                              // Start is called before the first frame update

    List<Collider> enemyTracker = new List<Collider>();
    void Awake()
    {
        trap = GetComponent<TrapSpell>();
    }

    // Update is called once per frame
    void Update()
    {
        if(trap.IsTrapActive )
        {
            if(activatedTime == 0)
                activatedTime = Time.time;

            if (Time.time < activatedTime + activeTimeSeconds)
            {
                Collider[] hits = Physics.OverlapSphere(transform.position, damageRadius, trap.enemyLayer);
                foreach (Collider hit in hits)
                {
                    
                    if(enemyTracker.Contains(hit) == false)
                    {
                        StartCoroutine(OnEnemyEnterTrap(hit));
                    }
                    //Stealer stealer = hit.GetComponent<Stealer>();
                    //if (stealer)
                    //{
                    //    stealer.InflictDamage(stealerDamagerPerSecond * Time.deltaTime, (int)trap.elementType);
                    //}
                    //else
                    //{
                    //    Attacker attacker = hit.GetComponent<Attacker>();
                    //    if (attacker)
                    //    {
                    //        attacker.InflictDamage(attackerDamagerPerSecond * Time.deltaTime, (int)trap.elementType);
                    //    }
                    //
                    //}
                }
            }
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0.92f, 0.016f, 0.1f);
        Gizmos.DrawSphere(transform.position, damageRadius);
    }

    IEnumerator OnEnemyEnterTrap(Collider enemyCollider)
    {

        IEntity entity = enemyCollider.GetComponent<IEntity>();
        
        if (entity != null) {
            enemyTracker.Add(enemyCollider);
            int removeIndex = enemyTracker.Count - 1;
            Stealer isStealer = enemyCollider.GetComponent<Stealer>();
            Attacker isAttacker = enemyCollider.GetComponent<Attacker>();

            while (enemyCollider && Time.time < activatedTime + activeTimeSeconds && Vector3.Distance(enemyCollider.ClosestPoint(transform.position), transform.position) <= damageRadius)
            {

                if (damageIntervals == 0)
                {
                    if (isStealer)
                    {
                        entity.InflictDamage(stealerDamage * Time.deltaTime, (int)trap.elementType);

                    }
                    else if (isAttacker)
                    {
                        entity.InflictDamage(attackerDamage * Time.deltaTime, (int)trap.elementType);
                    }
                    yield return null;
                }
                else
                {
                    if (isStealer)
                    {
                        entity.InflictDamage(stealerDamage, (int)trap.elementType);

                    }
                    else if (isAttacker)
                    {
                        entity.InflictDamage(attackerDamage, (int)trap.elementType);
                    }
                    yield return new WaitForSeconds(damageIntervals);
                }
            }
            if (enemyCollider)
            {
                enemyTracker.Remove(enemyCollider);
            }
            else
            {
                for(int i = 0; i < enemyTracker.Count; i++)// this just5 clears the emply entry
                {
                    if(enemyTracker[i] == null)
                    {
                        enemyTracker.RemoveAt(i);
                        break;
                    }
                }
            }
        }
        
    }
}

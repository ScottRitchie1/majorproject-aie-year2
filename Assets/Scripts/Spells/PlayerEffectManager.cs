﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffectManager : MonoBehaviour
{
    [ContextMenu("Play Effect")]
    public void ActivateEffect()
    {
        if (GetComponent<ParticleSystem>())
        {
            GetComponent<ParticleSystem>().Play(true);
        }

        if (GetComponent<RandomiseSound>())
        {
            GetComponent<RandomiseSound>().PlaySound();
        }
        else if (GetComponent<AudioSource>())
        {
            GetComponent<AudioSource>().Play();

        }

        if (GetComponent<LightController>())
        {
            GetComponent<LightController>().Restart();
        }
    }
}

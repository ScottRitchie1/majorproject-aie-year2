﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviveUI : MonoBehaviour
{
    PlayerController player;
    RectTransform rect;
    Text rangeText;
    Image image;

    public void Awake()
    {
        player = FindObjectOfType<PlayerController>();
        rect = GetComponent<RectTransform>();
        rangeText = GetComponentInChildren<Text>();
        image = GetComponent<Image>();
    }
    // Update is called once per frame
    void Update()
    {
        if (CrystalChunk.revivingChunk)
        {
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(CrystalChunk.revivingChunk.transform.position);

            if (image.enabled == false && screenPosition.z > 0)
            {
                image.enabled = true;
                rangeText.enabled = true;
            }else if(image.enabled == true && screenPosition.z < 0)
            {
                image.enabled = false;
                rangeText.enabled = false;
            }

            rect.position = screenPosition;

            if (rangeText && player)
            {
                rangeText.text = Mathf.Round(Vector3.Distance(player.transform.position, CrystalChunk.revivingChunk.transform.position)).ToString() + "m";
            }
        }
        else
        {
            if (image.enabled == true)
            {
                image.enabled = false;
                rangeText.enabled = false;
            }
        }
    }
}

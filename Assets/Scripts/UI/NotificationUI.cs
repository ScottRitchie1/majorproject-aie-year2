﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationUI : MonoBehaviour
{
    public Text notificationText;
    public bool showEventNotification;
    public bool showWaveNotification;
    public bool showRevivingNotification;
    public float fadeTime = 1;
    public float activeTime = 5;
    WaveManager waveManager;
    PlayerController player;
    int displayedWave = -1;
    int displayedEvent = 0;
    float currentNotificatonTimeStamp = -1000;
    
    
    // Start is called before the first frame update

    private void Awake()
    {
        waveManager = FindObjectOfType<WaveManager>();
        player = FindObjectOfType<PlayerController>();
        if (!waveManager)
        {
            enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (notificationText)
        {
            if(showRevivingNotification && player && player.currentHealth <= 0)
            {
                currentNotificatonTimeStamp = Time.time - activeTime - fadeTime;
                notificationText.text = "REVIVING...";
            }
            else if (currentNotificatonTimeStamp + activeTime + fadeTime + fadeTime < Time.time) {
                if (showWaveNotification && displayedWave != waveManager.CurrentWave)
                {
                    displayedWave = waveManager.CurrentWave;
                    currentNotificatonTimeStamp = Time.time;
                    notificationText.text = (waveManager.waves.Count == waveManager.CurrentWave + 1 ? "FINAL WAVE!" : "WAVE " + (waveManager.CurrentWave + 1).ToString());
                }
                else if (showEventNotification && WaveManager.CurrentShrineEvent != WaveManager.E_ShrineEvent.Inactive && displayedEvent == 0)
                {
                    //Fire, Water, Earth
                    displayedEvent = (int)WaveManager.CurrentShrineEvent;
                    currentNotificatonTimeStamp = Time.time;
                    notificationText.text = (displayedEvent == 1? "FIRE" : (displayedEvent == 2? "ICE" : "EARTH")) + " EVENT HAS STARTED";
                }
            }



            if (Time.time < currentNotificatonTimeStamp + fadeTime + activeTime + fadeTime)
            {
                float transparency = notificationText.color.a;

                if (Time.time < currentNotificatonTimeStamp + fadeTime + activeTime )
                {
                    if(Time.time < currentNotificatonTimeStamp + fadeTime)
                    {
                        //start
                        transparency = Mathf.InverseLerp(currentNotificatonTimeStamp, currentNotificatonTimeStamp + fadeTime, Time.time);
                    }
                    else
                    {
                        //middle
                        transparency = 1;
                    }
                }
                else
                {
                    //end
                    transparency = Mathf.InverseLerp(currentNotificatonTimeStamp + fadeTime + activeTime + fadeTime, currentNotificatonTimeStamp + fadeTime + activeTime, Time.time);
                }

                notificationText.color = new Color(notificationText.color.r, notificationText.color.g, notificationText.color.b, transparency);
            } else {
                // ui not active
                notificationText.color = new Color(notificationText.color.r, notificationText.color.g, notificationText.color.b, 0);
            }
        }
    }
}

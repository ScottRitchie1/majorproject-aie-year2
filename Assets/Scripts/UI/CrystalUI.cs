﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalUI : MonoBehaviour
{
    public Text remainingCrystalText = null;

    // Update is called once per frame
    void Update()
    {
        if(remainingCrystalText != null && Shrine.playerShrine)
        {
            remainingCrystalText.text = Shrine.playerShrine.thisShrineCrystals.Count.ToString();
        }
    }
}

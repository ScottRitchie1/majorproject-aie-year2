﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetingUI : MonoBehaviour
{
    PlayerSpellCaster caster;// player

    public Scrollbar targetHealthBar;

    public Image targetReticle;
    public Image targetLockReticleImage;

    public Sprite defaultReticle;// the default reticle when targeting something thats NOT effective
    //public Sprite effectiveReticle;// the reticle when targeting something thats IS Effective
    public Sprite targetLockReticle;// the target locki redicle that we toggle on and off

    //public float effectivnessColourPulseSpeed = 5;//rate at which the ui pulses when effective

    //public Color fireColour = Color.red;// the colour we pulse when targeting a fire enemy with an effective elemnt type
    //public Color waterColour = Color.blue;// the colour we pulse when targeting a water enemy with an effective elemnt type
    //public Color earthColour = Color.green;// the colour we pulse when targeting a earth enemy with an effective elemnt type
    public Gradient healthBarGradient;
    public List<Image> gradientImages = new List<Image>();

    private void Awake()
    {
        caster = FindObjectOfType<PlayerSpellCaster>();// get the reference to the player
        if (!caster)
            this.enabled = false;

        targetLockReticleImage.sprite = targetLockReticle;
        targetReticle.sprite = defaultReticle;
    }

    // Update is called once per frame
    void Update()
    {
        targetReticle.rectTransform.position = caster.currentTarget ? Camera.main.WorldToScreenPoint(caster.currentTarget.targetPosition) : -Vector3.forward;


         if (targetReticle.rectTransform.position.z > 0)// if we have a target that is infront of us
         {
             targetReticle.enabled = true;// enable the ui
             targetLockReticleImage.enabled = caster.IsTargetLocking;// update targetlock ui state

            //update the health bar if the target is an entity
             if (caster.currentTarget.TargetEntity != null && caster.currentTarget.TargetEntity.GetHealthPercentage() < 1)
             {
                 targetHealthBar.gameObject.SetActive(true);
                targetHealthBar.size = caster.currentTarget.TargetEntity.GetHealthPercentage();
                foreach(Image img in gradientImages)
                {
                    img.color = healthBarGradient.Evaluate(targetHealthBar.size);
                }

            }
            else
             {
                targetHealthBar.gameObject.SetActive(false);
            }

             /*
               // update effectivness ui
             if (((int)caster.selectedElementType == 0 && (int)caster.currentTarget.targetElement == 2) || (int)caster.selectedElementType - 1 == (int)caster.currentTarget.targetElement)
             {
                // element is effective, pulse ui colours
                 targetReticle.color = Color.Lerp(Color.white, (caster.selectedElementType == EElementType.FIRE ? fireColour : (caster.selectedElementType == EElementType.EARTH ? earthColour : waterColour)), Mathf.PingPong(Time.time * effectivnessColourPulseSpeed, 1));

                 if (targetReticle.sprite != effectiveReticle)
                     targetReticle.sprite = effectiveReticle;
             }
             else
             {
                //ui is ineffective
                 targetReticle.color = Color.white;
                 if (targetReticle.sprite != defaultReticle)
                     targetReticle.sprite = defaultReticle;
             }
             */
         }
        else// if we dont have a target
        {
            targetHealthBar.gameObject.SetActive(false);

            targetReticle.enabled = false;
            targetLockReticleImage.enabled = false;
        }
    }
}

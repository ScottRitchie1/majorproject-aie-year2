﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ReviveButton : MonoBehaviour
{
    Button button;
    Text buttonText;
    PlayerController player;
    string text = "";


    private void Awake()
    {
        button = GetComponentInChildren<Button>();
        buttonText = GetComponentInChildren<Text>();
        text = buttonText.text;
        player = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (player && player.currentHealth > 0 && button.gameObject.activeSelf == true)
        {
            buttonText.text = text;
            button.gameObject.SetActive(false);
            CamController.instance.LockCursor(true);
        }
        else if (player && player.currentHealth <= 0 && button.gameObject.activeSelf == false)
        {
            CamController.instance.LockCursor(false);
            button.gameObject.SetActive(true);
        }
    }

    public void Revive()
    {
        CrystalChunk.RevivePlayer(player);
        text = buttonText.text;
        buttonText.text = "Reviving...";
        }
}

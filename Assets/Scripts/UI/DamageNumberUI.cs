﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DamageNumberUI : MonoBehaviour
{
    public Text textPrefab;
    public float fadeOutTime = 1;
    public float activeTime = 1;
    public float floatSpeedY = 0.5f;
    public float floatSpeedX = 0.5f;
    float timeStamp = 0;

    public Color defaultColour = Color.white;
    public Color effectiveColour = Color.yellow;

    //public Color fireColour = Color.red;
    //public Color waterColour = Color.blue;
    //public Color earthColour = Color.green;

    class NumberElement
    {
        public Text text;
        public float timeStamp;
        public Vector2 offset;
        public Transform transform;
    }

    List<NumberElement> numberElements = new List<NumberElement>();
    //public Color defaultColour = Color.white;
    //public Color 


    private void Awake()
    {
        Stealer.OnTakeDamageStaticEvent.AddListener(OnAddDamageNumber);
        Attacker.OnTakeDamageStaticEvent.AddListener(OnAddDamageNumber);
        AttackerEgg.OnTakeDamageStaticEvent.AddListener(OnAddDamageNumber);
        Shrine.OnTakeDamageStaticEvent.AddListener(OnAddDamageNumber);
    }

    private void Update()
    {
        for(int i = 0; i < numberElements.Count; i++)
        {
            NumberElement element = numberElements[i];

            if (element.transform == null)
            {
                Destroy(element.text.gameObject);
                numberElements.Remove(element);
                i--;
                continue;
            }


            Vector3 elementPos = Camera.main.WorldToScreenPoint(element.transform.position) + new Vector3(element.offset.x, element.offset.y, 0);
            if(elementPos.z > 0)
            {
                if (element.text.enabled == false)
                    element.text.enabled = true;

                element.text.transform.position = elementPos;
                element.offset.y += floatSpeedY * Time.deltaTime;
                element.offset.x +=  (element.offset.x > 0 ? floatSpeedX : -floatSpeedX) * Time.deltaTime;

                element.text.color = new Color(element.text.color.r, element.text.color.g, element.text.color.b, Mathf.InverseLerp(element.timeStamp + activeTime + fadeOutTime, element.timeStamp + activeTime, Time.time));
                if(Time.time > element.timeStamp + activeTime + fadeOutTime)
                {
                    Destroy(element.text.gameObject);
                    numberElements.Remove(element);
                    i--;
                }
            }
            else if(elementPos.z > 0 && element.text.enabled == true)
            {
                element.text.enabled = false;

            }
        }
    }


    public void OnAddDamageNumber(Transform t, float damage, EElementType elementType, bool wasEffective = false)
    {
        NumberElement numberElement = new NumberElement();
        numberElement.transform = t;
        
        numberElement.text = Instantiate(textPrefab, transform);
        numberElement.text.enabled = false;



        numberElement.text.text = damage.ToString();
        numberElement.offset.x = Random.Range(0, 1) == 1? -0.01f : 0.01f;
        if (wasEffective)
        {
            numberElement.text.fontStyle = FontStyle.Bold;
            numberElement.text.fontSize = (int)(numberElement.text.fontSize * 1.5f);
            numberElement.text.color = effectiveColour;//(elementType == EElementType.FIRE ? fireColour : (elementType == EElementType.WATER ? waterColour : earthColour));
        }
        else
        {
            numberElement.text.color = defaultColour;
        }

        numberElement.timeStamp = Time.time;
        numberElements.Add(numberElement);
    }
}

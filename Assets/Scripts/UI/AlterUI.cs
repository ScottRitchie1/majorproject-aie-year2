﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlterUI : MonoBehaviour
{
    WaveManager wm;

    public Sprite earthIcon;
    public Sprite fireIcon;
    public Sprite waterIcon;

    public Color earthColour = Color.green;
    public Color fireColour = Color.red;
    public Color waterColour = Color.blue;

    [Space]
    public Image iconImage;
    public Scrollbar fillBar;
    Image fillBarImage;
    public Image glowImage;
    public Text timerText;

    [Space]
    public float glowFade = 1;
    public float glowIntensity = 1;
    float shrineActivationTime;
    public Color fillbarDefaultColour;


    // Start is called before the first frame update
    void Start()
    {
        wm = FindObjectOfType<WaveManager>();
        shrineActivationTime = FindObjectOfType<EventAlter>().timeBetweenActivations;
        fillBarImage = fillBar.handleRect.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (wm)
        {
            if(WaveManager.CurrentShrineEvent != WaveManager.E_ShrineEvent.Inactive)
            {
                Color elementColour = WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Fire ? fireColour : WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Water ? waterColour : earthColour;
                glowImage.color = new Color(
                    Mathf.MoveTowards(glowImage.color.r, elementColour.r, Time.deltaTime * glowFade),
                    Mathf.MoveTowards(glowImage.color.g, elementColour.g, Time.deltaTime * glowFade),
                    Mathf.MoveTowards(glowImage.color.b, elementColour.b, Time.deltaTime * glowFade), 
                    Mathf.MoveTowards(glowImage.color.a, glowIntensity, Time.deltaTime * glowFade));
                timerText.text = Mathf.Round(wm.ShrineEventRemainingActiveTime).ToString();
                fillBar.size = Mathf.InverseLerp(0, wm.shrineEventActiveTime, wm.ShrineEventRemainingActiveTime);
                if (iconImage.enabled == false)
                {
                    iconImage.color = elementColour;
                    iconImage.enabled = true;
                    iconImage.sprite = WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Fire ? fireIcon : WaveManager.CurrentShrineEvent == WaveManager.E_ShrineEvent.Water ? waterIcon : earthIcon;
                    fillBarImage.color = elementColour;

                }

            }
            else
            {
                if (iconImage.enabled == true)
                {
                    iconImage.enabled = false;
                    fillBarImage.color = fillbarDefaultColour;
                    
                }

                if (EventAlter.lastActivatedTimeStamp + shrineActivationTime > Time.time)
                {
                    float remianingTimeTillActivatable = EventAlter.lastActivatedTimeStamp + shrineActivationTime - Time.time;
                    timerText.text = Mathf.Round(remianingTimeTillActivatable).ToString();
                    glowImage.color = new Color(
                        Mathf.MoveTowards(glowImage.color.r, 1, Time.deltaTime * glowFade),
                        Mathf.MoveTowards(glowImage.color.g, 1, Time.deltaTime * glowFade),
                        Mathf.MoveTowards(glowImage.color.b, 1, Time.deltaTime * glowFade),
                        Mathf.MoveTowards(glowImage.color.a, 0, Time.deltaTime * glowFade));

                    fillBar.size = Mathf.InverseLerp(shrineActivationTime, 0, remianingTimeTillActivatable);
                }
                else
                {
                    if(timerText.text.Length > 0)
                    {
                        timerText.text = "";
                    }
                    fillBar.size = 1;
                    glowImage.color = new Color(
                        Mathf.MoveTowards(glowImage.color.r, 1, Time.deltaTime * glowFade),
                        Mathf.MoveTowards(glowImage.color.g, 1, Time.deltaTime * glowFade),
                        Mathf.MoveTowards(glowImage.color.b, 1, Time.deltaTime * glowFade),
                        Mathf.MoveTowards(glowImage.color.a, glowIntensity, Time.deltaTime * glowFade));
                }
            }
        }
    }
}

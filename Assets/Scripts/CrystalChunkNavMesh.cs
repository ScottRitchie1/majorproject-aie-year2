﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CrystalChunkNavMesh : MonoBehaviour
{
    public static NavMeshSurface navmesh;
    public int agentID;
    private void Awake()
    {
        navmesh = GetComponent<NavMeshSurface>();
        agentID = navmesh.agentTypeID;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
using UnityEngine.Rendering.PostProcessing;

public enum ERoutes { Path1 = 3, Path2, Path3, Path4, Path5, Path6, Path7, Path8};
[System.Serializable]
public class ShrineRoutes
{
    public ERoutes route;

    public List<ERoutes> routes = new List<ERoutes>();
}

public class Shrine : MonoBehaviour, IEntity, Elemental
{
    public static PlayerEngine player;
    public static Shrine playerShrine;
    public static Shrine fireShrine;
    public static Shrine earthShrine;
    public static Shrine iceShrine;

    AudioSource audioSource;
    public AudioSource ShrineAudioSource
    {
        get { return audioSource; }
    }
    public enum shrineType { PLAYER, FIRE, ICE, EARTH };
    public shrineType thisShrineType;

    public List<CrystalChunk> thisShrineCrystals = new List<CrystalChunk>();

    public float playerRetrieveCrystalRange = 5;
    public float playerRetrieveCrystalRate = 1;

    public PostProcessVolume shrinePostProcessing = null;

    [Header("Cystal Animation")]
    public Vector2 rotationSpeedRange = new Vector2(1, 2);
    public Vector2 heightRange = new Vector2(2, 5);
    public Vector2 radiusRange = new Vector2(1, 2);

    [Header("Health")]
    public float maxHealth = 100;
    public float currentHealth = 0;
    [HideInInspector]
    public bool canDamage = false;
    public Transform damageNumberPoint = null;
    [Space]
    public ParticleSystem shrineDieParticle;
    public AudioClip shrineDieSound;
    [Header("Spawning")]
    public List<Transform> spawnPoints = new List<Transform>();

    public Attacker attacker;
    public Transform eggSpawn;
    public float eggSpawnRadius;
    public Stealer stealer;
    public int crystalsRequired = 10;
    public float attackerSpawnTimer = 30;
    float timeTillSpawnNewAttacker = 0;
    [HideInInspector]
    public bool canSpawnAttackers = true;
    [Space]
    public ParticleSystem spawnEnemiesParticle;
    public AudioClip spawnEnemiesSound;
    [Header("Navigation")]
    public List<ShrineRoutes> routes = new List<ShrineRoutes>();

    Targetable targetable;

    public UnityEvent OnShrineDie = new UnityEvent();
    public static UnityEvent OnPlayerShrineDie = new UnityEvent();
    public static UnityEvent OnEnemyShrinesDie = new UnityEvent();

    public static StaticTakeDamageEvent OnTakeDamageStaticEvent = new StaticTakeDamageEvent();

    public ParticleSystem shrinePassiveParticleSystem;
    public ParticleSystem shrineEventActiveParticleSystem;

    public List<Rigidbody> fracturedObjects = new List<Rigidbody>();

    public Renderer shrineBaseRenderer;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        currentHealth = maxHealth;
        if (!player)
        {
            player = FindObjectOfType<PlayerEngine>();
            if (!player)
            {
                this.enabled = false;
                Debug.LogError("CouldntFindPlayer", this);
            }
        }

        if (thisShrineType == shrineType.FIRE)
        {
            if (fireShrine == null)
            {
                fireShrine = this;
            }
            else
            {
                Debug.LogError("FIRE SHRINE EXISTS", this);
                this.enabled = false;
            }
        }
        else if (thisShrineType == shrineType.EARTH)
        {
            if (earthShrine == null)
            {
                earthShrine = this;
            }
            else
            {
                Debug.LogError("EARTH SHRINE EXISTS", this);
                this.enabled = false;
            }
        }
        else if (thisShrineType == shrineType.ICE)
        {
            if (iceShrine == null)
            {
                iceShrine = this;
            }
            else
            {
                Debug.LogError("ICE SHRINE EXISTS", this);
                this.enabled = false;
            }
        }
        else
        {
            if (playerShrine == null)
            {
                playerShrine = this;
            }
            else
            {
                Debug.LogError("PLAYER SHRINE EXISTS", this);
                this.enabled = false;
            }
        }

        targetable = GetComponent<Targetable>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (thisShrineType != shrineType.PLAYER)
        {
            StartCoroutine(retrieveCrystals());
        }

        if (shrinePassiveParticleSystem)
        {
            ParticleSystem.EmissionModule em = shrinePassiveParticleSystem.emission;
            em.enabled = true;
        }

        if (shrineEventActiveParticleSystem)
        {
            ParticleSystem.EmissionModule em = shrineEventActiveParticleSystem.emission;
            em.enabled = false;
        }

        foreach (Rigidbody obj in fracturedObjects)
        {
            obj.isKinematic = true;
            if (obj.GetComponent<ScaleCurvesController>()) obj.GetComponent<ScaleCurvesController>().enabled = false;
            if (obj.GetComponent<Collider>()) obj.GetComponent<Collider>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (thisShrineType == shrineType.PLAYER)
        {
            //if (thisShrineCrystals.Count > 0)
            //{
            //    if (Input.GetKeyDown(KeyCode.Alpha1))
            //        thisShrineCrystals[0].TeatherToShrine(fireShrine);
            //
            //    if (Input.GetKeyDown(KeyCode.Alpha2))
            //    {
            //        thisShrineCrystals[0].DropChunk();
            //
            //    }
            //}
        }
        else
        {
            if (thisShrineCrystals.Count >= crystalsRequired)
            {
                timeTillSpawnNewAttacker -= Time.deltaTime;

                if (timeTillSpawnNewAttacker <= 0 && canSpawnAttackers)
                {
                    //for (int i = 0; i < crystalsRequired; i++)
                    //{
                    //    GameObject crystalToDelete = thisShrineCrystals[0].gameObject;
                    //    thisShrineCrystals.RemoveAt(0);
                    //    Destroy(crystalToDelete);
                    //}

                    Transform spawn = spawnPoints[Random.Range((int)0, (int)spawnPoints.Count)];

                    Attacker tempAttacker = Instantiate(attacker, spawn.position, spawn.rotation);
                    timeTillSpawnNewAttacker = attackerSpawnTimer;
                }

            } else if (Input.GetKeyDown(KeyCode.Plus))
            {
                Debug.Log("SPWNING ATTACKER");
                Transform spawn = spawnPoints[Random.Range((int)0, (int)spawnPoints.Count)];

                Attacker tempAttacker = Instantiate(attacker, spawn.position, spawn.rotation);
                timeTillSpawnNewAttacker = attackerSpawnTimer;
            }
        }

        if (targetable)
            targetable.enabled = canDamage;
    }

    IEnumerator retrieveCrystals()
    {
        while (true)
        {
            yield return null;

            if (thisShrineCrystals.Count > 0 && Vector3.Distance(transform.position, player.transform.position) < playerRetrieveCrystalRange)
            {
                //retrieve crystal
                CrystalChunk chunk = thisShrineCrystals[0];
                thisShrineCrystals.RemoveAt(0);
                chunk.TeatherToShrine(Shrine.playerShrine);
                yield return new WaitForSeconds(playerRetrieveCrystalRate);
            }
        }
    }



    public void Die()
    {
        for (int i = 0; i < thisShrineCrystals.Count; i++)
        {
            thisShrineCrystals[0].DropChunk();
        }


        if (OnShrineDie != null)
        {
            OnShrineDie.Invoke();
        }

        Renderer[] rs = GetComponentsInChildren<Renderer>();

        foreach (Renderer r in rs)
        {
            r.material.color = Color.grey;
        }

        if (thisShrineType != shrineType.PLAYER)
        {
            if (fireShrine.currentHealth <= 0 && earthShrine.currentHealth <= 0 && iceShrine.currentHealth <= 0)
            {
                if (OnEnemyShrinesDie != null)
                    OnEnemyShrinesDie.Invoke();
                Debug.Log("GAME OVER: PLAYER WON");
            }
        }
        else
        {
            if (OnPlayerShrineDie != null)
                OnPlayerShrineDie.Invoke();
        }

        if (audioSource && shrineDieSound) audioSource.PlayOneShot(shrineDieSound);

        if (shrineDieParticle) shrineDieParticle.Play();

        Destroy(GetComponent<Collider>());
        StartCoroutine(fadeEmmision(1));

        foreach (Rigidbody obj in fracturedObjects)
        {
            obj.isKinematic = false;
            if (obj.GetComponent<ScaleCurvesController>()) obj.GetComponent<ScaleCurvesController>().enabled = true;
            if (obj.GetComponent<Collider>()) obj.GetComponent<Collider>().enabled = true;
        }

        if (shrinePassiveParticleSystem)
        {
            ParticleSystem.EmissionModule em = shrinePassiveParticleSystem.emission;
            em.enabled = false;
        }

        if (shrineEventActiveParticleSystem)
        {
            ParticleSystem.EmissionModule em = shrineEventActiveParticleSystem.emission;
            em.enabled = false;
        }

    }

    IEnumerator fadeEmmision(float time)
    {
        if (shrineBaseRenderer) {
            Color startColour = shrineBaseRenderer.material.GetColor("_EmissionColor");
            float timestamp = Time.time;
            while (timestamp + time > Time.time)
            {
                shrineBaseRenderer.material.SetColor("_EmissionColor", Color.Lerp(startColour, new Color(0, 0, 0, 0), Mathf.InverseLerp(timestamp, timestamp + time, Time.time)));
                yield return null;
            }
            shrineBaseRenderer.material.SetColor("_EmissionColor", new Color(0, 0, 0, 0));

        }
    }

    public void InflictDamage(float damage, int damageType)
    {
        bool wasEffective = false;
        if (canDamage)
        {
            if (thisShrineType != shrineType.PLAYER)
            {
                EElementType elementType = (EElementType)((int)thisShrineType - 1);
                if ((damageType == 0 && (int)elementType == 2) || damageType - 1 == (int)elementType)
                {
                    wasEffective = true;
                    //double damage
                    damage *= 3.0f;
                }

            }
        }

        currentHealth -= damage;

        if (OnTakeDamageStaticEvent != null)
        {
            OnTakeDamageStaticEvent.Invoke((damageNumberPoint ? damageNumberPoint : transform), damage, (EElementType)damageType, wasEffective);
        }

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public float GetHealthPercentage()
    {
        return Mathf.InverseLerp(0, maxHealth, currentHealth);
    }

    public EElementType GetElement()
    {
        return (EElementType)(thisShrineType - 1);
    }

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(50, 130, 250, 50), "PLAYER SHRINE HP: " + currentHealth.ToString());
    //}

    public void OnDrawGizmosSelected()
    {
        if (eggSpawn)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(eggSpawn.position, eggSpawnRadius);
        }

        Gizmos.color = Color.blue;
        foreach(Transform t in spawnPoints)
        {
            Gizmos.DrawSphere(t.position, 0.25f);

        }
    }

    public static Shrine GetShrineByElement(EElementType element)
    {
        return GetShrineByShrineType((shrineType)((int)element + 1));
    }

    public static Shrine GetShrineByShrineType(shrineType type)
    {
        switch (type)
        {
            case shrineType.FIRE:
                return fireShrine;
            case shrineType.ICE:
                return iceShrine;
            case shrineType.EARTH:
                return earthShrine;
            default:
                return playerShrine;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class RFX4_OnEnableResetTransform : MonoBehaviour {

    Transform t;
    Vector3 startPosition;
    Quaternion startRotation;
    Vector3 startScale;
    bool isInitialized;
    public bool deParent = false;
    Transform parent;

	void OnEnable () {
	    if(!isInitialized)
        {
            isInitialized = true;
            t = transform;
            startPosition = t.position;
            startRotation = t.rotation;
            startScale = t.localScale;
            parent = t.parent;
            t.SetParent(null);
        }
        else
        {
            t.position = startPosition;
            t.rotation = startRotation;
            t.localScale = startScale;
            t.SetParent(parent);
        }
	}

    void OnDisable()
    {
        if (!isInitialized)
        {
            isInitialized = true;
            t = transform;
            startPosition = t.position;
            startRotation = t.rotation;
            startScale = t.localScale;
            parent = t.parent;
        }
        else
        {
            t.position = startPosition;
            t.rotation = startRotation;
            t.localScale = startScale;
            t.gameObject.SetActive(false);
        }
    }
}

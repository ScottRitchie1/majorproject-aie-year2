﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlexMainMenu : MonoBehaviour
{
    // Play button to start game
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Skip tutorial
    public void SkipTutorial()
    {
        SceneManager.LoadScene(2);
    }

    // Quit button to exit game
    public void QuitGame()
    {
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public GameObject gameOverLoss;
    public GameObject gameOverWin;

    // Start is called before the first frame update
    void Start()
    {
        Shrine.OnPlayerShrineDie.AddListener(OnGameLose);
        Shrine.OnEnemyShrinesDie.AddListener(OnGameWin);
        CrystalChunk.OnPlayerUnrevivable.AddListener(OnGameLose);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGameLose()
    {
        Debug.Log("GAME LOST");
        gameOverLoss.SetActive(true);
        // Remove the player's control over the character
        FindObjectOfType<PlayerController>().isControllable = false;
        if (CamController.instance)
            CamController.instance.LockCursor(false);
    }

    void OnGameWin()
    {
        Debug.Log("GAME WON");
        gameOverWin.SetActive(true);
        // Remove the player's control over the character
        FindObjectOfType<PlayerController>().isControllable = false;
        if (CamController.instance)
            CamController.instance.LockCursor(false);
    }

    // Back to main menu
    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Quit to desktop
    public void QuitGame()
    {
        Application.Quit();
    }
}

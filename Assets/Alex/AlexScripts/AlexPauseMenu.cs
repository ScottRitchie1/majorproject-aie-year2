﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AlexPauseMenu : MonoBehaviour
{
    public static bool paused = false;

    public GameObject pauseMenu;
    public GameObject inGameOptionsMenu;

    // Update is called once per frame
    void Update()
    {
        // Press Esc for pause
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(paused == true)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        // Stop time from unfreezing while in the in-game options menu
        if(inGameOptionsMenu.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        // Unfreeze game
        Time.timeScale = 1f;
        paused = false;
        if(CamController.instance)
        CamController.instance.LockCursor(true);
    }

    public void Pause()
    {
        inGameOptionsMenu.SetActive(false);
        pauseMenu.SetActive(true);
        // Freeze game when paused
        Time.timeScale = 0f;
        paused = true;
        if (CamController.instance)
            CamController.instance.LockCursor(false);
    }

    // Back to main menu
    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Quit to desktop
    public void QuitGame()
    {
        Application.Quit();
    }

    // Hardcoding disabling the pause menu when the options menu is active
    public void ShowPauseMenu()
    {
        pauseMenu.SetActive(true);
        inGameOptionsMenu.SetActive(false);
        foreach (Button b in pauseMenu.GetComponentsInChildren<Button>())
        {
            b.GetComponent<Animator>().SetTrigger("Normal");
        }
    }

    // Hardcoding disabling the options menu when the pause menu is active
    public void ShowOptionsMenu()
    {
        inGameOptionsMenu.SetActive(true);
        pauseMenu.SetActive(false);
        foreach (Button b in inGameOptionsMenu.GetComponentsInChildren<Button>())
        {
            b.GetComponent<Animator>().SetTrigger("Normal");
        }
    }
}

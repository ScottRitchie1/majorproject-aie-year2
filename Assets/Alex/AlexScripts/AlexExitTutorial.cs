﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlexExitTutorial : MonoBehaviour
{
    public Tutorial tutorial;

    public GameObject exitPrompt;

    public bool exitPromptReady = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>())
        {
            // Check if the player completed the tutorial
            if (tutorial.tutorialFinished == true)
            {
                tutorial.headToShrine.SetActive(false);
                exitPrompt.SetActive(true);
                exitPromptReady = true;
            }
        }
    }

    public void Update()
    {
        if (exitPromptReady == true)
        {
            if (Input.GetKey(KeyCode.F))
            {
                SceneManager.LoadScene(2);
            }
        }
    }
}

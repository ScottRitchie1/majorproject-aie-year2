﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TutorialStealerRespawn : MonoBehaviour
{
    public Stealer stealerPrefab;
    public Stealer tempStealer;
    public Transform pivot;
    public float delay = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(f());
    }



    IEnumerator f ()
    {
        tempStealer = Instantiate(stealerPrefab, (pivot? pivot.position : transform.position), (pivot ? pivot.rotation : transform.rotation), pivot);
        tempStealer.GetComponent<NavMeshAgent>().speed = 0;
        
        yield return null;
        yield return null;
        while (true)
        {
            if(tempStealer.GetHealthPercentage() == 0)
            {
                tempStealer.transform.SetParent(null);
                yield return new WaitForSeconds(delay);
                tempStealer = Instantiate(stealerPrefab, (pivot ? pivot.position : transform.position), (pivot ? pivot.rotation : transform.rotation), pivot);
                tempStealer.GetComponent<NavMeshAgent>().speed = 0;
            }
            yield return null;

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{

    public AnimationClip lookAnimation;
    public AnimationClip animation;
    public bool animationTriggered = false;
    public AlexKillPlayer akp;

    // Delay
    public bool delayDone = false;
    public bool delay2Done = false;
    public bool delay3Done = false;
    public bool delay4Done = false;
    public bool delay5Done = false;
    public bool delay6Done = false;
    public bool delay7Done = false;
    public bool delay8Done = false;
    public bool delay9Done = false;
    public bool delay10Done = false;
    public bool delay11Done = false;

    // Tutorial checklist
    public bool lookedAround = false;
    public bool movedAround = false;
    public bool jumped = false;
    public bool doubleJumped = false;
    public bool castedSpells = false;
    public bool targetLocked = false;
    public bool castedDefensive = false;
    public bool activatedAltar = false;
    public bool shrineAltarExplanation = false;
    public bool tutorialFinished = false;

    // Looking checklist
    public bool lookedLeft = false;
    public bool lookedRight = false;

    // Moving checklist
    public bool movedLeft = false;
    public bool movedRight = false;
    public bool movedForward = false;
    public bool movedBackward = false;

    // Casting Spells checklist
    public bool usedSpell = false;
    public bool changedSpell = false;

    // Defensive Spell checklist
    public bool usedDefensiveSpell = false;
    public bool changedDefensiveSpell = false;


    // UI items
    public GameObject fullMouseIcon;
    public GameObject mouseIconFading;
    public GameObject movementWASD;
    public GameObject movementWASDDone;
    public GameObject singleJump;
    public GameObject singleJumpDone;
    public GameObject doubleJump;
    public GameObject doubleJumpDone;
    public GameObject castSpells;
    public GameObject castSpellsDone;
    public GameObject lockOnTarget;
    public GameObject lockOnTargetDone;
    public GameObject defensiveSpell;
    public GameObject defensiveSpellDone;
    public GameObject altarActivation;
    public GameObject altarActivationDone;
    public GameObject altarExplanation;
    public GameObject altarExplanation2;
    public GameObject headToShrine;

    // private Animation anim;


    // Start is called before the first frame update
    void Start()
    {
        //  anim = gameObject.GetComponent<Animation>();
        GetComponent<Animation>().AddClip(animation, animation.name);

        StartCoroutine(Delay());
    }

    // Update is called once per frame
    void Update()
    {
        // At the start of the tutorial
        if (delayDone == true && lookedAround == false)
        {
            // Show 'look around with mouse' prompt
            fullMouseIcon.SetActive(true);

            // If the player looks left
            if (Input.GetAxis("Mouse X") < -1)
            {
                Debug.Log("Looked Left");
                lookedLeft = true;
            }

            // If the player looks right
            if (Input.GetAxis("Mouse X") > 1)
            {
                Debug.Log("Looked Right");
                lookedRight = true;
            }

            // If the player has looked both directions
            if (lookedLeft == true && lookedRight == true)
            {
                Debug.Log("Looked Both");
                // anim.Play();
                //fullMouseIcon.SetActive(false);
                lookedAround = true;


            }
            if (lookedAround == true)
            {
                // Scott stuff
                // if (animationTriggered == false)
                // {
                //     if(GetComponent<Animation>().isPlaying == false)
                //     {
                //         GetComponent<Animation>().Play(animation.name);
                //         animationTriggered = true;
                //     }
                // }
                // else if(GetComponent<Animation>().isPlaying == false)
                // {
                //     //destroy your objecty
                // }
                mouseIconFading.SetActive(true);
                fullMouseIcon.SetActive(false);

                MovementTutorial();
            }
        }

        // If the player has looked around
        if (lookedAround == true)
        {
            // Start movement tutorial
            MovementTutorial();
        }

        // If the player has jumped once
        if (jumped == true && doubleJumped == false)
        {
            // Start the double jump tutorial
            DoubleJumpTutorial();
        }

        // If the player has double jumped
        if (doubleJumped == true && castedSpells == false)
        {
            // Start the casting spells tutorial
            CastSpellsTutorial();
        }

        // If the player has casted spells
        if(castedSpells == true && targetLocked == false)
        {
            // Start the target lock tutorial
            TargetLockTutorial();
        }

        // If the player has locked onto a target
        if(targetLocked == true && castedDefensive == false)
        {
            // Start the defensive spell tutorial
            DefensiveSpellTutorial();
        }

        // If the player used the defensive spell
        if(castedDefensive == true && activatedAltar == false)
        {
            // Start the Shrine Event tutorial
            ShrineAltarTutorial();
        }

        // If the player used Shrine Alter
        if(activatedAltar == true)
        {
            StartCoroutine(Delay9());

            if(delay9Done == true)
            {
                altarExplanation.SetActive(true);
                StartCoroutine(Delay10());
            }
        }

        if(delay10Done == true)
        {
            altarExplanation2.SetActive(true);
            shrineAltarExplanation = true;
        }

        if(shrineAltarExplanation == true && akp.rocksFell == false)
        {
            StartCoroutine(Delay11());
            if(delay11Done == true)
            {
                headToShrine.SetActive(true);
            }
        }

        if(shrineAltarExplanation == true && akp.rocksFell == true)
        {
            tutorialFinished = true;
        }
    }

    // MOVEMENT
    public void MovementTutorial()
    {
        StartCoroutine(Delay2());

        if (delay2Done == true)
        {
            // Show 'move around with WASD' prompt
            movementWASD.SetActive(true);

            // Check if the player moved left
            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("Moved Left");
                movedLeft = true;
            }

            // Check if the player moved right
            if (Input.GetKeyDown(KeyCode.D))
            {
                Debug.Log("Moved Right");
                movedRight = true;
            }

            // Check if the player moved forward
            if (Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log("Moved Forward");
                movedForward = true;
            }

            // Check if the player moved left
            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("Moved Backward");
                movedBackward = true;
            }

            // If the player moved in every direction at least once
            if (movedLeft == true && movedRight == true && movedForward == true && movedBackward == true)
            {
                movementWASD.SetActive(false);
                movementWASDDone.SetActive(true);
                movedAround = true;
                JumpTutorial();
            }
        }
    }

    // JUMPING
    public void JumpTutorial()
    {
        StartCoroutine(Delay3());

        if (delay3Done == true && jumped == false)
        {
            singleJump.SetActive(true);

            // If the player jumps
            if (Input.GetKey(KeyCode.Space))
            {
                singleJump.SetActive(false);
                singleJumpDone.SetActive(true);
                jumped = true;
                DoubleJumpTutorial();
            }
        }
    }

    // DOUBLE JUMPING
    public void DoubleJumpTutorial()
    {
        StartCoroutine(Delay4());

        if (delay4Done == true && doubleJumped == false)
        {
            doubleJump.SetActive(true);

            // If the player jumped while already in the air
            if (FindObjectOfType<PlayerController>().isGrounded == false && Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Double Jumped");
                doubleJump.SetActive(false);
                doubleJumpDone.SetActive(true);
                doubleJumped = true;
            }
        }
    }

    // CASTING SPELLS
    public void CastSpellsTutorial()
    {
        StartCoroutine(Delay5());

        if (delay5Done == true && castedSpells == false)
        {
            // Show spell casting UI
            castSpells.SetActive(true);

            // If the player casted a spell
            if (Input.GetKey(KeyCode.Mouse0))
            {
                usedSpell = true;
                Debug.Log("Used Spell");
            }

            // If the player changed spells
            if (Input.mouseScrollDelta.y != 0)
            {
                changedSpell = true;
                Debug.Log("Changed Spell");
            }

            if(usedSpell == true && changedSpell == true)
            {
                castSpells.SetActive(false);
                castSpellsDone.SetActive(true);
                castedSpells = true;
            }
        }
        // Target locking   FindObjectOfType<PlayerSpellCaster>().IsTargetLocking == true

        // Activating shrine    WaveManager.CurrentShrineEvent != WaveManager.E_ShrineEvent.Inactive
    }

    // TARGET LOCK
    public void TargetLockTutorial()
    {
        StartCoroutine(Delay6());

        if (delay6Done == true && targetLocked == false)
        {
            // Show target lock UI
            lockOnTarget.SetActive(true);

            // If the player locks on to a target
            if (FindObjectOfType<PlayerSpellCaster>().IsTargetLocking == true && Input.GetKey(KeyCode.E))
            {
                Debug.Log("Locked on to target");
                lockOnTarget.SetActive(false);
                lockOnTargetDone.SetActive(true);
                targetLocked = true;
            }
        }
    }

    // DEFENSIVE SPELL
    public void DefensiveSpellTutorial()
    {
        StartCoroutine(Delay7());

        if(delay7Done == true && castedDefensive == false)
        {
            // Show defensive spell UI
            defensiveSpell.SetActive(true);

            // If the player PLACES a defensive spell
            if (Input.GetKey(KeyCode.Mouse1))
            {
                usedDefensiveSpell = true;
            }

            // If the player changed spells
            if (Input.mouseScrollDelta.y != 0)
            {
                changedDefensiveSpell = true;
            }

            if (usedDefensiveSpell == true && changedDefensiveSpell == true)
            {
                defensiveSpell.SetActive(false);
                defensiveSpellDone.SetActive(true);
                castedDefensive = true;
            }
        }
    }

    // SHRINE ALTAR
    public void ShrineAltarTutorial()
    {
        StartCoroutine(Delay8());

        if(delay8Done == true && activatedAltar == false)
        {
            // Show shrine altar UI
            altarActivation.SetActive(true);

            // If the player activated a shrine altar
            if(WaveManager.CurrentShrineEvent != WaveManager.E_ShrineEvent.Inactive)
            {
                Debug.Log("Shrine Event Started");
                altarActivation.SetActive(false);
                altarActivationDone.SetActive(true);
                activatedAltar = true;
            }
        }
    }

    


    public IEnumerator Delay()
    {
        Debug.Log("gud");
        yield return new WaitForSeconds(3F);
        Debug.Log("meme");
        delayDone = true;
    }

    public IEnumerator Delay2()
    {
        yield return new WaitForSeconds(5f);
        delay2Done = true;
    }

    public IEnumerator Delay3()
    {
        yield return new WaitForSeconds(3f);
        delay3Done = true;
    }

    public IEnumerator Delay4()
    {
        yield return new WaitForSeconds(3f);
        delay4Done = true;
    }

    public IEnumerator Delay5()
    {
        yield return new WaitForSeconds(3f);
        delay5Done = true;
    }

    public IEnumerator Delay6()
    {
        yield return new WaitForSeconds(3f);
        delay6Done = true;
    }

    public IEnumerator Delay7()
    {
        yield return new WaitForSeconds(3f);
        delay7Done = true;
    }

    public IEnumerator Delay8()
    {
        yield return new WaitForSeconds(3f);
        delay8Done = true;
    }

    public IEnumerator Delay9()
    {
        yield return new WaitForSeconds(3f);
        delay9Done = true;
    }

    public IEnumerator Delay10()
    {
        yield return new WaitForSeconds(8f);
        delay10Done = true;
    }

    public IEnumerator Delay11()
    {
        yield return new WaitForSeconds(8f);
        delay11Done = true;
    }
}

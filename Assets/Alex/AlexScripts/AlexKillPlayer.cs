﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlexKillPlayer : MonoBehaviour
{

    public Animator rockAnimation;
    public float delay = 1.2f;
    public bool rocksFell = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>())
        {
            rockAnimation.SetTrigger("trigger");
            Invoke("KillPlayer", delay);
            rocksFell = true;
        }
    }

    public void KillPlayer()
    {
        GameObject.FindObjectOfType<PlayerController>().InflictDamage(999999, 1);
    }
}
